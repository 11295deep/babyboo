import React from "react";
//import Navigator from "./Screens/Navigation";
import 'react-native-gesture-handler';
import { Provider } from "react-redux";
import Store from './src/Store'
import Routes from './src/Screens/Routes'

export default class App extends React.Component {
  render() {

    return (
      <Provider store={Store}>
        <Routes />
      </Provider>
    );
  }
}
