import React, { Component } from 'react';
import { FlatList, Button, View, Text, Image, Dimensions, TouchableOpacity, ScrollView, SafeAreaView, StyleSheet } from 'react-native';
import { scale, moderateScale, verticalScale } from "./Scale";
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';

export default class MilestonePopUp extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <Modal
                isVisible={this.props.isModalVisible}
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                }}
                animationInTiming={1000}
                animationOutTiming={1000}
                backdropTransitionInTiming={1000}
                backdropTransitionOutTiming={1000}>
                <View style={{ width: scale(300), height: scale(363), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>
                    <Image
                        style={{ width: scale(150.68), height: scale(123.75),}}
                        source={this.props.image}
                        resizeMode='contain'
                    />

                    <Text style={{ fontSize: scale(15), textAlign: 'center', marginLeft: 7, marginRight: 7,marginTop:verticalScale(38.4) }}>{this.props.text}</Text>
                    <View style={{marginTop: verticalScale(30), }}>

                        <TouchableOpacity
                            style={{
                                justifyContent: 'center',
                                backgroundColor: '#fff',
                                alignItems: 'center',
                                
                                borderRadius: 10,
                                width: scale(139),
                                
                                height: scale(40),
                            }}
                            onPress={this.props.onCancel}>
                            <LinearGradient
                                start={{ x: 0.1, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                                locations={[0, 0.5, 0.6]}
                                colors={['#A076E8', '#5DC4DD']}
                                style={{
                                    
                                    borderRadius: 10,
                                    justifyContent: 'center',
                                    width: scale(139),
                                    alignItems: 'center',
                                   
                                    height: scale(40),
                                }}>
                                <Text style={{ color: '#FFFFFF', fontWeight: 'bold', fontSize: scale(15) }}>{this.props.cancel_label}</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }
}