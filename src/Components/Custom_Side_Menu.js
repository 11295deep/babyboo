import React from 'react';
import {
    FlatList, Button, View, Text,Alert, Image, Dimensions, TouchableOpacity, ScrollView, SafeAreaView, StyleSheet,Linking
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';
import { scale, moderateScale, verticalScale } from './Scale';
import Modal from 'react-native-modal';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from "react-native-spinkit";
import PopUp from "./PopUp";

import {connect} from 'react-redux';
import { userinfo ,loadingOn,loadingOff,changeSelectedChild} from '../Action'

const GOOGLE_PACKAGE_NAME = 'com.babyboo';

class RadioButton extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onClick} activeOpacity={0.8} style={styles.radioButton} disabled={this.props.button.selected ? true : false}>
                {/* <View style={[styles.radioButtonHolder, { height: 50, width: 50, borderColor: "#13EC1A",backgroundColor:"#000" }]}> */}
                {
                    (this.props.button.selected)
                        ?
                        (<View style={[styles.radioIcon, { height: scale(55), width: scale(55), borderColor: "#A173E8", borderWidth: 3 }]}>
                            <Image
                                style={{ height: scale(50), width: scale(50), borderRadius: 400 }}
                                source={{ uri: this.props.button.image }}
                                resizeMode="cover" />
                        </View>)
                        :
                        <View style={[styles.radioIcon, { height: scale(50), width: scale(50), borderColor: "transparent", borderWidth: 0 }]}>
                            <Image
                                style={{ height: scale(50), width: scale(50), borderRadius: 400 }}
                                source={{ uri: this.props.button.image }}
                                resizeMode="cover" />
                        </View>
                }
                <Text style={[styles.label, { color: "#000", marginTop: verticalScale(13), alignSelf: "center" }]}>{this.props.button.name}</Text>
                {/* </View> */}

            </TouchableOpacity>
        );
    }
}

class Custom_Side_Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            isLoading: true,
            image: "",
            isModalVisible: false,

        }
    }
    _out = async () => {
        await AsyncStorage.removeItem('token');
        return this.props.navigation.navigate('Auth');
    }
    cancel = () => {
        this.setState({ isModalVisible: false })
    }
    componentDidMount() {
        // this.setImage();
        // this._navListener = this.props.navigation.addListener('didFocus', this.setImage); //Call api on every onfocus
    }

    changeActiveRadioButton = async (index) => {
         console.log('index', index);
        
        this.props.changeSelectedChild(index)
        this.props.navigation.closeDrawer();
    }

    startRatingCounter = () => {
        let t = setInterval(() => {
            clearInterval(t);
            Alert.alert(
                'Rate us',
                'Would you like to share your review with us? This will help and motivate us a lot.',
                [
                    { text: 'Sure', onPress: () => { this.props.navigation.navigate('Home');this.openStore();} },
                    {
                        text: 'No Thanks!',
                        onPress: () => this.props.navigation.navigate('Home'),
                        style: 'cancel',
                    },
                ],
                { cancelable: false }
            );

        }, 10);
    };

    openStore = () => {
        if (Platform.OS != 'ios') {
            Linking.openURL(`market://details?id=${GOOGLE_PACKAGE_NAME}`).catch(err =>
                alert('Please check for the Google Play Store')
            );
        }
    };


    render() {
        return (
            <ScrollView>
                <SafeAreaView
                    forceInset={{ top: 'always', horizontal: 'never' }}>

                    <PopUp isModalVisible={this.state.isModalVisible}
                        onCancel={() => this.cancel()}
                        onSubmit={() => this._out()}
                        label = "LOGOUT"
                        cancel_label="CANCEL"
                        text ="Are you sure you want to Logout from BabyBoo?"
                        image ={require('../assets/logoutpopup.png')} />

                    <View style={{ flexDirection: "column", height: scale(215), backgroundColor: "#DFF3F8" }}>
                        <View style={{ height: scale(35), marginTop: verticalScale(25) }}>
                            <Image
                                style={{ width: scale(163), height: scale(35), marginLeft: scale(15), }}
                                source={require('../assets/sidelogo.png')}
                                resizeMode='cover'
                            />
                        </View>


                        <View style={{ marginTop: verticalScale(40), height: scale(115), flexDirection: "row", }}>


                            {
                                this.props.sideMenu.map((item, key) =>
                                    (
                                        <RadioButton key={key} button={item} onClick={this.changeActiveRadioButton.bind(this,key)} />
                                    ))
                            }

                            {/* <FlatList
                                data={this.state.data}
                                horizontal={true}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item, index }) => (
                                    <View style={{ flexDirection: "row", justifyContent: "space-between", marginLeft: scale(25) }}>
                                        <TouchableOpacity style={{ flexDirection: "column", alignItems: "center" }}
                                            onPress={() => this.onSelect(index)}>
                                                <Image
                                                    style={{ width: scale(50), height: scale(50), borderRadius: 400, }}
                                                    source={{ uri: item.image }}
                                                    resizeMode='cover'
                                                /> 
                                            <Text style={{ fontSize: scale(14), color: "#000", fontFamily: "Roboto-Bold", marginTop: verticalScale(13) }}>{item.name}</Text>
                                        </TouchableOpacity>
                                    </View>
                                )}
                            /> */}

                            {this.props.sideMenu.length != 2 ?
                                <View style={{ flexDirection: "row", justifyContent: "space-between", marginLeft: scale(45), }}>
                                    <TouchableOpacity style={{ flexDirection: "column", alignItems: "center" }}
                                        onPress={() => this.props.navigation.navigate('RegisterChild')}>
                                        <View style={{ width: scale(50), height: scale(50), borderRadius: 400, backgroundColor: "#fff", justifyContent: "center", alignItems: "center" }}>
                                            <Image
                                                style={{ width: scale(19.17), height: scale(19.17) }}
                                                source={require('../assets/more.png')}
                                                resizeMode='cover'
                                            />
                                        </View>
                                        <Text style={{ fontSize: scale(14), color: "#000", fontFamily: "Roboto-Bold", marginTop: verticalScale(13) }}>ADD</Text>
                                    </TouchableOpacity>
                                </View> : null}

                        </View>
                    </View>

                    <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(20) }}
                        onPress={() => {
                            this.props.navigation.navigate('Profile');
                            this.props.navigation.closeDrawer();
                        }}>
                        <Image
                            style={{ width: scale(32), height: scale(32), marginLeft: scale(15) }}
                            source={require('../assets/usermenu.png')}
                            resizeMode='cover'
                        />
                        <Text
                            style={{ fontSize: scale(16), marginLeft: scale(25) }}

                        >
                            Profile
                    </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(20) }}
                        onPress={() => {
                            this.props.navigation.navigate('ChangePassword');
                            this.props.navigation.closeDrawer();
                        }}>
                        <Image
                            style={{ width: scale(32), height: scale(32), marginLeft: scale(15) }}
                            source={require('../assets/changepassword.png')}
                            resizeMode='cover'
                        />
                        <Text
                            style={{ fontSize: scale(16), marginLeft: scale(25) }}

                        >
                            Change Password
                             </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(20) }}
                        onPress={() => {
                            this.startRatingCounter();
                            this.props.navigation.closeDrawer();
                        }}>
                        <Image
                            style={{ width: scale(32), height: scale(32), marginLeft: scale(15) }}
                            source={require('../assets/star.png')}
                            resizeMode='cover'
                        />
                        <Text
                            style={{ fontSize: scale(16), marginLeft: scale(25) }}

                        >
                            Rate Us
                             </Text>
                    </TouchableOpacity>                    

                    <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(20) }}
                        onPress={() => {
                            // this.props.navigation.navigate('Help');
                            this.props.navigation.closeDrawer();
                        }}>
                        <Image
                            style={{ width: scale(32), height: scale(32), marginLeft: scale(15) }}
                            source={require('../assets/Help.png')}
                            resizeMode='cover'
                        />
                        <Text
                            style={{ fontSize: scale(16), marginLeft: scale(25) }}

                        >
                            Help
                             </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(20) }}
                        onPress={() => {
                            this.props.navigation.navigate('About');
                            this.props.navigation.closeDrawer();
                        }}>
                        <Image
                            style={{ width: scale(32), height: scale(32), marginLeft: scale(15) }}
                            source={require('../assets/Aboutus.png')}
                            resizeMode='cover'
                        />
                        <Text
                            style={{ fontSize: scale(16), marginLeft: scale(25) }}

                        >
                            About Us
                    </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{ flexDirection: "row", alignItems: "center", marginTop: verticalScale(20) }}
                        onPress={() => {
                            // this.props.navigation.navigate('Logout');
                            this.setState({ isModalVisible: !this.state.isModalVisible })
                            this.props.navigation.closeDrawer();
                        }}>
                        <Image
                            style={{ width: scale(32), height: scale(32), marginLeft: scale(15) }}
                            source={require('../assets/Logout.png')}
                            resizeMode='cover'
                        />
                        <Text
                            style={{ fontSize: scale(16), marginLeft: scale(25) }}

                        >
                            Logout
                             </Text>
                    </TouchableOpacity>

                </SafeAreaView>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f7f9',
    },
    linearGradient: {
        marginLeft: scale(7),
        borderRadius: 10,
        justifyContent: 'center',
        width: scale(115),
        alignItems: 'center',
        marginRight: scale(20),
        height: scale(40),
    },
    btnSignin: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        marginLeft: scale(7),
        borderRadius: 10,
        width: scale(115),
        marginRight: scale(20),
        height: scale(40),
    },
    btn: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        borderRadius: 10,
        padding: 10,
        height: 40,
        width: 125,
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        alignSelf: "center",
        height: 50,
        width: 300,
        borderRadius: 10,
        marginTop: 30
    },
    radioButton:
    {
        flexDirection: 'column',
        marginLeft: scale(45),
        //margin: scale(10),
        alignItems: 'center',
        //justifyContent: 'center'
    },

    radioButtonHolder:
    {
        borderRadius: 50,
        borderWidth: 2,
        marginLeft: scale(50),
        flexDirection: "column",
        justifyContent: 'center',
        alignItems: 'center'
    },

    radioIcon:
    {
        borderRadius: 50,
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: 'center'
    },

    label:
    {
        fontSize: scale(14)
    },

})

const child = (child,selectedChild) => {
    const res = child
    let data = []
        for (var i = 0; i < res.length; i++) {
            const dataObject = {};
            console.log('ll')
            dataObject['name'] = res[i].child.name;
            dataObject['image'] = `${res[i].imageUrl}?time=${new Date()}`;
            dataObject['selected'] = i == selectedChild ? true : false
            data.push(dataObject);
        }
        console.log('sidemenu',data);
        return data;
}

function mapStateToProps(state){
    let selectedChild = state.userinfo.selectedChild
    return{
        user:state.userinfo.user,
        sideMenu:child(state.userinfo.user,selectedChild),
        // completeVaccines:calculateVaccines(state.userinfo.user[0]),
        // completePerc:calculatePerc(state.userinfo.user[0]),
        // nextVaccine:calculateNextVaccine(state.userinfo.user[0]),
        loder: state.loder,
        
    }
}


export default connect(mapStateToProps,{userinfo,loadingOn,loadingOff,changeSelectedChild})(Custom_Side_Menu)