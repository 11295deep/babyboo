import React, {Component} from 'react';
import { withNavigation } from 'react-navigation';
import { TouchableOpacity, View, Text, Image,Dimensions,TouchableHighlight } from 'react-native';
//import { TouchableOpacity } from "react-native-gesture-handler";
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import { scale, moderateScale, verticalScale } from './Scale';
class HamburgerIcon extends Component{
    render() {
    return (
        <TouchableOpacity
        style={{
            width: scale(44),
            height: scale(44),
            marginLeft: scale(15),
            justifyContent:"center"
        }}
        onPress={()=>{
            this.props.navigation.toggleDrawer();
        }}>
            <Icon name='menu' size={scale(25)} color='white'/>
        </TouchableOpacity>
    )
    };
}
export default withNavigation(HamburgerIcon);