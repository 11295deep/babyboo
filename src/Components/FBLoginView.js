import React, { Component } from 'react';
import { StyleSheet, Text, View,TouchableOpacity,Image } from 'react-native';
import PropTypes from 'prop-types';
var Icon = require('react-native-vector-icons/FontAwesome');
import { scale, moderateScale, verticalScale } from './Scale';

/**
 Example FBLoginView class
 Please note:
 - this is not meant to be a full example but highlights what you have access to
 - If you use a touchable component, you will need to set the onPress event like below
 **/
const shadowStyle = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.23,
  shadowRadius: 2.62,

  elevation: 4,
}

class FBLoginView extends Component {
  static contextTypes = {
    isLoggedIn: PropTypes.bool,
    login: PropTypes.func,
    logout: PropTypes.func,
    props: PropTypes.shape({})
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={[]}>
        <TouchableOpacity
          style={[styles.btn, shadowStyle]}
          onPress={() => {
            if (!this.context.isLoggedIn) {
              this.context.login()
            } else {
              this.context.logout()
            }
  
          }}>
          <Image
            style={{ width: scale(25), height: scale(25), borderWidth: 0, }}
            source={require('../assets/facebook.png')}
            resizeMode='contain' />
          <Text style={{ color: '#3b5998', fontWeight: 'bold', fontSize: scale(14), marginLeft: scale(10) }}>Facebook </Text>
        </TouchableOpacity>
        {/* <Icon.Button onPress={() => {
          if (!this.context.isLoggedIn) {
            this.context.login()
          } else {
            this.context.logout()
          }

        }}
          color={"#000000"}
          backgroundColor={"#ffffff"} name={"facebook"} size={20} borderRadius={100} >

        </Icon.Button> */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  btn: {
      justifyContent: 'center',
      backgroundColor: '#fff',
      alignItems: 'center',
      flexDirection: 'row',
      borderRadius: 10,
      padding: scale(10),
      height: scale(40),
      marginTop: verticalScale(30),
      width: scale(135),
  },
  
})


module.exports = FBLoginView;