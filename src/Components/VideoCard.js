import React, { Component } from 'react';
import { View, TextInput, Image, TouchableOpacity, Text, ImageBackground } from 'react-native';
import { scale, moderateScale, verticalScale } from "./Scale";

export default class VideoCard extends Component {
    constructor() {
        super();
    }
    render() {
        return (
            <TouchableOpacity style={{ width: scale(180), height: scale(135), marginTop: verticalScale(15), backgroundColor: 'transparent', marginLeft: this.props.index === 0 ? null : scale(10) }}
                onPress={this.props.OnClick}>
                <ImageBackground style={{ width: scale(180), height: scale(100), justifyContent: "center", alignItems: "center" }}
                    source={{ uri: `https://i.ytimg.com/vi/` + this.props.item.link + `/mqdefault.jpg` }}
                    imageStyle={{ borderRadius: 10, }}
                    resizeMode="cover">
                    <Image
                        style={{ width: scale(48), height: scale(48), }}
                        source={require('../assets/Buttons-Play.png')}
                        resizeMode="cover"
                    />
                    <View style={{ backgroundColor: "#000000", borderRadius: 8, width: scale(40), height: scale(20), alignSelf: "flex-end", right: scale(7), bottom: scale(7), position: 'absolute', justifyContent: "center", alignItems: "center" }}>
                        <Text style={{ color: "#FFFFFF", fontSize: scale(12) }}>{this.props.item.length}</Text>
                    </View>
                </ImageBackground>

                <Text style={{ color: "#000000", fontSize: scale(14), marginTop: verticalScale(12) }} numberOfLines={1}>{this.props.item.title}</Text>

            </TouchableOpacity>
        );
    }
}