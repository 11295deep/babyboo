import React, { Component } from 'react';
import { FlatList, Button, View, Text, Image, Dimensions, TouchableOpacity, ScrollView, SafeAreaView, StyleSheet } from 'react-native';
import { scale, moderateScale, verticalScale } from "./Scale";
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';

export default class PopUp extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <Modal
                isVisible={this.props.isModalVisible}
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                }}
                animationInTiming={1000}
                animationOutTiming={1000}
                backdropTransitionInTiming={1000}
                backdropTransitionOutTiming={1000}>
                <View style={{ width: scale(300), height: scale(363), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>
                    <Image
                        style={{ width: scale(150.68), height: scale(123.75),}}
                        source={this.props.image}
                        resizeMode='contain'
                    />

                    <Text style={{ fontSize: scale(15), textAlign: 'center', marginLeft: 7, marginRight: 7,marginTop:verticalScale(38.4) }}>{this.props.text}</Text>
                    <View style={{ flexDirection: "row", marginTop: verticalScale(30), }}>

                        <TouchableOpacity
                            style={{
                                justifyContent: 'center',
                                backgroundColor: '#fff',
                                alignItems: 'center',
                                marginLeft: scale(7),
                                borderRadius: 10,
                                width: scale(115),
                                marginRight: scale(20),
                                height: scale(40),
                            }}
                            onPress={this.props.onCancel}>
                            <LinearGradient
                                start={{ x: 0.1, y: 1.0 }} end={{ x: 1.0, y: 0.0 }}
                                locations={[0, 0.5, 0.6]}
                                colors={['#a076e8', '#5dc4dd']}
                                style={{
                                    marginLeft: scale(7),
                                    borderRadius: 10,
                                    justifyContent: 'center',
                                    width: scale(115),
                                    alignItems: 'center',
                                    marginRight: scale(20),
                                    height: scale(40),
                                }}>
                                <Text style={{ color: '#FFFFFF', fontWeight: 'bold', fontSize: scale(15) }}>{this.props.cancel_label}</Text>
                            </LinearGradient>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{
                                borderColor: "#707070", borderWidth: 1, justifyContent: 'center',
                                backgroundColor: '#fff',
                                alignItems: 'center',
                                borderRadius: 10,
                                marginLeft: scale(5),
                                height: scale(40),
                                width: scale(115),
                            }}
                            onPress={this.props.onSubmit}>
                            <Text style={{ color: '#000' }}>{this.props.label}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }
}