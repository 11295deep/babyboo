import React from 'react';
import { Button, View, Text, StyleSheet, ImageBackground, Image, Animated, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { scale, moderateScale, verticalScale } from './Scale';


class RadioButton extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onClick} activeOpacity={0.8} style={{ width: "33.33%", alignItems: "center", justifyContent: "center", }}>
                {
                    this.props.button.selected
                        ?
                        <View style={{ width: "100%", height: scale(40), borderBottomWidth: 2, borderBottomColor: "#6F2BE2", alignItems: "center", justifyContent: "center", borderTopLeftRadius: 4, borderTopRightRadius: 4, }}>
                            <Text style={{ fontSize: scale(14), fontWeight: "bold" }}>{this.props.button.label}</Text>
                        </View>
                        :
                        <View style={{ width: "100%", height: scale(40), alignItems: "center", justifyContent: "center", }}>
                            <Text style={{ fontSize: scale(14) }}>{this.props.button.label}</Text>
                        </View>
                }
            </TouchableOpacity>
        );
    }
}
export default RadioButton;