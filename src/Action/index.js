import { MILESTONE, LOGIN, HIDELODER, SHOWLODER, REGISTER, USERINFO, VACCINE_SAVE, UPDATE_VACCINE_SAVE, CHANGE_SELECTED_CHILD, UPDATE_VACCINE, RHYMES, CHILD, CHILDVACCINE, UPDATE_MILESTONE, MILESTON_SAVE, UPDATE_MILESTONE_SAVE } from '../constant';
import axios from 'axios';
import url from "../API";
import { config } from "../config"
import AsyncStorage from '@react-native-community/async-storage';
import setAuthToken from "../utils/setAuthToken";

export const saveUser = (userdata, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    dispatch({
        type: REGISTER,
        payload: userdata
    })
    try {
        const response = await axios.post(config().registerUrl, userdata);
        console.log("Action_saveUser_response", response);
        if (response.status === 200) {
            callback()
            // alert('Successfully Register')
        }
        return response;
    }
    catch (e) {
        console.log("Action_saveUser_error", e.response);
        console.log("Action_saveUser_error", e);
        if (e.response.status === 403) {
            alert(e.response.data)
        } else {
            alert(e)
        }
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const login = (logindata, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        const response = await axios.post(config().loginUrl, logindata);
        console.log("Action_login_response", response);
        if (response.status === 200) {
            callback()
            setAuthToken(response.data.token);
            await AsyncStorage.setItem('logged', '1');
            await AsyncStorage.setItem('token', response.data.token);
            dispatch({
                type: LOGIN,
                payload: response.data.token
            })
            alert('Successfully Login')
        }
        return response;
    }
    catch (e) {
        console.log("Action_login_error", e.response)
        console.log("Action_login_error", e)
        if (e.response.status === 403) {
            alert('Incorrect Email or Password')
        }
        else {
            alert(e)
        }
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const fbloginUser = (logindata, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        const response = await axios.post(config().fbloginurl, data);
        console.log("Action_fbloginUser_response", response);
        if (response.data) {
            callback()
            setAuthToken(response.data.token);
            await AsyncStorage.setItem('logged', '1');
            await AsyncStorage.setItem('token', response.data.token);
            dispatch({
                type: LOGIN,
                payload: response.data.token
            })
            alert('Successfully Login')
        }
        return response;
    }
    catch (e) {
        console.log("Action_fbloginUser_error", e.response)
        console.log("Action_fbloginUser_error", e)

        alert(e)
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const userinfo = (callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    const token = await AsyncStorage.getItem('token');
    const login = await AsyncStorage.getItem('username');
    try {
        const response = await axios.get(config().userInfoUrl + login);
        console.log("Action_getUserInfo_response", response);
        let res = response.data;
        let child = [];
        const object = {};
        object[`child`] = res.child
        object[`imageUrl`] = res.imageUrl
        object[`pdfUrl`] = res.pdfUrl
        child.push(object)

        if (response.status === 200) {
            dispatch({
                type: CHILD,
                payload: child
            })
            dispatch({
                type: CHILDVACCINE,
                payload: response.data.childVaccines
            })
            if (response.data.children) {
                dispatch({
                    type: USERINFO,
                    payload: response.data.children
                })
                dispatch({
                    type: VACCINE_SAVE,
                    payload: response.data.children
                })

                callback()
            }
        }
        return response;
    }
    catch (e) {
        console.log("Action_getUserInfo_error", e.response)
        console.log("Action_getUserInfo_error", e)
        alert(e)
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const Milestone = (callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    const token = await AsyncStorage.getItem('token');
    const login = await AsyncStorage.getItem('username');
    try {
        const response = await axios.get(config().getMilestoneDataUrl);
        console.log("Action_Milestone_response", response);
        if (response.status === 200) {
            dispatch({
                type: MILESTONE,
                payload: response.data
            })
            dispatch({
                type: MILESTON_SAVE,
                payload: response.data
            })

            callback()
        }
        return response;
    }
    catch (e) {
        console.log("Action_Milestone_error", e.response)
        console.log("Action_Milestone_error", e)
        alert(e)
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const Update_Milestone = (selectedChild, data, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        const response = await axios.post(config().updateMilestoneDataUrl + selectedChild, { milestone: data });
        console.log("Action_Update_Milestone_response", response);
        if (response.status === 200) {
            dispatch({
                type: UPDATE_MILESTONE_SAVE,
                payload: { [selectedChild]: data }
            })
            dispatch({
                type: UPDATE_MILESTONE,
                payload: { [selectedChild]: data }
            })
            callback()
        }
        return response;
    }
    catch (e) {
        console.log("Action_Update_Milestone_error", e.response)
        console.log("Action_Update_Milestone_error", e)
        alert(e)
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const upload_Image = (image, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        let response;
        console.log('image',image);
        
        response = await axios.post(config().uploadImgUrl, image);
        console.log("Action_upload_Image_response", response);
        if (response.status === 200) {
            alert('Image Successfully Uploaded')
        }

        return response;
    } catch (e) {
        console.log("Action_upload_Image_error", e.response);
        console.log("Action_upload_Image_error", e);
        alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}
export const registerChild = (child, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        let response;
        response = await axios.post(config().registerChildUrl, child);
        console.log("Action_registerChild_response", response);
        if (response.status === 200) {
            alert('Child Successfully Added')
        }

        return response;
    } catch (e) {
        console.log("Action_registerChild_error", e.response);
        console.log("Action_registerChild_error", e);
        alert(e);
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const resetPassword = (data, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        let response;
        response = await axios.post(config().resetPasswordUrl, data);
        console.log("Action_resetPassword_response", response);
        // if (response.status === 200) {
        //     alert('Successfully Change Password')
        // }

        return response;
    } catch (e) {
        console.log("Action_resetPassword_error", e.response);
        console.log("Action_resetPassword_error", e);
        if (e.response.status === 403) {
            alert(e.response.data);
        }
        else {
            alert(e)
        }
        return e;

        // alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const forgotpwd = (data, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        let response;
        response = await axios.post(config().forgotPasswordUrl, data);
        console.log("Action_forgotpwd_response", response);
        // if (response.status === 200) {
        //     alert('Successfully Change Password')
        // }

        return response;
    } catch (e) {
        console.log("Action_forgotpwd_error", e.response);
        console.log("Action_forgotpwd_error", e);
        if (e.response.status === 403) {
            alert(e.response.data);
        }
        else {
            alert(e)
        }
        return e;

        // alert(e);
    }
    finally {
        loadingOff(dispatch)
    }
}

export const rhymes = (callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        const response = await axios.get(config().getRhymesDataUrl);
        console.log("Action_rhymes_response", response);
        if (response.status === 200) {
            dispatch({
                type: RHYMES,
                payload: response.data
            })
            callback()
        }
        return response;
    }
    catch (e) {
        console.log("Action_rhymes_error", e.response)
        console.log("Action_rhymes_error", e)
        alert(e)
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}

export const Issue_Vaccine = (selectedChild, data, callback = () => { }) => async (dispatch, getState) => {
    loadingOn(dispatch)
    try {
        const response = await axios.post(config().issueVaccine + selectedChild, { childVaccine: data });
        console.log("Action_Issue_Vaccine_response", response);
        if (response.status === 200) {
            dispatch({
                type: UPDATE_VACCINE,
                payload: { selectedChild, data: { childVaccines: data } }
            })
            dispatch({
                type: UPDATE_VACCINE_SAVE,
                payload: { selectedChild, data: { childVaccines: data } }
            })
            callback()
        }
        return response;
    }
    catch (e) {
        console.log("Action_Issue_Vaccine_error", e.response)
        console.log("Action_Issue_Vaccine_error", e)
        alert(e)
        return e;
    }
    finally {
        loadingOff(dispatch)
    }
}


export const loadingOn = (dispatch) => {
    dispatch({
        type: SHOWLODER,
    });
};

export const loadingOff = (dispatch) => {
    dispatch({
        type: HIDELODER,
    });
};

export const changeSelectedChild = (index) => {
    return {
        type: CHANGE_SELECTED_CHILD,
        payload: index
    }
}

export const changeMilestone = (data) => async (dispatch, getState) => {
    console.log('data', data);

    dispatch({
        type: UPDATE_MILESTONE,
        payload: data,
    });
    // return {

    // }
}
