export const TEST_SERVER = 'http://3.91.182.21:3001';
export const RELEASE_SERVER = 'http://34.207.213.121:3001';

//http://34.207.213.121

export function config() {
  const confs = {
    test: {
      loginUrl: `${TEST_SERVER}/login`,
      registerUrl: `${TEST_SERVER}/registerUser`,
      fbloginurl: `${TEST_SERVER}/fblogin`,
      userInfoUrl: `${TEST_SERVER}/getUserInfo/?stream=bb_stream&key=`,
      getMilestoneDataUrl: `${TEST_SERVER}/getMilestoneData/`,
      updateMilestoneDataUrl: `${TEST_SERVER}/updateMilestone?index=`,
      uploadImgUrl: `${TEST_SERVER}/uploadImg`,
      registerChildUrl: `${TEST_SERVER}/registerChild`,
      resetPasswordUrl: `${TEST_SERVER}/resetPassword`,
      forgotPasswordUrl: `${TEST_SERVER}/forgotPassword`,
      getRhymesDataUrl: `${TEST_SERVER}/getRhymesData`,
      issueVaccine: `${TEST_SERVER}/issueVaccine?index=`,
    },
    release: {
      loginUrl: `${RELEASE_SERVER}/login`,
      registerUrl: `${RELEASE_SERVER}/registerUser`,
      fbloginurl: `${RELEASE_SERVER}/fblogin`,
      userInfoUrl: `${RELEASE_SERVER}/getUserInfo/?stream=bb_stream&key=`,
      getMilestoneDataUrl: `${RELEASE_SERVER}/getMilestoneData/`,
      updateMilestoneDataUrl: `${RELEASE_SERVER}/updateMilestone?index=`,
      uploadImgUrl: `${RELEASE_SERVER}/uploadImg`,
      registerChildUrl: `${RELEASE_SERVER}/registerChild`,
      resetPasswordUrl: `${RELEASE_SERVER}/resetPassword`,
      forgotPasswordUrl: `${RELEASE_SERVER}/forgotPassword`,
      getRhymesDataUrl: `${RELEASE_SERVER}/getRhymesData`,
      issueVaccine: `${RELEASE_SERVER}/issueVaccine?index=`,
    },
  }

  //const conf = confs['test'];
  const conf = confs['release'];

  return conf;
}