import { combineReducers } from "redux";
import register from './register.reducer';
import login from './login.reducer';
import userinfo from './userinfo.reducer';
import loder from './loder.reducer'
import Rhymes from "./Rhymes.reducer";
import milestone from "./milestone.reducer"

export default combineReducers({
    register,
    login,
    userinfo,
    loder,
    Rhymes,
    milestone
})