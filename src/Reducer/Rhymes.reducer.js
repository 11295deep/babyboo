import {RHYMES} from '../constant'
export default function(state={Rhymesdata:''}, action){
    switch(action.type){
        
        case RHYMES:
            return{
                ...state,
                Rhymesdata:action.payload,
            }
        default:
            return state
    }
}