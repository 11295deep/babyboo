import { USERINFO, USERUPDATE, CHANGE_SELECTED_CHILD, CHILD, CHILDVACCINE, VACCINE_SAVE, UPDATE_VACCINE_SAVE, UPDATE_VACCINE } from '../constant'
export default function (state = { user: [], userSave: [], selectedChild: 0, child: [], childVaccine: [] }, action) {
    switch (action.type) {

        case USERINFO:
            console.log('reduceruser', action.payload)
            return {
                ...state,
                user: action.payload,
            }
        case UPDATE_VACCINE:
            user = state.user[state.selectedChild]
            user.childVaccines = {
                ...user.childVaccines,
                ...action.payload.data.childVaccines
            }
            console.log("state", state.child);

            // return {
            //     ...state,
            //     user: {
            //         ...state.user,
            //         [state.selectedChild]:user
            //     },
            // }
            return state
        case VACCINE_SAVE:
            return {
                ...state,
                userSave: action.payload,
            }
        case UPDATE_VACCINE_SAVE:
            userSave = state.userSave[state.selectedChild]
            userSave.childVaccines = {
                ...userSave.childVaccines,
                ...action.payload.data.childVaccines
            }
            return state
        case CHANGE_SELECTED_CHILD:
            return {
                ...state,
                selectedChild: action.payload
            }
        case CHILD:
            return {
                ...state,
                child: action.payload,
            }
        case CHILDVACCINE:
            return {
                ...state,
                childVaccine: action.payload,
            }

        default:
            return state
    }
}
