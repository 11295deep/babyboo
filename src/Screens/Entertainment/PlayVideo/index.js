import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    PixelRatio,
    ImageBackground,
    SafeAreaView,
    Image,
    FlatList,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    BackHandler,
    Switch,
    RefreshControl,
    Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes, login } from '../../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../../../Components/Header';
import Spinner from "react-native-spinkit";
import firebase from 'react-native-firebase';
import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
} from 'react-native-youtube';
import { HeaderBackButton } from 'react-navigation-stack';
import HamburgerIcon from "../../../Components/HamburgerIcon"
import { BackgroundCarousel } from "../../../Components/BackgroundCarousel";
import { scale, moderateScale, verticalScale } from '../../../Components/Scale';

const screenDimension = Dimensions.get('window');
const isBigDevice = screenDimension.height > 600;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const shadowStyle = {
    shadowColor: '#000',
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
};
class PlayVideo extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: '  Entertainment  ',
       // headerLeft: <HamburgerIcon />,
       headerLeft: (
        <HeaderBackButton onPress={() => navigation.navigate('Entertainment')} />
    ),
        headerBackground: <Header />,
        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: scale(20),
            fontFamily: "Roboto-Bold",
            fontWeight: "bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        },
    });
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            id: this.props.navigation.getParam('id', 'NO-User'),
            title: this.props.navigation.getParam('title', 'NO-User'),
            switchValue: false,
            selectedData: [],

            isReady: false,
            status: null,
            quality: null,
            error: null,
            isPlaying: true,
            isLooping: false,
            duration: 0,
            height: null,
            currentTime: 0,
            fullscreen: false,
            containerMounted: false,
            containerWidth: null,
            currentPlayingIndex: 0,
            autoplaying: false,
            autoplay: false
        };
    }
    componentWillMount() {

        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Entertainment');
        return true;
    }

    toggleSwitch = (value) => {
        this.setState({ switchValue: value, autoplay: value })
    }

    autoplay = (value) => {
        if (value)
            return new Promise(async (res, rej) => {
                res()
                let currentTime = await this._youTubeRef.getCurrentTime(),
                    getDuration = await this._youTubeRef.getDuration()
                if (currentTime != 0 && getDuration > currentTime) {


                    console.log('in here', await this._youTubeRef.getCurrentTime());
                    this.setState({
                        autoplaying: true
                    })

                    var i = this.state.currentPlayingIndex

                    while (await this._youTubeRef.getDuration() > await this._youTubeRef.getCurrentTime())
                        continue
                    setTimeout(() => {
                        if (this.state.autoplay)
                            for (var j = 0; j < this.props.Rhymes.length; j++) {
                                if (this.props.Rhymes[i].link === this.props.Rhymes[j].link) {
                                    this.props.Rhymes.splice(i, 1);
                                }
                            }
                        this.setState({
                            id: this.props.Rhymes[i].link,
                            title: this.props.Rhymes[i].title,
                            isPlaying: true,
                            currentPlayingIndex: i + 1
                        })

                        this.setState({
                            autoplaying: false
                        })
                    }, 100)
                    console.log(this.state.autoplaying);
                }
            })

    }

    _click = (id, title, item) => {
        let data = [];
        for (var i = 0; i < this.props.Rhymes.length; i++) {
            if (id === this.props.Rhymes[i].link) {
                this.props.Rhymes.splice(i, 1);
                data.push(this.props.Rhymes.splice(i, 1))
                // this.props.Rhymes.push(item);
            }
        }
        console.log('tttt', ...data);
        this.ListView_Ref.scrollTo({ animated: true },0);

        this.setState({
            id: id,
            title: title
        })

    }

    render() {
        const { navigation } = this.props;
        const name = navigation.getParam('id', 'NO-User');
        console.log(this.state.autoplaying, this.state.autoplay);

        if (this.state.autoplay && !this.state.autoplaying) {
            this.autoplay(true)
        }
        if (this.props.loder) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }} />
                </View>
            );
        }

        return (
            <View style={styles.container}>

                <ScrollView
                    ref={(ref) => {
                        this.ListView_Ref = ref;
                    }}
                    onLayout={({
                        nativeEvent: {
                            layout: { width },
                        },
                    }) => {
                        if (!this.state.containerMounted)
                            this.setState({ containerMounted: true });
                        if (this.state.containerWidth !== width)
                            this.setState({ containerWidth: width });
                    }}>
                    {this.state.containerMounted && (
                        <YouTube
                            ref={component => {
                                this._youTubeRef = component;
                            }}
                            apiKey="AIzaSyC7AYGZS9YK_L23ksv0MFOb-GqEPI-LVZA"
                            videoId={this.state.id}
                            play={this.state.isPlaying}
                            loop={this.state.isLooping}
                            fullscreen={this.state.fullscreen}
                            controls={1}
                            style={[
                                {
                                    height:
                                        PixelRatio.roundToNearestPixel(
                                            this.state.containerWidth / (16 / 9)
                                        ),
                                },
                                styles.player,
                            ]}
                            onError={e => {
                                console.log('error', e, 'e.error', e.error);
                                this.setState({ error: e.error })
                            }}
                            onReady={e => this.setState({ isReady: true })}
                            onChangeState={e => { console.log('hello', e); this.setState({ status: e.state }) }}
                            onChangeQuality={e => this.setState({ quality: e.quality })}
                            onChangeFullscreen={e =>
                                this.setState({ fullscreen: e.isFullscreen })
                            }
                            onProgress={e => {

                                this.setState({
                                    duration: e.duration,
                                    currentTime: e.currentTime,
                                })
                            }
                            }
                        />
                    )}

                    {/* <TouchableOpacity
                        style={styles.button}
                        onPress={() => {
                            this.cc()

                            this._youTubeRef && this._youTubeRef.getDuration()
                        }
                        }>
                        <Text style={styles.buttonText}>Previous Video</Text>
                    </TouchableOpacity> */}

                    <Text style={{ fontSize: scale(22), fontWeight: "bold", marginTop: verticalScale(15), marginLeft: scale(20) }} numberOfLines={1}>{this.state.title}</Text>

                    {/* <View style={styles.buttonGroup}>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => this.setState(s => ({ isPlaying: !s.isPlaying }))}>
                            <Text style={styles.buttonText}>
                                {this.state.status == 'playing' ? 'Pause' : 'Play'}
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.instructions}>
                        {this.state.isReady ? 'Player is ready' : 'Player setting up...'}
                    </Text>
                    <Text style={styles.instructions}>Status: {this.state.status}</Text>
                    <Text style={styles.instructions}>Quality: {this.state.quality}</Text>

                    <Text style={styles.instructions}>
                        {this.state.error ? 'Error: ' + this.state.error : ''}
                    </Text> */}
                    <Text style={styles.instructions}>
                        {this.state.error ? 'Error: ' + this.state.error : ''}
                    </Text>

                    <View style={{
                        marginTop: verticalScale(39),
                        marginRight: scale(20),
                        marginLeft: scale(20),
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between"
                    }}>
                        <View style={{
                            // width: "80%"
                        }}>
                            <Text style={{
                                fontSize: scale(18),
                                fontFamily: "Roboto-Bold",
                                color: '#707070',
                            }}>More Related Rhymes</Text>
                        </View>

                        <View style={{
                            // width: "20%"
                            flexDirection: "row",
                            alignItems: "center"
                        }}>
                            <Text style={{
                                fontSize: scale(13),
                                fontFamily: "Roboto-Regular",
                                color: '#707070',
                            }}>Auto Play</Text>
                            <Switch
                                style={{ marginLeft: scale(7) }}
                                onValueChange={this.toggleSwitch}
                                value={this.state.switchValue} />
                        </View>
                    </View>

                    <FlatList
                        data={this.props.Rhymes}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => (
                            <>
                                {item.link !== this.state.id &&
                                    <View>
                                        <TouchableOpacity style={{ marginLeft: scale(20), marginRight: scale(20), marginTop: verticalScale(12), flexDirection: "row" }}
                                            onPress={() => this._click(item.link, item.title, item)}>
                                            <ImageBackground style={{ width: scale(177), height: scale(98), justifyContent: "center", alignItems: "center" }}
                                                source={{ uri: `https://i.ytimg.com/vi/` + item.link + `/mqdefault.jpg` }}
                                                imageStyle={{ borderRadius: 10, }}
                                                resizeMode="cover">
                                                <View style={{ backgroundColor: "#000000", borderRadius: 8, width: scale(40), height: scale(20), alignSelf: "flex-end", right: scale(7), bottom: scale(7), position: 'absolute', justifyContent: "center", alignItems: "center" }}>
                                                    <Text style={{ color: "#FFFFFF", fontSize: scale(12) }}>{item.length}</Text>
                                                </View>
                                            </ImageBackground>
                                            <View style={{ width: "40%" }}>
                                                <Text style={{ color: "#000000", fontSize: scale(14), marginLeft: scale(12), }} numberOfLines={4}>{item.title}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                }
                            </>
                        )} />


                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f7f9',
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    buttonGroup: {
        flexDirection: 'row',
        alignSelf: 'center',
    },
    button: {
        paddingVertical: 4,
        paddingHorizontal: 8,
        alignSelf: 'center',
    },
    buttonText: {
        fontSize: 18,
        color: 'blue',
    },
    buttonTextSmall: {
        fontSize: 15,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    player: {
        alignSelf: 'stretch',
        marginVertical: 10,
    },
})

const Rhymesdata = (video) => {

    const res = video;

    let data = [];
    for (let key in res) {
        const Object = {};
        for (var i = 0; i < res[key].length; i++) {
            Object = res[key][i]
            data.push(Object);
        }
    }

    return data;
}


function mapStateToProps(state) {
    let selectedChild = state.userinfo.selectedChild
    return {
        user: state.userinfo.user[selectedChild],
        loder: state.loder,
        Rhymes: Rhymesdata(state.Rhymes.Rhymesdata)

    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes })(PlayVideo)