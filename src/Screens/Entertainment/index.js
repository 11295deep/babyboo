import React from 'react';
import {
    LayoutAnimation, Platform, UIManager,
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    FlatList,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    BackHandler,
    RefreshControl,
    Alert,
} from 'react-native';
import { connect } from 'react-redux';
import VideoCard from "../../Components/VideoCard"
import { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes } from '../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../../Components/Header';
import Spinner from "react-native-spinkit";
import firebase from 'react-native-firebase';
import HamburgerIcon from "../../Components/HamburgerIcon"
import { BackgroundCarousel } from "../../Components/BackgroundCarousel";
import { scale, moderateScale, verticalScale } from '../../Components/Scale';

const screenDimension = Dimensions.get('window');
const isBigDevice = screenDimension.height > 600;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const shadowStyle = {
    shadowColor: '#000',
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
};

class RadioButton extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onClick} activeOpacity={0.8} style={{ marginLeft: this.props.index === 0 ? null : scale(15), }}>
                {
                    (this.props.button.selected)
                        ?
                        (<View style={{ width: scale(193), height: scale(66), borderRadius: 10, flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
                            <LinearGradient
                                colors={[this.props.button.color2, this.props.button.color1]}
                                style={{ width: scale(193), height: scale(66), borderRadius: 10, flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 0 }}>
                                <Image
                                    style={{ height: scale(40), width: scale(40), }}
                                    source={this.props.button.focused}
                                    resizeMode="contain" />
                                <Text style={{ fontSize: scale(18), fontWeight: "bold", color: "#FFFFFF" }}>{this.props.button.label}</Text>
                            </LinearGradient>
                        </View>)
                        :
                        <View style={{ width: scale(193), height: scale(66), borderRadius: 10, backgroundColor: "#FFFFFF", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center", borderColor: this.props.button.borderText, borderWidth: 1 }}>
                            <Image
                                style={{ height: scale(40), width: scale(40), }}
                                source={this.props.button.unfocused}
                                resizeMode="contain" />
                            <Text style={{ fontSize: scale(18), fontWeight: "bold", color: this.props.button.borderText }}>{this.props.button.label}</Text>
                        </View>
                }

            </TouchableOpacity>
        );
    }
}


class Entertainment extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: '  Entertainment  ',
        headerLeft: <HamburgerIcon />,
        headerBackground: <Header />,
        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: scale(20),
            fontFamily: "Roboto-Bold",
            fontWeight: "bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        },
    });
    constructor(props) {

        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.arrayholder = [];
        this.state = {
            text: '',
            expanded: false,
            onFocus: false,
            language:[{
                text:'हिंदी ',color:'#425CEA',id:'Hindi'
            },
            {
                text:'ENGLISH',color:'#228EED',id:'English'
            },
            {
                text:'తెలుగు',color:'#00C1F0',id:'Telugu'
            }],
            radioItems: [{
                label: 'Story Telling',
                borderText: "#F79132",
                color1: '#FC4A1A',
                color2: "#F79533",
                focused: require('../../assets/Group3627.png'),
                unfocused: require('../../assets/Group3606.png'),
                selected: false
            },

            {
                label: 'Rhymes',
                borderText: "#00C1EA",
                color1: '#00C1EA',
                color2: "#00E1F8",
                focused: require('../../assets/music-1.png'),
                unfocused: require('../../assets/music.png'),
                selected: false
            },

            {
                label: 'Lullabies',
                borderText: "#FF5159",
                color1: '#FF5058',
                color2: "#FF7478",
                focused: require('../../assets/asleep-1.png'),
                unfocused: require('../../assets/asleep.png'),
                selected: false
            },

            {
                label: 'Learning',
                borderText: "#00C7BD",
                color1: '#00C7BD',
                color2: "#00E9E8",
                focused: require('../../assets/abc-1.png'),
                unfocused: require('../../assets/abc.png'),
                selected: false
            },
            {
                label: 'Cartoons',
                borderText: "#4C1DE0",
                color1: '#4A00E0',
                color2: "#8E2DE2",
                focused: require('../../assets/XMLID_-1.png'),
                unfocused: require('../../assets/XMLID_1550_.png'),
                selected: false
            }], selectedItem: ''

        };
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    changeLayout = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ expanded: !this.state.expanded });
    }

    componentWillMount() {

        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Home');
        return true;
    }

    componentDidMount = async () => {
        this.state.radioItems.map((item) => {
            if (item.selected == true) {
                this.setState({ selectedItem: item.label });
            }
        });
        //await this.props.rhymes()
    }

    changeActiveRadioButton(index) {
        this.state.radioItems.map((item) => {
            item.selected = false;
        });

        this.state.radioItems[index].selected = true;

        this.setState({ radioItems: this.state.radioItems }, () => {
            this.setState({ selectedItem: this.state.radioItems[index].label });
        });
    }

    _click = (item, title) => {
        console.log('click', item);

        this.props.navigation.navigate('PlayVideo', { id: item, title: title });
    }

    _click_Language = (text,title) =>{
        this.props.navigation.navigate('Language',{ id: text, title:title });
    }

    SearchFilterFunction = (text) => {
        this.setState({
            text: text,
        });
    }

    render() {
        console.log('rhymes', this.props.Rhymesdata);
        console.log('this.props.Popular', this.props.Popular);
        const { text } = this.state;
        let Rhymes = this.props.Rhymes.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item.title ? item.title.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
            //return item.productName.toLowerCase().indexOf(text.toLowerCase()) !== -1
        });
        this.arrayholder = this.props.Rhymes

        if (this.props.loder) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }} />
                </View>
            );
        }
        return (
            <View style={styles.container}>
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}>
                    <View style={{ marginLeft: scale(20), marginTop: verticalScale(25) }}>
                        <View style={{ marginRight: scale(20), height: scale(48), backgroundColor: "#FFFFFF", borderRadius: 10, flexDirection: "row", alignItems: "center" }}>
                            <View style={{ justifyContent: "center", alignItems: "center", width: "20%", }}>
                                <Image
                                    style={{
                                        width: scale(19.5),
                                        height: scale(19.5),
                                    }}
                                    source={require('../../assets/magnifying-glass.png')}
                                />
                            </View>

                            <View style={{ justifyContent: "center", width: "80%", }}>
                                <TextInput style={{ width: "100%", height: scale(48), fontSize: scale(14) }}
                                    placeholder="Search"
                                    onChangeText={text => this.SearchFilterFunction(text)}
                                    value={this.state.text}
                                    onBlur={() => this.setState({ onFocus: false })}
                                    onFocus={() => this.setState({ onFocus: true })} />
                            </View>
                        </View>
                        {/* <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}
                            nestedScrollEnabled={true}>
                            <View style={{ marginTop: verticalScale(17), flexDirection: "row", marginRight: scale(20) }}>
                                <TouchableOpacity style={{ width: scale(193), height: scale(66), borderRadius: 10, backgroundColor: "#FFFFFF", flexDirection: "row", justifyContent: "space-evenly", alignItems: "center" }}>
                            <Image
                                style={{
                                    width: scale(19.5),
                                    height: scale(19.5),
                                }}
                                source={require('../../assets/magnifying-glass.png')}
                            />
                            <Text style={{ fontSize: scale(18), fontWeight: "bold" }}>Story Telling</Text>
                        </TouchableOpacity>

                                {
                                    this.state.radioItems.map((item, key) =>
                                        (
                                            <RadioButton key={key} button={item} index={key} onClick={this.changeActiveRadioButton.bind(this, key)} />
                                        ))
                                }
                            </View>
                        </ScrollView> */}
                        {this.state.text === '' ?
                            <View>
                                <View style={{
                                    marginTop: verticalScale(20),
                                    marginRight: scale(20),
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "space-between"
                                }}>
                                    <View style={{
                                        // width: "80%"
                                    }}>
                                        <Text style={{
                                            fontSize: scale(18),
                                            fontFamily: "Roboto-Bold",
                                            color: '#707070',
                                        }}>Popular Video</Text>
                                    </View>

                                    <View style={{
                                        // width: "20%"
                                    }}>
                                        <TouchableOpacity onPress={()=>this._click_Language("NAN","Popular")}>
                                            <Text style={{
                                                fontSize: scale(13),
                                                fontFamily: "Roboto-Regular",
                                                color: '#707070',
                                            }}>View All</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                <View style={{ marginRight: scale(20), }}>
                                    <FlatList
                                        data={this.props.Popular}
                                        horizontal={true}
                                        keyExtractor={(item, index) => index.toString()}
                                        renderItem={({ item, index }) => (
                                            <VideoCard item={item} index={index} OnClick={() => this._click(item.link, item.title)} />
                                        )} />
                                </View>

                                <View style={{ width: deviceWidth, backgroundColor: "#FFFFFF", marginLeft: scale(-20), marginTop: verticalScale(35), }}>

                                    <View style={{
                                        marginTop: verticalScale(5),
                                        marginRight: scale(20),
                                        marginLeft: scale(20),
                                        flexDirection: "row",
                                        alignItems: "center",
                                        justifyContent: "space-between"
                                    }}>
                                        <View style={{
                                            // width: "80%"
                                        }}>
                                            <Text style={{
                                                fontSize: scale(18),
                                                fontFamily: "Roboto-Bold",
                                                color: '#707070',
                                            }}>Browse By</Text>
                                        </View>

                                        {/* <View style={{
                                        // width: "20%"
                                    }}>
                                        <TouchableOpacity>
                                            <Text style={{
                                                fontSize: scale(13),
                                                fontFamily: "Roboto-Regular",
                                                color: '#707070',
                                            }}>View All</Text>
                                        </TouchableOpacity>
                                    </View> */}
                                    </View>
                                    <TouchableOpacity style={{ width: scale(140), height: scale(45), borderRadius: 10, marginLeft: scale(20), marginTop: verticalScale(15) }}
                                        onPress={this.changeLayout}>
                                        <LinearGradient
                                            colors={['#425CEA', '#00C1F0']}
                                            style={{ width: scale(140), height: scale(45), borderRadius: 10, alignItems: "center", justifyContent: "center" }}
                                            start={{ x: 0, y: 1 }}
                                            end={{ x: 1, y: 0 }}>
                                            <Text style={{
                                                fontSize: scale(18),
                                                fontFamily: "Roboto-Regular",
                                                color: '#FFFFFF',
                                                fontWeight: "bold"
                                            }}>Language</Text>
                                        </LinearGradient>
                                    </TouchableOpacity>
                                    <View style={{ marginTop: verticalScale(15) }} />
                                    <View style={{ height: this.state.expanded ? null : 0, overflow: 'hidden' }}>
                                        <ScrollView
                                            horizontal={true}
                                            showsHorizontalScrollIndicator={false}
                                            showsVerticalScrollIndicator={false}
                                            nestedScrollEnabled={true}>
                                            <View style={{ flexDirection: "row",marginRight: scale(20) }}>

                                            {this.state.language.map((item,index)=>(
                                                    <TouchableOpacity style={{ width: scale(146), height: scale(85), borderRadius: 10, marginLeft: scale(20), backgroundColor: item.color, alignItems: "center", justifyContent: "center" }}
                                                    onPress={()=>this._click_Language(item.id,"Browseby")}>
                                                        <Text style={{
                                                            fontSize: scale(30),
                                                            fontFamily: "Roboto-Regular",
                                                            color: '#FFFFFF',
                                                            
                                                        }}>{item.text}</Text>
                                                    </TouchableOpacity>
                                                    ))}

                                            </View>
                                        </ScrollView>
                                        <View style={{marginTop:verticalScale(15)}}/>
                                    </View>


                                </View>

                                <View style={{
                                    marginTop: verticalScale(20),
                                    marginRight: scale(20),
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "space-between"
                                }}>
                                    <View style={{
                                        // width: "80%"
                                    }}>
                                        <Text style={{
                                            fontSize: scale(18),
                                            fontFamily: "Roboto-Bold",
                                            color: '#707070',
                                        }}>For you</Text>
                                    </View>

                                    <View style={{
                                        // width: "20%"
                                    }}>
                                        <TouchableOpacity onPress={()=>this._click_Language("NAN","Foryou")}> 
                                            <Text style={{
                                                fontSize: scale(13),
                                                fontFamily: "Roboto-Regular",
                                                color: '#707070',
                                            }}>View All</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                <View style={{ marginRight: scale(20), }}>
                                    <FlatList
                                        data={this.props.ForYou}
                                        horizontal={true}
                                        keyExtractor={(item, index) => index.toString()}
                                        renderItem={({ item, index }) => (
                                            <VideoCard item={item} index={index} OnClick={() => this._click(item.link, item.title)} />
                                        )} />
                                </View>

                                <View style={{
                                    marginTop: verticalScale(20),
                                    marginRight: scale(20),
                                    flexDirection: "row",
                                    alignItems: "center",
                                    justifyContent: "space-between"
                                }}>
                                    <View style={{
                                        // width: "80%"
                                    }}>
                                        <Text style={{
                                            fontSize: scale(18),
                                            fontFamily: "Roboto-Bold",
                                            color: '#707070',
                                        }}>Trending Now</Text>
                                    </View>

                                    <View style={{
                                        // width: "20%"
                                    }}>
                                        <TouchableOpacity onPress={()=>this._click_Language("NAN","Trending")}>
                                            <Text style={{
                                                fontSize: scale(13),
                                                fontFamily: "Roboto-Regular",
                                                color: '#707070',
                                            }}>View All</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                <View style={{ marginRight: scale(20), }}>
                                    <FlatList
                                        data={this.props.TrendingNow}
                                        horizontal={true}
                                        keyExtractor={(item, index) => index.toString()}
                                        renderItem={({ item, index }) => (
                                            <VideoCard item={item} index={index} OnClick={() => this._click(item.link, item.title)} />
                                        )} />
                                </View>
                            </View> : null}

                    </View>
                    {this.state.text != '' ?
                        <FlatList
                            data={Rhymes}
                            extraData={this.state}
                            enableEmptySections={true}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity style={{ width: deviceWidth, height: scale(280), marginTop: verticalScale(20), backgroundColor: 'transparent', }}
                                    onPress={() => this._click(item.link, item.title)}>
                                    <ImageBackground style={{ height: scale(229), width: deviceWidth, justifyContent: "center", alignItems: "center" }}
                                        source={{ uri: `https://i.ytimg.com/vi/` + item.link + `/mqdefault.jpg` }}
                                        resizeMode="cover">
                                        <Image
                                            style={{ width: scale(58), height: scale(58), }}
                                            source={require('../../assets/Buttons-Play.png')}
                                            resizeMode="cover"
                                        />
                                        <View style={{ backgroundColor: "#000000", borderRadius: 8, width: scale(40), height: scale(20), alignSelf: "flex-end", right: scale(7), bottom: scale(7), position: 'absolute', justifyContent: "center", alignItems: "center" }}>
                                            <Text style={{ color: "#FFFFFF", fontSize: scale(12) }}>{item.length}</Text>
                                        </View>
                                    </ImageBackground>
                                    <Text style={{ color: "#000000", fontSize: scale(17), marginTop: verticalScale(15), marginLeft: scale(20) }} numberOfLines={1}>{item.title}</Text>
                                </TouchableOpacity>

                            )} /> : null}

                    <View style={{ height: scale(20) }} />

                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f7f9',
    },
})

const Populardata = (video) => {

    const res = video;

    let data = [];
    for (let key in res) {
        const Object = {};
        for (var i = 0; i < res[key].length; i++) {
            Object = res[key][i]
            data.push(Object);
        }
    }

    let PopularVideo = [];

    for (var i = 0; i < 50; i++) {
        const rand = {}
        rand = data[Math.floor(Math.random() * data.length)];
        for (var j = 0; j < data.length; j++) {
            if (rand === data[j]) {
                data.splice(j, 1);
            }
        }
        PopularVideo.push(rand);
    }

    var data2 = PopularVideo
    console.log("PopularVideo", PopularVideo);

    return data;
}

const ForYourdata = (video) => {

    const res = video;

    let data = [];
    for (let key in res) {
        const Object = {};
        for (var i = 0; i < res[key].length; i++) {
            Object = res[key][i]
            data.push(Object);
        }
    }

    let PopularVideo = [];

    for (var i = 0; i < 50; i++) {
        const rand = {}
        rand = data[Math.floor(Math.random() * data.length)];
        for (var j = 0; j < data.length; j++) {
            if (rand === data[j]) {
                data.splice(j, 1);
            }
        }
        PopularVideo.push(rand);
    }

    var data2 = PopularVideo
    console.log("PopularVideo", PopularVideo);

    return data;
}

const TrendingNowdata = (video) => {

    const res = video;

    let data = [];
    for (let key in res) {
        const Object = {};
        for (var i = 0; i < res[key].length; i++) {
            Object = res[key][i]
            data.push(Object);
        }
    }

    let PopularVideo = [];

    for (var i = 0; i < 50; i++) {
        const rand = {}
        rand = data[Math.floor(Math.random() * data.length)];
        for (var j = 0; j < data.length; j++) {
            if (rand === data[j]) {
                data.splice(j, 1);
            }
        }
        PopularVideo.push(rand);
    }

    var data2 = PopularVideo
    console.log("PopularVideo", PopularVideo);

    return data;
}
const Rhymesdata = (video) => {

    const res = video;

    let data = [];
    for (let key in res) {
        const Object = {};
        for (var i = 0; i < res[key].length; i++) {
            Object = res[key][i]
            data.push(Object);
        }
    }

    return data;
}

function mapStateToProps(state) {
    let selectedChild = state.userinfo.selectedChild
    return {
        user: state.userinfo.user[selectedChild],
        loder: state.loder,
        Popular: Populardata(state.Rhymes.Rhymesdata),
        ForYou: ForYourdata(state.Rhymes.Rhymesdata),
        TrendingNow: TrendingNowdata(state.Rhymes.Rhymesdata),
        Rhymes: Rhymesdata(state.Rhymes.Rhymesdata)

    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes })(Entertainment)