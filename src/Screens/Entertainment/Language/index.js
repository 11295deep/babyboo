import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    PixelRatio,
    ImageBackground,
    SafeAreaView,
    Image,
    FlatList,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    BackHandler,
    Switch,
    RefreshControl,
    Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes, login } from '../../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../../../Components/Header';
import Spinner from "react-native-spinkit";
import firebase from 'react-native-firebase';
import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
} from 'react-native-youtube';
import { HeaderBackButton } from 'react-navigation-stack';
import HamburgerIcon from "../../../Components/HamburgerIcon"
import { BackgroundCarousel } from "../../../Components/BackgroundCarousel";
import { scale, moderateScale, verticalScale } from '../../../Components/Scale';

const screenDimension = Dimensions.get('window');
const isBigDevice = screenDimension.height > 600;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const shadowStyle = {
    shadowColor: '#000',
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
};
class Language extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: '  Entertainment  ',
        headerLeft: <HamburgerIcon />,
        headerBackground: <Header />,
        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: scale(20),
            fontFamily: "Roboto-Bold",
            fontWeight: "bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        },
    });
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            id: this.props.navigation.getParam('id', 'NO-User'),
            title: this.props.navigation.getParam('title', 'NO-User'),
        };
    }
    componentWillMount() {

        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Entertainment');
        return true;
    }

    _click = (item, title) => {
        console.log('click', item);
        this.props.navigation.navigate('PlayVideo', { id: item, title: title });
    }

    render() {
        console.log('rh', this.props.Rhymes);


        if (this.props.loder) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }} />
                </View>
            );
        }

        return (
            <View style={styles.container}>

                <View style={{
                    marginTop: verticalScale(20),
                    marginRight: scale(20),
                    marginLeft: scale(20),
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between"
                }}>
                    <View style={{
                        // width: "80%"
                    }}>
                        <Text style={{
                            fontSize: scale(18),
                            fontFamily: "Roboto-Bold",
                            color: '#707070',
                        }}>{this.state.title === "Browseby" ? "Browse By Language" : this.state.title === "Popular" ? "Popular Rhymes" : this.state.title === "Foryou" ? "For you" : this.state.title === "Trending" ? "Trending Now": null}</Text>
                    </View>
                </View>
                <View style={{marginTop:verticalScale(15)}}/>
                <FlatList
                    data={this.state.id === "NAN"? this.props.RhymesView : this.props.Rhymes[this.state.id]}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => (
                        <TouchableOpacity style={{ width: deviceWidth, height: scale(280), marginTop: index === 0 ? null : verticalScale(15), backgroundColor: 'transparent', }}
                            onPress={() => this._click(item.link, item.title)}>
                            <ImageBackground style={{ height: scale(229), width: deviceWidth, justifyContent: "center", alignItems: "center" }}
                                source={{ uri: 'https://i.ytimg.com/vi/'+item.link+'/mqdefault.jpg' }}
                                resizeMode="cover">
                                <Image
                                    style={{ width: scale(58), height: scale(58), }}
                                    source={require('../../../assets/Buttons-Play.png')}
                                    resizeMode="cover"
                                />
                                <View style={{ backgroundColor: "#000000", borderRadius: 8, width: scale(40), height: scale(20), alignSelf: "flex-end", right: scale(7), bottom: scale(7), position: 'absolute', justifyContent: "center", alignItems: "center" }}>
                                    <Text style={{ color: "#FFFFFF", fontSize: scale(12) }}>{item.length}</Text>
                                </View>
                            </ImageBackground>
                            <Text style={{ color: "#000000", fontSize: scale(17), marginTop: verticalScale(15), marginLeft: scale(20) }} numberOfLines={1}>{item.title}</Text>
                        </TouchableOpacity>

                    )} />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f7f9',
    },

})

const Rhymesdata = (video) => {

    const res = video;

    let data = [];
    for (let key in res) {
        const Object = {};
        for (var i = 0; i < res[key].length; i++) {
            Object = res[key][i]
            data.push(Object);
        }
    }
    let PopularVideo = [];
    for (var i = 0; i < 62; i++) {
        const rand = {}
        rand = data[Math.floor(Math.random() * data.length)];
        for (var j = 0; j < data.length; j++) {
            if (rand === data[j]) {
                data.splice(j, 1);
            }
        }
        PopularVideo.push(rand);
    }

    return PopularVideo;
}


function mapStateToProps(state) {
    let selectedChild = state.userinfo.selectedChild
    return {
        user: state.userinfo.user[selectedChild],
        loder: state.loder,
        Rhymes: state.Rhymes.Rhymesdata,
        RhymesView: Rhymesdata(state.Rhymes.Rhymesdata)

    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes })(Language)