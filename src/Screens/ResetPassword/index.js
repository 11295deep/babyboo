// http://34.207.213.121:3001/resetPassword
// {"newPassword": "12345678", "currentPassword": "1234567"}

import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator,
    BackHandler,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import { resetPassword, } from '../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import Header from "../../Components/Header";
import LinearGradient from 'react-native-linear-gradient';
import { HeaderBackButton } from 'react-navigation-stack';
import { scale, moderateScale, verticalScale } from '../../Components/Scale';
const screenDimension = Dimensions.get('window');
const isBigDevice = screenDimension.height > 600;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import axios from 'axios';
// import url from "./Config/API"

const shadowStyle = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
}

// const changepwd = async (data) => {
//     try {
//         const response = await axios.post(`${url}/resetPassword`, data);
//         return response;
//     } catch (e) {
//         console.log("hi");
//         console.log(e);
//         if (e == "Error: Request failed with status code 403"){
//             Alert.alert("Incorrecr Old Password");
//         }else{
//             Alert.alert('Connection Error','Unable to connect to the Internet');
//         }
//         //alert(e);
//         this.alertPresent = true;
//         return false;
//     }
// };


class ResetPassword extends React.Component {

    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            old_pwd: '',
            new_pwd: '',
            error: null,
            confirm_pwd: '',

        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Home');
        return true;
    }

    _pwd = async () => {
        var {
            old_pwd,
            new_pwd,
            confirm_pwd
        } = this.state;
        const data = {
            "newPassword": confirm_pwd,
            "currentPassword": old_pwd
        }

        if (new_pwd === confirm_pwd) {
            const response = await this.props.resetPassword(data)
            console.log('res', response);
            if(response.status == 200) {
                Alert.alert('Your password has been changed successfully')
            this.props.navigation.navigate('Home')
            }
        } 
        else{
            this.setState({error: "Password don't match!"})
        }
    }

    render() {

        return (
            <ImageBackground
                style={{ flex: 1 }}
                source={require('../../assets/background.png')}>
                <ScrollView showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}>
                    <View style={{ marginTop: verticalScale(15), marginLeft: scale(25), flexDirection: "row" }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                            <Image
                                style={{ width: scale(20), height: scale(20), borderWidth: 0, marginTop: verticalScale(5) }}
                                source={require('../../assets/arrow.png')}
                                resizeMode='contain'
                            />
                        </TouchableOpacity>
                        <Text style={{ fontSize: scale(20), fontFamily:"Roboto-Bold",
      fontWeight:"bold", marginLeft: scale(20), color: "#707070" }}>Change Password</Text>
                    </View>
                    <View style={styles.container}>
                        <Image
                            style={{ width: scale(130), height: scale(130), borderWidth: 0, marginTop:verticalScale(50) }}
                            source={require('../../assets/pwdlogo.png')}
                            resizeMode='contain'
                        />
                        <View style={{ marginTop: verticalScale(20), alignSelf: "flex-start", marginLeft: scale(20) }}>
                            <Text style={{ fontSize: scale(15), fontWeight: "bold", marginLeft: scale(12), color: "#707070" }}></Text>
                        </View>
                        <View style={{ marginTop: verticalScale(11), alignSelf: "flex-start", marginLeft: scale(20) }}>
                            <Text style={{ fontSize: scale(11), marginLeft: scale(12), color: "#707070" }}></Text>
                        </View>

                        <View style={[styles.SectionStyle, shadowStyle]}>
                            <TextInput
                                style={[styles.input]}
                                placeholder="Old Password"
                                autoCapitalize="none"
                                keyboardType="default"
                                onChangeText={(old_pwd) => this.setState({ old_pwd })}
                                value={this.state.old_pwd} />
                        </View>

                        <View style={[styles.SectionStyle, shadowStyle]}>
                            <TextInput
                                style={[styles.input]}
                                placeholder="New Password"
                                autoCapitalize="none"
                                keyboardType="default"
                                onChangeText={(new_pwd) => this.setState({ new_pwd })}
                                value={this.state.new_pwd} />
                        </View>

                        <View style={[styles.SectionStyle, shadowStyle]}>
                            <TextInput
                                style={[styles.input]}
                                placeholder="Confirm Password"
                                autoCapitalize="none"
                                keyboardType="default"
                                onChangeText={(confirm_pwd) => this.setState({ confirm_pwd })}
                                value={this.state.confirm_pwd} />
                        </View>
                        <Text style={{ color: "red",marginTop:verticalScale(5) }}>{this.state.error}</Text>

                        <TouchableOpacity
                            style={[styles.btnSignin, shadowStyle]}
                            onPress={this._pwd}>
                            <Text style={{ color: '#a076e8', fontFamily:"Roboto-Bold",
      fontWeight:"bold", fontSize: scale(15) }}>Change Password</Text>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        margin: scale(15),
        height: scale(50),
        width: scale(300),
        paddingLeft: scale(20),
        fontSize: scale(15),
        borderRadius: 10,
        backgroundColor: '#fff',
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        borderRadius: 10,
        height: scale(50),
        width: scale(300),
        margin: scale(20),
        marginTop: verticalScale(10)
    },
    btnSignin: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        marginLeft: scale(20),
        borderRadius: 10,
        marginTop: verticalScale(20),
        width: scale(300),
        marginRight: scale(20),
        padding: scale(10),
        height: scale(50),
    },
})

function mapStateToProps(state) {
    return {
        // otpdata: state.otp.otpdata,
        // userdata: state.register.userdata,
    }
}

export default connect(mapStateToProps, { resetPassword, })(ResetPassword);