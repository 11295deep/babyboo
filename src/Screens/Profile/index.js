import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator,
    BackHandler,
    RefreshControl
} from 'react-native';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, changeSelectedChild, upload_Image } from '../../Action'
import { scale, moderateScale, verticalScale } from '../../Components/Scale';
import ImagePicker from 'react-native-image-picker';
import HamburgerIcon from '../../Components/HamburgerIcon';
import { HeaderBackButton } from 'react-navigation-stack';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../../Components/Header';
const screenDimension = Dimensions.get('window');
const isBigDevice = screenDimension.height > 600;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import axios from 'axios';
// import HamburgerIcon from './HamburgerIcon';
import Spinner from "react-native-spinkit";

class RadioButton extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onClick} activeOpacity={0.8} style={styles.radioButton}>
                {
                    (this.props.button.selected)
                        ?
                        (<View style={[styles.radioIcon, { height: scale(159), width: scale(159), borderColor: "#A173E8", borderWidth: 5 }]}>
                            <Image
                                style={{ height: scale(154), width: scale(154), borderRadius: 400 }}
                                source={{ uri: this.props.button.image }}
                                resizeMode="cover" />
                            <TouchableOpacity onPress={this.props.onPress} activeOpacity={0.8}
                                style={{ position: 'absolute', left: scale(120), alignSelf: "flex-start", width: scale(30), height: scale(30), borderRadius: 400, backgroundColor: "#A173E8", justifyContent: "center", alignItems: "center" }}>
                                <Image
                                    style={{ height: scale(9.79), width: scale(13.18) }}
                                    source={require('../../assets/camerawhite.png')}
                                    resizeMode="center" />
                            </TouchableOpacity>
                        </View>)
                        :
                        <View style={[styles.radioIcon, { height: scale(110), width: scale(110), borderColor: "transparent", borderWidth: 0 }]}>
                            <Image
                                style={{ height: scale(110), width: scale(110), borderRadius: 400 }}
                                source={{ uri: this.props.button.image }}
                                resizeMode="cover" />
                        </View>
                }
                <Text style={{ color: this.props.button.selected ? "#000" : "#707070", marginTop: verticalScale(11), alignSelf: "center", fontSize: this.props.button.selected ? scale(23) : scale(16) }}>{this.props.button.name}</Text>
                <Text style={{ color: this.props.button.selected ? "#000" : "#707070", marginTop: verticalScale(3), alignSelf: "center", fontSize: this.props.button.selected ? scale(15) : scale(8) }}>{this.props.button.gender}</Text>
            </TouchableOpacity>
        );
    }
}


class Profile extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: '  Profile  ',
       // headerLeft: <HamburgerIcon />,
       headerLeft: (
        <HeaderBackButton onPress={() => navigation.navigate('Home')} />
    ),
        headerBackground: <Header />,
        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: scale(20),
            fontFamily: "Roboto-Bold",
            fontWeight: "bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        },
    });

    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            dataSource: [],
            isLoading: true,
            // photo: "",
            selectedchild: "",
            sel: '',
        }
    }

    componentWillMount() {

        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Home');
        return true;
    }

    changeActiveRadioButton = async (index) => {
        console.log('index', index);
        this.props.changeSelectedChild(index)
    }

    handleChoosePhoto = () => {
        // const options = {
        //   noData: true,
        // }
        var options = {
            title: 'Select Image',
            // customButtons: [
            //     { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
            // ],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            console.log('image res', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else if (response.uri) {
                console.log("photores", response);

                this.setState({ photo: response }, () => this.handleUploadPhoto());
                //() => this.handleUploadPhoto()
            }
        })
    }
    createFormData = (photo, body) => {
        const data = new FormData();

        data.append("photo", {
            name: photo.fileName,
            type: photo.type,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });

        Object.keys(body).forEach(key => {
            data.append(key, body[key]);
        });
        console.log('image data', data);
        return data;
    };
    handleUploadPhoto = async () => {
        try {
            let response;
            
            if (this.props.selectedChild === 0) {
                console.log('hiiii',this.createFormData(this.state.photo, { childName: "0" }));

                response = await this.props.upload_Image(this.createFormData(this.state.photo, { index: 0}));
            } else if (this.props.selectedChild === 1) {
                response = await this.props.upload_Image(this.createFormData(this.state.photo, { index: 1}));
            }
            if (response.status === 200) {
                const respose = await this.props.userinfo()
            }

            console.log('rrr', response);
        }
        catch (e) {
            console.log('error image upload', e);
        }
    };

    render() {
        console.log('profilescren');
        
        if (this.props.loder) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }} />
                </View>
            );
        }
        return (
            <View style={styles.container}>
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                // refreshControl={
                //     <RefreshControl
                //         //refresh control used for the Pull to Refresh
                //         refreshing={this.state.isLoading}
                //         onRefresh={this.onRefresh.bind(this)}
                //         colors={['#a076e8', '#5dc4dd']}
                //     />
                // }
                >

                    <View style={{ marginTop: verticalScale(32), height: scale(256), flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginLeft: scale(30), marginRight: scale(30) }}>
                        {
                            this.props.profile.map((item, key) =>
                                (
                                    <View style={{ justifyContent: "space-between" }}>
                                        <RadioButton key={key} button={item} onClick={this.changeActiveRadioButton.bind(this, key)}
                                            onPress={this.handleChoosePhoto} />
                                    </View>
                                ))
                        }

{/* <View style={[styles.radioIcon, { height: scale(110), width: scale(110), borderColor: "transparent", borderWidth: 0 }]}>
                            <Image
                                style={{ height: scale(110), width: scale(110), borderRadius: 400 }}
                                source={{ uri: this.props.button.image }}
                                resizeMode="cover" />
                        </View> */}

                        {this.props.profile.length != 2 ?
                            <View style={{ flexDirection: "row", justifyContent: "space-between", }}>
                                <TouchableOpacity style={{ flexDirection: "column", alignItems: "center" }}
                                    onPress={() => this.props.navigation.navigate('RegisterChild')}>
                                    <View style={{ height: scale(110), width: scale(110), borderRadius: 400, backgroundColor: "#fff", justifyContent: "center", alignItems: "center" }}>
                                        <Image
                                            style={{ width: scale(25), height: scale(25) }}
                                            source={require('../../assets/more.png')}
                                            resizeMode='cover'
                                        />
                                    </View>
                                    <Text style={{ fontSize: scale(14), color: "#000", fontFamily: "Roboto-Bold", marginTop: verticalScale(13) }}>ADD</Text>
                                </TouchableOpacity>
                            </View> : null}
                    </View>

                    <View style={{ marginTop: verticalScale(16), height: scale(50), marginLeft: scale(20), marginRight: scale(20), backgroundColor: "#FFFFFF", borderRadius: 10, flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                        <View style={{
                            width: "15%",
                            backgroundColor: "transparent",
                            borderRadius: 400,
                            marginLeft: scale(15)
                        }}>
                            <LinearGradient
                                start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                                colors={['#a076e8', '#5dc4dd']}
                                style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center", }}>
                                <Image
                                    style={{
                                        width: scale(16.44),
                                        height: scale(15.49),
                                    }}
                                    source={require('../../assets/cake.png')}
                                />
                            </LinearGradient>
                        </View>
                        <View style={{
                            width: "40%",
                            height: scale(50), justifyContent: "center"
                        }}>
                            <Text style={{ fontSize: scale(16), fontFamily: "Roboto-Regular", }}>Date of Birth</Text>
                        </View>
                        <View style={{
                            width: "40%",
                            height: scale(50), justifyContent: "center"
                        }}>
                            <Text style={{ fontSize: scale(16), fontFamily: "Roboto-Regular", }}>{this.props.dob}</Text>
                        </View>
                    </View>

                    <View style={{ marginTop: verticalScale(20), height: scale(109), marginLeft: scale(20), marginRight: scale(20), backgroundColor: "#FFFFFF", borderRadius: 10, flexDirection: "row", }}>
                        <View style={{
                            width: "15%",
                            backgroundColor: "transparent",
                            borderRadius: 400,
                            marginLeft: scale(15),
                            marginTop: verticalScale(10)
                        }}>
                            <LinearGradient
                                start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                                colors={['#a076e8', '#5dc4dd']}
                                style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center", }}>
                                <Image
                                    style={{
                                        width: scale(16.44),
                                        height: scale(15.49),
                                    }}
                                    source={require('../../assets/injection2.png')}
                                />
                            </LinearGradient>
                        </View>
                        <View style={{
                            width: "40%",
                            height: scale(109), marginTop: verticalScale(10),
                            flexDirection: "column"
                        }}>
                            <Text style={{ fontSize: scale(18), fontFamily: "Roboto-Regular", }}>Vaccines</Text>

                            <View style={{ marginTop: verticalScale(21) }}>
                                <Text style={{ fontSize: scale(12), fontFamily: "Roboto-Regular", }}>Next Due Vaccine</Text>
                                <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#A076E8" }}>{this.props.nextVaccine[`0`].datenum} {this.props.nextVaccine[`0`].month}, {this.props.nextVaccine[`0`].year}</Text>
                                <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Regular", color: "#707070" }}>{this.props.nextVaccine[`0`].day}</Text>
                            </View>
                        </View>
                        <View style={{
                            width: "40%",
                            height: scale(109), justifyContent: "center",
                        }}>
                            <AnimatedCircularProgress
                                size={scale(89.82)}
                                width={scale(10)}
                                backgroundWidth={scale(10)}
                                fill={this.props.completePerc}
                                tintColor="#A076E8"
                                arcSweepAngle={360}
                                rotation={360}
                                backgroundColor="#DFF2FF"
                                lineCap="round">
                                {(fill) => (
                                    <View>
                                        <Text style={{ fontSize: scale(18), fontFamily: "Roboto-Bold", alignSelf: "center", color: "#A076E8" }}>{this.props.completeVaccines}/14</Text>
                                        <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Bold", alignSelf: "center", color: "#707070" }}>Completed</Text>
                                    </View>
                                )}
                            </AnimatedCircularProgress>


                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f7f9',
        justifyContent: "center",
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        alignSelf: "center",
        height: scale(50),
        width: scale(300),
        borderRadius: 10,
        marginTop: verticalScale(30)
    },
    icon: {
        backgroundColor: '#ccc',
        position: 'absolute',
        right: 0,
        bottom: 0
    },
    radioButton:
    {
        flexDirection: 'column',
        //marginLeft: scale(45),
        //margin: scale(10),
        alignItems: 'center',
        //justifyContent: 'center'
    },

    radioButtonHolder:
    {
        borderRadius: 50,
        borderWidth: 2,
        marginLeft: scale(50),
        flexDirection: "column",
        justifyContent: 'center',
        alignItems: 'center'
    },

    radioIcon:
    {
        borderRadius: 154,
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: 'center'
    },

    label:
    {
        fontSize: scale(14)
    },
})

const birthdate = (dob) => {
    var dateString = dob;

    var dateParts = dateString.split("/");
    var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    var month1 = new Array();
    month1[0] = "Jan";
    month1[1] = "Feb";
    month1[2] = "Mar";
    month1[3] = "Apr";
    month1[4] = "May";
    month1[5] = "Jun";
    month1[6] = "Jul";
    month1[7] = "Aug";
    month1[8] = "Sept";
    month1[9] = "Oct";
    month1[10] = "Nov";
    month1[11] = "Dec";
    var month = month1[dateObject.getMonth()]
    var datenum = dateObject.getDate()
    var year = dateObject.getUTCFullYear()

    var dob = datenum + " " + month + ", " + year

    return dob;
}

const calculateVaccines = (childVaccines) => {
    console.log('omm', childVaccines);

    const res = childVaccines;
    console.log('222', res);

    let data = [];
    for (let key in res.childVaccines) {
        const vaccineObject = {};
        var com = 0;
        res.childVaccines[key].vaccines.forEach((vaccine, index) => {
            vaccineObject[`vaccine${index + 1}`] = vaccine.name;
            if (vaccine.givenDate !== "") {
                com = com + 1;
            }
            vaccineObject[`complete_count`] = com;
        });
        vaccineObject['date'] = res.childVaccines[key].dueDate;
        vaccineObject['name'] = res.child.name;
        vaccineObject['duration'] = key;
        vaccineObject[`count`] = res.childVaccines[key].vaccines.length;

        var dateString1 = res.childVaccines[key].dueDate;
        var dateParts1 = dateString1.split("/");
        var dateObject1 = new Date(+dateParts1[2], dateParts1[1] - 1, +dateParts1[0]);
        var weekday = new Array(7);
        weekday[0] = "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";
        vaccineObject['day'] = weekday[dateObject1.getDay()]
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        vaccineObject['month'] = month[dateObject1.getMonth()]
        vaccineObject['datenum'] = dateObject1.getDate()
        vaccineObject['year'] = dateObject1.getUTCFullYear()

        data.push(vaccineObject);
    }
    console.log('datahome.....', data);

    var give = 0;
    for (var i = 0; i < data.length; i++) {
        if (data[i].count === data[i].complete_count) {
            give = give + 1;
        }
    }
    console.log('giveprofile', give)
    return give;
}

const calculatePerc = (childVaccines) => {

    const res = childVaccines;

    let data = [];
    for (let key in res.childVaccines) {
        const vaccineObject = {};
        var com = 0;
        res.childVaccines[key].vaccines.forEach((vaccine, index) => {
            vaccineObject[`vaccine${index + 1}`] = vaccine.name;
            if (vaccine.givenDate !== "") {
                com = com + 1;
            }
            vaccineObject[`complete_count`] = com;
        });
        vaccineObject['date'] = res.childVaccines[key].dueDate;
        vaccineObject['name'] = res.child.name;
        vaccineObject['duration'] = key;
        vaccineObject[`count`] = res.childVaccines[key].vaccines.length;

        var dateString1 = res.childVaccines[key].dueDate;
        var dateParts1 = dateString1.split("/");
        var dateObject1 = new Date(+dateParts1[2], dateParts1[1] - 1, +dateParts1[0]);
        var weekday = new Array(7);
        weekday[0] = "Sunday";
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";
        vaccineObject['day'] = weekday[dateObject1.getDay()]
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";
        vaccineObject['month'] = month[dateObject1.getMonth()]
        vaccineObject['datenum'] = dateObject1.getDate()
        vaccineObject['year'] = dateObject1.getUTCFullYear()

        data.push(vaccineObject);
    }
    console.log('datahome.....', data);

    var give = 0;
    for (var i = 0; i < data.length; i++) {
        if (data[i].count === data[i].complete_count) {
            give = give + 1;
        }
    }
    console.log('give', give)
    var pos = 14;
    var perc = ((give / pos) * 100).toFixed(2);
    return perc;
}

const calculateNextVaccine = (childVaccines) => {

    // const res = childVaccines;

    // let data = [];
    // for (let key in res.childVaccines) {
    //     const vaccineObject = {};
    //     var com = 0;
    //     res.childVaccines[key].vaccines.forEach((vaccine, index) => {
    //         vaccineObject[`vaccine${index + 1}`] = vaccine.name;
    //         if (vaccine.givenDate !== "") {
    //             com = com + 1;
    //         }
    //         vaccineObject[`complete_count`] = com;
    //     });
    //     vaccineObject['date'] = res.childVaccines[key].dueDate;
    //     vaccineObject['name'] = res.child.name;
    //     vaccineObject['duration'] = key;
    //     vaccineObject[`count`] = res.childVaccines[key].vaccines.length;

    //     var dateString1 = res.childVaccines[key].dueDate;
    //     var dateParts1 = dateString1.split("/");
    //     var dateObject1 = new Date(+dateParts1[2], dateParts1[1] - 1, +dateParts1[0]);
    //     var weekday = new Array(7);
    //     weekday[0] = "Sunday";
    //     weekday[1] = "Monday";
    //     weekday[2] = "Tuesday";
    //     weekday[3] = "Wednesday";
    //     weekday[4] = "Thursday";
    //     weekday[5] = "Friday";
    //     weekday[6] = "Saturday";
    //     vaccineObject['day'] = weekday[dateObject1.getDay()]
    //     var month = new Array();
    //     month[0] = "January";
    //     month[1] = "February";
    //     month[2] = "March";
    //     month[3] = "April";
    //     month[4] = "May";
    //     month[5] = "June";
    //     month[6] = "July";
    //     month[7] = "August";
    //     month[8] = "September";
    //     month[9] = "October";
    //     month[10] = "November";
    //     month[11] = "December";
    //     vaccineObject['month'] = month[dateObject1.getMonth()]
    //     vaccineObject['datenum'] = dateObject1.getDate()
    //     vaccineObject['year'] = dateObject1.getUTCFullYear()

    //     data.push(vaccineObject);
    // }
    // console.log('datahome.....', data);

    // let finalData = [];
    // for (var i = 0; i < data.length; i++) {
    //     const v = {};
    //     if (data[i].count != data[i].complete_count) {
    //         finalData.push(data[i]);
    //     }
    // }
    // console.log('final', finalData);

    // return finalData;
    const res = childVaccines;
        
  let data = [];
  for (let key in res.childVaccines) {
    const vaccineObject = {};
    var com = 0;
    res.childVaccines[key].vaccines.forEach((vaccine, index) => {
      vaccineObject[`vaccine${index + 1}`] = vaccine.name;
      if (vaccine.givenDate !== "") {
        com = com + 1;
      }
      vaccineObject[`complete_count`] = com;
    });
    vaccineObject['date'] = res.childVaccines[key].dueDate;
    vaccineObject['name'] = res.child.name;
    vaccineObject['duration'] = key;
    vaccineObject[`count`] = res.childVaccines[key].vaccines.length;

    var dateString1 = res.childVaccines[key].dueDate;
    var dateParts1 = dateString1.split("/");
    var dateObject1 = new Date(+dateParts1[2], dateParts1[1] - 1, +dateParts1[0]);
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    vaccineObject['day'] = weekday[dateObject1.getDay()]
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    vaccineObject['month'] = month[dateObject1.getMonth()]
    vaccineObject['datenum'] = dateObject1.getDate()
    vaccineObject['year'] = dateObject1.getUTCFullYear()

    data.push(vaccineObject);
  }

  let final = [];
  for (var j = 0; j < data.length; j++) {
    var dateString = data[j].date
    var dateParts = dateString.split("/");
    var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    var g1 = new Date();
    var g2 = new Date(dateObject);
    if (g1.getTime() < g2.getTime()) {
      final.push(data[j])
    }
  }

  return final;
}

const profiledata = (data, selectedChild) => {
    const Sourcedata = data;
    let profile = []
    for (var i = 0; i < Sourcedata.length; i++) {
        const dataObject = {};
        console.log('ll')
        dataObject['name'] = Sourcedata[i].child.name;
        dataObject['gender'] = Sourcedata[i].child.gender === "m" ? "Baby Boy" : "Baby Girl";
        dataObject['image'] = `${Sourcedata[i].imageUrl}?time=${new Date()}`;
        dataObject['selected'] = i == selectedChild ? true : false
        profile.push(dataObject);
    }
    console.log('source', profile);

    return profile;
}

function mapStateToProps(state) {
    let selectedChild = state.userinfo.selectedChild
    return {
        // user: state.userinfo.user,
        selectedChild: state.userinfo.selectedChild,
        dob: birthdate(state.userinfo.user[selectedChild].child.dob),
        completeVaccines: calculateVaccines(state.userinfo.user[selectedChild]),
        completePerc: calculatePerc(state.userinfo.user[selectedChild]),
        nextVaccine: calculateNextVaccine(state.userinfo.user[selectedChild]),
        profile: profiledata(state.userinfo.user, selectedChild),
        loder: state.loder,

    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, changeSelectedChild, upload_Image })(Profile)