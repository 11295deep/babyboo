import React, { Component } from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    ScrollView,
    FlatList,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    LayoutAnimation, UIManager,
    ActivityIndicator,
    BackHandler,
    Alert
} from 'react-native';
import {connect} from 'react-redux';
import { userinfo ,loadingOn,loadingOff,changeSelectedChild} from '../../Action'
import { scale, moderateScale, verticalScale } from '../../Components/Scale';
//import RNHTMLtoPDF from 'react-native-html-to-pdf';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import Header from "../../Components/Header";
import HamburgerIcon from "../../Components/HamburgerIcon"
import { HeaderBackButton } from 'react-navigation-stack';
import axios from 'axios';
import RNFetchBlob from 'rn-fetch-blob'
import Spinner from "react-native-spinkit";

class ExpandableItemComponent extends Component {
    //Custom Component for the Expandable List
    constructor() {
        super();
        this.state = {
            layoutHeight: 0,
        };
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.item.isExpanded) {
            this.setState(() => {
                return {
                    layoutHeight: null,
                };
            });
        } else {
            this.setState(() => {
                return {
                    layoutHeight: 0,
                };
            });
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.layoutHeight !== nextState.layoutHeight) {
            return true;
        }
        return false;
    }

    render() {
        return (

            <View style={{ width: scale(322), borderRadius: 15, marginTop: verticalScale(20), backgroundColor: "#FFFFFF", justifyContent: "center", alignItems: "center", flex: 1 }}>
                <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center", height: scale(80), position: "relative" }}>

                    <View style={{ width: "35%", justifyContent: "center", alignItems: "center" }}>
                        <Text style={{
                            fontSize: scale(16), fontFamily: "Roboto-Bold",
                            fontWeight: "bold", color: "#000"
                        }}>
                            {this.props.item.key}
                        </Text>
                    </View>

                    <View style={{ width: "55%", flexDirection: "column", justifyContent: "center", }}>
                        <View style={{ flexDirection: "row" }}>
                            <Text style={{ color: "#707070", marginLeft: scale(20) }}>
                                Due Date:
            </Text>
                            <Text style={{ marginLeft: scale(5), color: "#707070" }}>
                                {this.props.item.dueDate}
                            </Text>
                        </View>
                        {!this.props.item.Expand ? null :
                            <View style={{ flexDirection: "row", marginTop: verticalScale(10), }}>
                                <Text style={{ color: "#707070", marginLeft: scale(20) }}>
                                    Given Date:
                    </Text>
                                <Text style={{ marginLeft: scale(5), color: "#707070" }}>
                                    {this.props.item.givenDate}
                                </Text>
                            </View>}


                    </View>

                    <View style={{ width: "10%", }}>

                        {this.props.item.Expand ? !this.props.item.isExpanded ?
                            <TouchableOpacity activeOpacity={0.8} onPress={this.props.onClickFunction}>
                                <Image
                                    style={{ width: scale(27), height: scale(27), }}
                                    source={require('../../assets/downarrowblue.png')}
                                    resizeMode='cover'
                                />
                            </TouchableOpacity> :
                            <TouchableOpacity activeOpacity={0.8} onPress={this.props.onClickFunction}>
                                <Image
                                    style={{ width: scale(27), height: scale(27), }}
                                    source={require('../../assets/uparrowblue.png')}
                                    resizeMode='cover'
                                />
                            </TouchableOpacity> :
                            <Image
                                style={{ width: scale(27), height: scale(27), }}
                                source={require('../../assets/downarrow.png')}
                                resizeMode='cover'
                            />
                        }
                    </View>
                </View>

                <View style={{ height: this.state.layoutHeight, overflow: 'hidden', }}>
                    <View style={{ flexDirection: "column", marginLeft: "35%" }}>
                        <View style={{ flexDirection: "row", }}>
                            <Text style={{ color: "#707070", marginLeft: scale(20) }}>
                                Height:
            </Text>
                            <Text style={{ marginLeft: scale(5), color: "#707070" }}>
                                {this.props.item.height} cm
                    </Text>
                        </View>

                        <View style={{ flexDirection: "row", marginTop: verticalScale(5) }}>
                            <Text style={{ color: "#707070", marginLeft: scale(20) }}>
                                Weight:
            </Text>
                            <Text style={{ marginLeft: scale(5), color: "#707070" }}>
                                {this.props.item.weight} kg
                    </Text>
                        </View>
                    </View>
                    {this.props.item.vaccines.map((item, key) => (
                        <>
                        {item.givenDate ? 
                        <View style={{ flexDirection: "row", marginTop: verticalScale(10), width: scale(322), backgroundColor: key % 2 == 0 ? "#DFF3F8" : "#fff" }}>

                            <View style={{ width: "35%", alignItems: "center", }}>
                                <Text style={{
                                    fontSize: scale(16), fontFamily: "Roboto-Bold",
                                    fontWeight: "bold", color: "#000"
                                }}>
                                    {item.name}
                                </Text>
                            </View>

                            <View style={{ width: "55%", flexDirection: "column", }}>

                                <View style={{ flexDirection: "row", marginLeft: scale(20) }}>
                                    <Text style={{ color: "#707070" }}>
                                        Given Date:
  </Text>
                                    <Text style={{ marginLeft: scale(5), color: "#707070" }}>
                                        {item.givenDate}
                                    </Text>
                                </View>


                                <View style={{ flexDirection: "row", marginTop: verticalScale(5) }}>
                                    <Text style={{ color: "#707070", marginLeft: scale(20) }}>
                                        Brand:
  </Text>
                                    <Text style={{ marginLeft: scale(5), color: "#707070" }}>
                                        {item.brand}
                                    </Text>
                                </View>
                                <View style={{ flexDirection: "row", marginTop: verticalScale(5) }}>
                                    <Text style={{ color: "#707070", marginLeft: scale(20) }}>
                                        Mfg Date:
  </Text>
                                    <Text style={{ marginLeft: scale(5), color: "#707070" }}>
                                        {item.mfgDate}
                                    </Text>
                                </View>

                                <View style={{ flexDirection: "row", marginTop: verticalScale(5) }}>
                                    <Text style={{ color: "#707070", marginLeft: scale(20) }}>
                                        Exp Date:
  </Text>
                                    <Text style={{ marginLeft: scale(5), color: "#707070" }}>
                                        {item.expDate}
                                    </Text>
                                </View>

                                <View style={{ flexDirection: "row", marginTop: verticalScale(5) }}>
                                    <Text style={{ color: "#707070", marginLeft: scale(20) }}>
                                        Batch No.:
  </Text>
                                    <Text style={{ marginLeft: scale(5), color: "#707070" }}>
                                        {item.batchNumber}
                                    </Text>
                                </View>

                                <View style={{ flexDirection: "row", marginTop: verticalScale(5) }}>
                                    <Text style={{ color: "#707070", marginLeft: scale(20) }}>
                                        Hospital:
  </Text>
                                    <Text style={{ marginLeft: scale(5), color: "#707070" }}>
                                        {item.hospital}
                                    </Text>
                                </View>

                            </View>

                        </View>
                        :null}
                        </>
                    ))}
                    <View style={{ height: scale(3), }} />
                </View>

            </View>
        );
    }
}

class Vaccinedigitalreport extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HamburgerIcon />,
        title: 'Vaccine Digital Report',
        headerBackground: (
            <Header />
        ),
        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: 20,
            fontFamily: "Roboto-Bold",
            fontWeight: "bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        }
    });
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {

            dataSource: [],
            Source: [],
            filePath: '',
            isLoading: true,
            pdf: [],
            pdfurl: '',
            visible: false,
        }

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Vaccine');
        return true;
    }

    actualDownload = () => {
        const { dirs } = RNFetchBlob.fs;
        RNFetchBlob.config({
            fileCache: true,
            addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                mediaScannable: true,
                title: `Vaccine_Digital_Report.pdf`,
                path: `${dirs.DownloadDir}/Vaccine_Digital_Report.pdf`,
            },
        })
            .fetch('GET', this.props.pdf, {})
            .then((res) => {
                Alert.alert('The file saved to ', res.path());
                console.log('The file saved to ', res.path());
            })
            .catch((e) => {
                console.log(e)
            });
    }

    downloadFile = async () => {
        try {
            const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.actualDownload();
            } else {
                Alert.alert('Permission Denied!', 'You need to give storage permission to download the file');
            }
        } catch (err) {
            alert(err);
            console.warn(err);
        }
    }

    updateLayout = index => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        const array = [...this.props.Source];
        array.map((value, placeindex) =>
            placeindex === index
                ? (array[placeindex]['isExpanded'] = !array[placeindex]['isExpanded'])
                : (array[placeindex]['isExpanded'] = false)
        );
        this.setState(() => {
            return {
                listDataSource: array,
            };
        });
    };

    render() {
        if (this.props.loder) {
            return (
                <View style={styles.container}>
                    <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }} />
                </View>
            )
        }
        return (
            <View style={styles.container}>
                <ScrollView showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}>
                    {this.props.Source.map((item, key) => (
                        <ExpandableItemComponent
                            key={item.key}
                            onClickFunction={this.updateLayout.bind(this, key)}
                            item={item}
                        />
                    ))}

                    {/* {this.props.visible ? */}
                        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                            <TouchableOpacity
                                style={{
                                    justifyContent: 'center',
                                    borderWidth: 1,
                                    borderColor: !this.props.visible ? "#a076e8" : "#707070",
                                    alignItems: 'center',
                                    backgroundColor:!this.props.visible ? 'transparent' : "#707070",
                                    borderRadius: 10,
                                    width: scale(150),
                                    marginTop: verticalScale(15),
                                    height: scale(50),
                                }}
                                onPress={() =>{ this.props.navigation.navigate('View_As_PDF')}}
                                disabled={this.props.visible}>

                                <Text style={{
                                    color: !this.props.visible ? '#a076e8' : "#FFFFFF", fontFamily: "Roboto-Bold",
                                    fontWeight: "bold", fontSize: scale(16)
                                }}>
                                    {console.log('visible',this.props.visible)
                                    }View As PDF</Text>

                            </TouchableOpacity>

                            <TouchableOpacity
                                style={styles.btnSignin}
                                // onPress={this.askPermission.bind(this)}
                                onPress={this.downloadFile}
                                disabled={this.props.visible}>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    colors={!this.props.visible ? ['#a076e8', '#5dc4dd'] : ['#707070',"#707070"]}
                                    style={styles.linearGradient}>
                                    <Text style={{
                                        color: '#FFFFFF', fontFamily: "Roboto-Bold",
                                        fontWeight: "bold", fontSize: scale(16)
                                    }}>Download PDF</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </View> 
                        {/* : null}  */}

                    <View style={{ height: scale(20) }} />

                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f4f7f9',
    },
    input: {
        margin: scale(15),
        height: scale(50),
        width: scale(300),
        padding: scale(5),
        fontSize: scale(16),
        borderBottomWidth: 1,
        borderBottomColor: 'black',
    },
    linearGradient: {
        borderRadius: 10,
        justifyContent: 'center',
        width: scale(150),
        alignItems: 'center',
        height: scale(50),
    },
    btnSignin: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        borderRadius: 10,
        width: scale(150),
        marginTop: verticalScale(15),
        height: scale(50),
    },

})

const DataSource = (data) => {
    const SourceData = data;
        let data2 = [];
        for (let key in SourceData.childVaccines) {
            const dataobject = {};
            if (SourceData.childVaccines[key].givenDate === "") {
                dataobject = SourceData.childVaccines[key];
                dataobject['Expand'] = false;
                // dataobject['Expand'] = true;
                dataobject['isExpanded'] = false;
                dataobject['key'] = key;
            } else {
                dataobject = SourceData.childVaccines[key];
                dataobject['Expand'] = true;
                // dataobject['Expand'] = false;
                dataobject['isExpanded'] = false;
                dataobject['key'] = key;
            }
            data2.push(dataobject);
        }
        return data2;
}

const pdfurl = (data) => {
    console.log('data',data);
    return data.pdfUrl;
    
}

const pdfdata = (SourceData) => {
    let data = [];
        for (let key in SourceData.childVaccines) {
            const object = {};
            object = SourceData.childVaccines[key].givenDate;
            data.push(object);
        }
    for(let key2 in data){
        if(data[key2] !== ""){
            return false;
        }
        else{
            return true;
        }
    }
}

function mapStateToProps(state){
    let selectedChild = state.userinfo.selectedChild
    return{
        user:state.userinfo.user,
        pdf: pdfurl(state.userinfo.user[selectedChild]),
        Source : DataSource(state.userinfo.user[selectedChild]),
        visible : pdfdata(state.userinfo.user[selectedChild]),
        loder: state.loder,
        
    }
}

export default connect(mapStateToProps,{userinfo,loadingOn,loadingOff,changeSelectedChild})(Vaccinedigitalreport)