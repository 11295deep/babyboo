import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    BackHandler,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    Button
} from 'react-native';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, upload_Image, registerChild,changeSelectedChild, rhymes,Milestone } from '../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import DatePicker from 'react-native-datepicker';
import { scale, moderateScale, verticalScale } from '../../Components/Scale';
import ImagePicker from 'react-native-image-picker';
import Spinner from "react-native-spinkit";

const shadowStyle = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
}

class RadioButton extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <TouchableOpacity onPress={this.props.onClick} activeOpacity={0.8} style={styles.radioButton}>
                <View style={[styles.radioButtonHolder, { height: this.props.button.size, width: this.props.button.size, borderColor: "transparent", marginLeft: scale(20) }]}>
                    {
                        (this.props.button.selected)
                            ?
                            (<View style={[styles.radioIcon, shadowStyle, { height: this.props.button.size * 2, width: this.props.button.size * 2, borderColor: this.props.button.color, borderWidth: 2, }]}>
                                <Image
                                    style={{ height: scale(60), width: scale(35) }}
                                    source={this.props.button.label}
                                    resizeMode="contain" />
                                <Text style={{ fontSize: scale(11), color: this.props.button.color }}>{this.props.button.name}</Text>
                            </View>)
                            :
                            <View style={[styles.radioIcon, shadowStyle, { height: this.props.button.size * 2, width: this.props.button.size * 2, borderColor: "transparent", borderWidth: 2 }]}>
                                <Image
                                    style={{ height: scale(60), width: scale(35) }}
                                    source={this.props.button.label}
                                    resizeMode="contain" />
                                <Text style={{ fontSize: scale(11), color: this.props.button.color }}>{this.props.button.name}</Text>
                            </View>
                    }
                </View>
            </TouchableOpacity>
        );
    }
}


class RegisterChild extends React.Component {
    constructor(props) {
        super(props);
        this.state =
        {
            date: '',
            select: false,
            error: null,
            name: '',
            dob: '',
            boy: "m",
            girl: "f",
            gender: null,
            dataSource: [],
            isLoading: true,
            hide: false,
            // photo:'',
            radioItems:
                [
                    {
                        label: require('../../assets/boy.png'),
                        size: 50,
                        color: "#91E1F5",
                        selected: false,
                        gender: 'm',
                        name: 'Boy',

                    },

                    {
                        label: require('../../assets/girl.png'),
                        size: 50,
                        color: "#D23C69",
                        selected: false,
                        gender: 'f',
                        name: 'Girl',
                    },
                ], selectedItem: ''
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }


    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.goBack(null);
        return true;
    }

    componentDidMount = async () => {
        this._getData();
    }

    _getData = async () => {
        const token = await AsyncStorage.getItem('token');
        console.log('async', token);
        const respose = await this.props.userinfo(() => this.props.navigation.navigate('Home'))
        this.state.radioItems.map((item) => {
            if (item.selected == true) {
                this.setState({ selectedItem: item.gender });
            }
            this.setState({
                isLoading: false,
                dataSource: respose.data.children
            })
            console.log('dataaaaaaa', this.state.dataSource);

        });
    }

    _getData2 = async () => {
        const token = await AsyncStorage.getItem('token');
        console.log('async', token);
        const respose = await this.props.userinfo()
        this.state.radioItems.map((item) => {
            if (item.selected == true) {
                this.setState({ selectedItem: item.gender });
            }
            this.setState({
                isLoading: false,
                dataSource: respose.data.children
            })
            console.log('dataaaaaaa', this.state.dataSource);
        });
    }

    changeActiveRadioButton(index) {
        this.state.radioItems.map((item) => {
            item.selected = false;
        });
        this.state.radioItems[index].selected = true;

        this.setState({ radioItems: this.state.radioItems }, () => {
            this.setState({ selectedItem: this.state.radioItems[index].gender });
        });
    }
    genderselect = () => {

        if (this.state.status1 == false && this.state.status2 == true) {
            this.setState({ status1: true })
            this.setState({ status2: false })
            return this.setState({ gender: 'm' })
        }
        else {
            this.setState({ status1: false })
            this.setState({ status2: true })
            return this.setState({ gender: 'f' })
        }
    }

    _submit = async () => {

        var {
            name,
            date,
            selectedItem
        } = this.state;

        const data = {
            "gender": selectedItem,
            "name": name,
            "dob": date
        }
        console.log('date', this.state.date);
        console.log(data);
        if (name == "") { this.setState({ error: "* Name can't be empty" }); }
        else if (date == "") { this.setState({ error: "* DOB can't be empty" }); }
        else if (selectedItem == "") { this.setState({ error: "* Select Gender" }); }
        else {
            console.log('child', data);
            const result = await this.props.registerChild(data)
            if (result.status === 200) {
                this.setState({
                    date: '',
                    select: false,
                    error: null,
                    name: '',
                    dob: '',
                    boy: "m",
                    girl: "f",
                    gender: null,
                    dataSource: [],
                    photo: '',
                    hide: false,
                    radioItems:
                        [
                            {
                                label: require('../../assets/boy.png'),
                                size: 50,
                                color: "#91E1F5",
                                selected: false,
                                gender: 'm',
                                name: 'Boy',

                            },

                            {
                                label: require('../../assets/girl.png'),
                                size: 50,
                                color: "#D23C69",
                                selected: false,
                                gender: 'f',
                                name: 'Girl',
                            },
                        ], selectedItem: ''
                })
                await this.props.userinfo()
                await this.props.changeSelectedChild(0)
                await this.props.rhymes()
                await this.props.Milestone()
                this.props.navigation.navigate('Home');
            }

        }
    }
    handleChoosePhoto = () => {
        // const options = {
        //   noData: true,
        // }
        var options = {
            title: 'Select Image',
            // customButtons: [
            //     { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
            // ],
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            console.log('image res', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else if (response.uri) {
                this.setState({ photo: response }, () => this.handleUploadPhoto());
                //() => this.handleUploadPhoto()
            }
        })
    }
    createFormData = (photo, body) => {
        const data = new FormData();

        data.append("photo", {
            name: photo.fileName,
            type: photo.type,
            uri:
                Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
        });

        Object.keys(body).forEach(key => {
            data.append(key, body[key]);
        });
        console.log('image data', data);
        return data;
    };
    handleUploadPhoto = async () => {
        try {
            let response;

            if (!this.state.dataSource) {
                console.log('hiiii', this.createFormData(this.state.photo, { index: 0 }));
                response = await this.props.upload_Image(this.createFormData(this.state.photo, { index: 0 }));
            } else if (this.state.dataSource.length === 1) {
                response = await this.props.upload_Image(this.createFormData(this.state.photo, { index: 1 }));
            }
            console.log('rrr', response);
        }
        catch (e) {
            console.log('error image upload', e);
        }
    };
    hidefun = () => {
        this.setState({
            hide: true,
        })
    }
    add_another_child = async () => {
        var {
            name,
            date,
            selectedItem
        } = this.state;

        const data = {
            "gender": selectedItem,
            "name": name,
            "dob": date
        }
        console.log('date', this.state.date);
        console.log(data);
        if (name == "") { this.setState({ error: "* Name can't be empty" }); }
        else if (date == "") { this.setState({ error: "* DOB can't be empty" }); }
        else if (selectedItem == "") { this.setState({ error: "* Select Gender" }); }
        else {
            console.log('child', data);
            const result = await this.props.registerChild(data)
            if (result.status === 200) {
                this.setState({
                    date: '',
                    select: false,
                    error: null,
                    name: '',
                    dob: '',
                    boy: "m",
                    girl: "f",
                    gender: null,
                    dataSource: [],
                    photo: '',
                    hide: false,
                    radioItems:
                        [
                            {
                                label: require('../../assets/boy.png'),
                                size: 50,
                                color: "#91E1F5",
                                selected: false,
                                gender: 'm',
                                name: 'Boy',

                            },

                            {
                                label: require('../../assets/girl.png'),
                                size: 50,
                                color: "#D23C69",
                                selected: false,
                                gender: 'f',
                                name: 'Girl',
                            },
                        ], selectedItem: ''
                })
                this._getData2();
            }
        }

        // await AsyncStorage.removeItem('token');
        // return this.props.navigation.navigate('Auth');
    }

    render() {
        const { photo, isLoading } = this.state
        console.log('loder', this.props.loder);
        console.log('data', this.props.user);

        if (this.props.loder) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }} />
                </View>
            );
        }
        return (
            <View style={{ flex: 1, backgroundColor: "#F4F7F9" }}>
                <ScrollView showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}>
                    <View style={styles.container}>
                        <View>
                            <Text style={{ fontSize: scale(19), color: "#707070" }}> Add Child </Text>
                        </View>
                        <View style={{ marginTop: verticalScale(5), flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            {/* {this.state.hide === true ? null : */}
                            <TouchableOpacity
                            onPress={() => { this.handleChoosePhoto(); this.hidefun() }}
                                style={[styles.myButton, shadowStyle]}
                            >
                                {photo ? <Image
                                    source={{ uri: photo.uri }}
                                    style={{ width: scale(163), height: scale(163), borderRadius: 400 }}
                                /> :
                                    <Image
                                        style={{ height: scale(27), width: scale(32) }}
                                        source={require('../../assets/camera.png')}
                                        resizeMode="contain" />
                                }
                            </TouchableOpacity>
                            {/* } */}
                            {/* {photo && (
                                <Image
                                    source={{ uri: photo.uri }}
                                    style={{ width: scale(163), height: scale(163), borderRadius: 400 }}
                                />
                            )} */}
                            {/* <Button title="Choose Photo" onPress={this.handleChoosePhoto} /> */}
                            <TouchableOpacity style={{ marginTop: verticalScale(10) }} onPress={() => { this.handleChoosePhoto(); this.hidefun() }} >
                                <Text>Add Photo</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ marginTop: verticalScale(25) }}>
                        <Text style={{ fontSize: scale(16), color: "#707070", marginLeft: scale(29) }}>What’s your Baby’s Gender?</Text>
                    </View>


                    <View style={{ width: scale(322), flexDirection: "row", marginTop: verticalScale(20), alignItems: 'center', justifyContent: 'center' }}>

                        {
                            this.state.radioItems.map((item, key) =>
                                (
                                    <RadioButton key={key} button={item} onClick={this.changeActiveRadioButton.bind(this, key)} />
                                ))
                        }
                    </View>

                    <View style={{ alignItems: 'center', marginTop: verticalScale(5) }}>
                        <View style={[styles.SectionStyle, shadowStyle]}>
                            <TextInput
                                style={[styles.input]}
                                placeholder="What’s your Baby’s Name?"
                                keyboardType="default"
                                onChangeText={(name) => this.setState({ name })}
                                value={this.state.name}
                            />
                        </View>

                        <View style={{ justifyContent: 'center', alignItems: 'flex-start', borderRadius: 10, height: scale(50), width: scale(300), margin: scale(20), backgroundColor: "#ffffff" }}>
                            <DatePicker
                                style={[{
                                    height: scale(50),
                                    width: scale(300),
                                    borderRadius: 10,
                                    justifyContent: "center",
                                    backgroundColor: "#FFFFFF",
                                }, shadowStyle]}
                                date={this.state.date} //initial date from state
                                mode="date" //The enum of date, datetime and time
                                placeholder="What’s your Baby’s Birthday?"
                                placeHolderTextStyle={{ color: "#707070", fontSize: scale(15), }}
                                format="DD/MM/YYYY"
                                minDate="01-01-1900"
                                maxDate={new Date()}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                iconComponent={
                                    <Image
                                        style={{ width: scale(20), height: scale(20), marginRight: scale(20), alignSelf: "center" }}
                                        source={require('../../assets/calandergrey.png')}
                                        resizeMode="cover"
                                    />}
                                customStyles={{
                                    dateIcon: {
                                        position: 'relative',
                                        alignItems: 'flex-end'
                                    },
                                    dateInput: {
                                        borderWidth: 0,
                                        fontSize: scale(15),
                                        height: scale(50),
                                        width: scale(400),
                                        alignItems: "flex-start",
                                        paddingLeft: scale(15),


                                    }
                                }}
                                onDateChange={(date) => { this.setState({ date: date }) }}
                                value={this.state.date}
                            />
                        </View>

                        <Text style={{ color: "red", marginTop: scale(10) }}>{this.state.error}</Text>

                        {/* <TouchableOpacity
                            style={[styles.btnSignin, shadowStyle]}
                            onPress={() => this.next()}>
                            <Text style={{ color: '#a076e8', fontWeight: 'bold', fontSize: scale(15) }}>Next </Text>
                        </TouchableOpacity> */}


                    </View>
                    <View style={{ width: scale(300), flexDirection: "row", justifyContent: "space-between", alignSelf: "center" }}>
                        <TouchableOpacity
                            style={{
                                justifyContent: 'center',
                                borderWidth: 1,
                                borderColor: "#a076e8",
                                alignItems: 'center',
                                borderRadius: 10,
                                width: scale(140),
                                marginTop: verticalScale(15),
                                height: scale(44),
                            }}
                            onPress={() => this.add_another_child()}
                        //disabled={this.state.dataSource.length === 3 ? true : false}
                        >

                            <Text style={{
                                color: '#a076e8', fontFamily: "Roboto-Bold",
                                fontWeight: "bold", fontSize: scale(13)
                            }}>Add Another Child</Text>

                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.btnSignin}
                            onPress={() => this._submit()}
                        >
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                colors={['#a076e8', '#5dc4dd']}
                                style={styles.linearGradient}>
                                <Text style={{
                                    color: '#FFFFFF', fontFamily: "Roboto-Bold",
                                    fontWeight: "bold", fontSize: scale(13)
                                }}>Submit</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                    <View style={{ height: scale(15) }} />
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: verticalScale(35)
    },
    myButton: {
        padding: scale(5),
        height: scale(163),
        width: scale(163),
        borderRadius: 400,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    btngender: {
        padding: scale(5),
        height: scale(91),
        width: scale(91),
        borderRadius: scale(400),
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 2,
        borderRadius: 10,
        height: scale(50),
        width: scale(300),
        margin: scale(20)
    },
    input: {
        margin: scale(15),
        height: scale(50),
        width: scale(300),
        paddingLeft: scale(15),
        fontSize: scale(15),
        borderRadius: 10,
        backgroundColor: '#fff',
    },

    radioButton: {
        margin: scale(10),
        alignItems: 'center',
        marginLeft: scale(50),
        justifyContent: 'center'
    },

    radioButtonHolder:
    {
        borderRadius: 50,
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },

    radioIcon:
    {
        borderRadius: 50,
        backgroundColor: "#FFFFFF",
        justifyContent: 'center',
        alignItems: 'center'
    },

    label:
    {
        marginLeft: scale(10),
        fontSize: scale(20)
    },

    selectedTextHolder:
    {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        padding: scale(15),
        backgroundColor: 'rgba(0,0,0,0.6)',
        justifyContent: 'center',
        alignItems: 'center'
    },

    selectedText:
    {
        fontSize: scale(18),
        color: 'white'
    },


    btn: {
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: '#707070',
        alignItems: 'center',
        marginLeft: scale(15),
        width: scale(300),
        marginRight: scale(15),
        padding: scale(10),
    },
    linearGradient: {
        borderRadius: 10,
        justifyContent: 'center',
        width: scale(140),
        alignItems: 'center',
        height: scale(44),
    },
    btnSignin: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        borderRadius: 10,
        width: scale(140),
        marginTop: verticalScale(15),
        height: scale(44),
    },
})

function mapStateToProps(state) {
    return {
        user: state.userinfo.user,
        loder: state.loder,

    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, upload_Image, registerChild,changeSelectedChild, rhymes,Milestone})(RegisterChild)
// export default RegisterChild;