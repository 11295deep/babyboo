import * as React from 'react';
import { Button, View, Text, Image, Dimensions, TouchableOpacity } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-navigation';
import { HeaderBackButton } from 'react-navigation-stack';
import { scale, moderateScale, verticalScale } from '../Components/Scale';
//https://youtu.be/azhuem9P91g
import SplashScreen from "./SplashScreen"
import Signin from "./Signin"
import PlayVideo from "./Entertainment/PlayVideo"
import Signup from "./Signup"
import Language from "./Entertainment/Language"
import ForgotPassword from "./ForgotPassword";
import AuthLoadingScreen from './AuthLoadingScreen'
import RegisterChild from "./RegisterChild"
import Home from "./Home";
import Vaccine from "./Vaccine";
import Milestone from "./Milestone";
import Entertainment from "./Entertainment";
import Profile from "./Profile";
import Custom_Side_Menu from "../Components/Custom_Side_Menu";
import About from "./About";
import ResetPassword from "./ResetPassword";
import Vaccinedigitalreport from "./Vaccinedigitalreport";
import Next_Due_Vaccine from "./Next_Due_Vaccine";
import About_Vaccine from "./About_Vaccine";
import BCG from "./About_Vaccine/BCG";
import OPV from "./About_Vaccine/OPV";
import HEPB from "./About_Vaccine/HEPB";
import DTwP from "./About_Vaccine/DTwP";
import IPV from "./About_Vaccine/IPV";
import Hib from "./About_Vaccine/Hib";
import RvVaccine from "./About_Vaccine/RvVaccine";
import PCV from "./About_Vaccine/PCV"
import MMR from "./About_Vaccine/MMR"
import TCV from "./About_Vaccine/TCV"
import VaricellaVaccine from "./About_Vaccine/VaricellaVaccine"
import HPV from "./About_Vaccine/HPV"
import View_As_PDF from "./View_As_PDF"

const VaccineScreen = createStackNavigator({
    Vaccine: {
        screen: Vaccine,
    },
    Next_Due_Vaccine: {
        screen: Next_Due_Vaccine,
    },
    Vaccinedigitalreport: { 
        screen: Vaccinedigitalreport 
    },
    View_As_PDF:{
        screen: View_As_PDF
    }
},
    {
        initialRouteName: 'Vaccine',

    }
)
// createStackNavigator(
//     {
//         Vaccine: { screen: Vaccine },
//     },
//     { headerLayoutPreset: 'center' }
// )
const MilestoneScreen = createStackNavigator(
    {
        Milestone: { screen: Milestone }
    },
    { headerLayoutPreset: 'center' }
)
const HomeScreen = createStackNavigator(
    {
        Home: { screen: Home },
        About_Vaccine: {
            screen: About_Vaccine,
        },
        BCG: {
            screen: BCG,
        },
        OPV: {
            screen: OPV,
        },
        HEPB: {
            screen: HEPB,
        },
        DTwP:{
            screen: DTwP,
        },
        IPV:{
            screen: IPV,
        },
        Hib:{
            screen: Hib,
        },
        RvVaccine:{
            screen: RvVaccine,
        },
        PCV:{
            screen: PCV,
        },
        MMR:{
            screen: MMR,
        },
        TCV:{
            screen: TCV,
        },
        VaricellaVaccine:{
            screen:VaricellaVaccine,
        },
        HPV:{
            screen:HPV,
        }

    },
    {
        initialRouteName: 'Home',

    }

)

const ProfileScreen = createStackNavigator(
    {
        Profile: { screen: Profile, }
    },
    { headerLayoutPreset: 'center' }
)

const EntertainmentScreen = createStackNavigator({
    Entertainment: {
        screen: Entertainment,
    },
    Language: {
        screen: Language,
    }
},
    {
        initialRouteName: 'Entertainment',

    }
)

const BottomTabNavigator = createBottomTabNavigator({

    Home: { screen: HomeScreen },
    Vaccine: { screen: VaccineScreen },
    Milestone: { screen: MilestoneScreen },
    Entertainment: { screen: EntertainmentScreen },
    // Profile: { screen: ProfileScreen },
},
    {
        initialRouteName: 'Home',
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Vaccine') {
                    if (focused) {
                        return <Image
                            style={{ width: scale(28), height: scale(28), borderWidth: 0, }}
                            source={require('../assets/injectionmenu.png')}
                            resizeMode='contain' />;
                    }
                    else {
                        return <Image
                            style={{ width: scale(28), height: scale(28), borderWidth: 0, }}
                            source={require('../assets/vaccinegrey.png')}
                            resizeMode='contain' />
                    }
                } else if (routeName === 'Milestone') {
                    if (focused) {
                        return <Image
                            style={{ width: scale(28), height: scale(28), borderWidth: 0, }}
                            source={require('../assets/milesronemenu.png')}
                            resizeMode='contain' />;
                    }
                    else {
                        return <Image
                            style={{ width: scale(28), height: scale(28), borderWidth: 0, }}
                            source={require('../assets/Milestonegrey.png')}
                            resizeMode='contain' />
                    }
                } else if (routeName === 'Home') {
                    if (focused) {
                        return <Image
                            style={{ width: scale(28), height: scale(28), borderWidth: 0, }}
                            source={require('../assets/house.png')}
                            resizeMode='contain' />;
                    }
                    else {
                        return <Image
                            style={{ width: scale(28), height: scale(28), borderWidth: 0, }}
                            source={require('../assets/homegrey.png')}
                            resizeMode='contain' />
                    }
                }
                else if (routeName === 'Entertainment') {
                    if (focused) {
                        return <Image
                            style={{ width: scale(28), height: scale(28), borderWidth: 0, }}
                            source={require('../assets/ENTERTAINMENT.png')}
                            resizeMode='contain' />;
                    }
                    else {
                        return <Image
                            style={{ width: scale(28), height: scale(28), borderWidth: 0, }}
                            source={require('../assets/ENTERTAINMENTgrey.png')}
                            resizeMode='contain' />
                    }
                }
                // else if (routeName === 'Profile') {
                //     if (focused) {
                //     return <Image
                //         style={{ width: scale(28), height: scale(28), borderWidth: 0, }}
                //         source={require('../assets/usermenu.png')}
                //         resizeMode='contain' />;}
                //         else{
                //             return <Image
                //             style={{ width: scale(28), height: scale(28), borderWidth: 0, }}
                //             source={require('../assets/profilegrey.png')}
                //             resizeMode='contain' />
                //         }
                // }
            },
        }),
        tabBarOptions: {
            activeTintColor: '#a076e8',
            inactiveTintColor: '#707070',
            style: {
                backgroundColor: '#FFFFFF',
                height: scale(60)
            }
        },
    });

const HamburgerNavigation = createDrawerNavigator(

    {
        BottomTabNavigator: {
            screen: BottomTabNavigator,
            navigationOptions: {
                header: null,
                tabBarVisible: false,
            },
        },
    },
    {
        initialRouteName: 'BottomTabNavigator',
        drawerWidth: scale(250),
        contentComponent: Custom_Side_Menu,
    },
    // { About: { screen: About } },
    // { Logout: { screen: Logout } },
);


const AuthStack = createStackNavigator({
    Signin:
    {
        screen: Signin,
        navigationOptions: {
            header: null,
        },
    }
})
const AppStack = createStackNavigator({

    RegisterChild: {
        screen: RegisterChild,
        navigationOptions: {
            header: null,
            tabBarVisible: false,
        },
    },

})
const Root = createSwitchNavigator({
    SplashScreen: SplashScreen,
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack
},
    {
        initialRouteName: 'SplashScreen',
    })

const AboutScreen = createStackNavigator(
    {
        About: { screen: About }
    },
    { headerLayoutPreset: 'center' }
)

// const VaccinedigitalreportScreen = createStackNavigator(
//     {
//         Vaccinedigitalreport: { screen: Vaccinedigitalreport }
//     },
//     { headerLayoutPreset: 'center' }
// )

const RootStack = createStackNavigator({
    Root: {
        screen: Root,
        navigationOptions: {
            header: null,
        },
    },
    Signup:
    {
        screen: Signup,
        navigationOptions: {
            header: null,
        },
    },
    ForgotPassword: {
        screen: ForgotPassword,
        navigationOptions: {
            header: null,
            tabBarVisible: false,
        }
    },
    ChangePassword: {
        screen: ResetPassword,
        navigationOptions: {
            header: null,
            tabBarVisible: false,
        }
    },
    PlayVideo: {
        screen: PlayVideo,
        // navigationOptions: {
        //     header: null,
        // }
    },
    Drawer: {
        screen: HamburgerNavigation,
        navigationOptions: {
            header: null,
        },
    },
    About: {
        screen: AboutScreen,
        navigationOptions: {
            header: null,
        },
    },

    Profile: {
        screen: ProfileScreen,
        navigationOptions: {
            header: null,
        },
    },
    // Vaccinedigitalreport: {
    //     screen: VaccinedigitalreportScreen,
    //     navigationOptions: {
    //         header: null,
    //     },
    // }

},
    {
        initialRouteName: 'Root',
    })

const AppContainer = createAppContainer(RootStack);

export default class Routes extends React.Component {
    render() {
        return <AppContainer />;
    }
}