import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    Dimensions,
    ScrollView,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid, ActivityIndicator,
    Platform,
    Switch,
    Alert
} from 'react-native';
import url from "../../API";
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import { scale, moderateScale, verticalScale } from '../../Components/Scale';
import axios from 'axios';
import { FBLogin, FBLoginManager } from 'react-native-facebook-login';
import GoogleSignIn from 'react-native-google-sign-in';
import FBLoginView from '../../Components/FBLoginView';
import { saveUser,fbloginUser } from '../../Action'
import { connect } from 'react-redux';
import Popup from "../../Components/PopUp";

const screenDimension = Dimensions.get('window');
const isBigDevice = screenDimension.height > 600;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const shadowStyle = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
}

class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.toggleSwitch = this.toggleSwitch.bind(this);
        this.state = {
            email: '',
            password: '',
            name: '',
            mobile: '',
            error1: false,
            error2: false,
            error3: false,
            error4: false,
            error5: false,
            error6: false,
            error7: false,
            showPassword: true,
            isModalVisible:false,
        }
    }

    toggleSwitch() {
        this.setState({ showPassword: !this.state.showPassword });
    }
    validateEmail() {
        let reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        // /^([a-z0-9\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/;
        // /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return reg_email.test(this.state.email);
    }
    validatePassword() {
        let reg_pwd = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/;
        return reg_pwd.test(this.state.password);
    }
    validateContact() {
        let reg_phone = /^\d{10}$/;

        return reg_phone.test(this.state.mobile);
    }

    onGoogleLogin = async e => {
        await GoogleSignIn.configure({
            clientID: '361840620293-nhomkhv77sceqhi8benpjevmhq13vn96.apps.googleusercontent.com',
            scopes: ['openid', 'email', 'profile'],
            shouldFetchBasicProfile: true,
        });

        GoogleSignIn.signInPromise().then((user) => {
            console.log('signInPromise resolved', user);
            setTimeout(() => {
                this.storeTokenAndNavigate(user.email);
            }, 1000);
        }, (e) => {
            console.log('signInPromise rejected', e);
            setTimeout(() => {
                alert(`signInPromise error: ${JSON.stringify(e)}`);
            }, 1000);
        });
    }
    onFBLogin = async (e) => {
        if (!e.profile) return;
        this.storeTokenAndNavigate(e.profile.email);
    }

    storeTokenAndNavigate = async (username) => {
        const data = {
            "username": username,
        }
        try {
            //await AsyncStorage.setItem('username', username);
            const response = await this.props.fbLoginUser(data)
            console.log('sigin',response.data);
            if (response.data) {
                await AsyncStorage.setItem('username', username);
                this.props.navigation.navigate('App');

            }
            else this.setState({ error: "Incorrect Username Or Password!!" })
        }
        catch (e) {
            console.log("fbloginUser_error", e.response)
            console.log("fbloginUser_error", e)
            alert('User not registered');
        }
    }

    _signUp = async () => {
        var {
            email,
            password,
            name,
            mobile,
        } = this.state
        const data = {
            "stream": "bb_stream",
            "key": email,
            "data": {
                "name": name,
                "mobile": mobile,
                "password": password
            }
        }
        if(name == "" && email == "" && mobile == "" && password == ""){
            this.setState({ error1: true,
                error2: true,
                error4: true,
                error6: true
            });

        }else if (name == "") {
            this.setState({ error1: true });
        } else if (email == "") {
            this.setState({
                error1: false,
                error2: true
            });
        } else if (!this.validateEmail()) {
            this.setState({
                error1: false,
                error2: false, error3: true
            });
        } else if (mobile == "") {
            this.setState({
                error1: false,
                error2: false, error3: false, error4: true
            });
        } else if (!this.validateContact()) {
            this.setState({
                error1: false,
                error2: false, error3: false, error4: false, error5: true
            });
        } else if (password == "") {
            this.setState({
                error1: false,
                error2: false, error3: false, error4: false, error5: false, error6: true
            });
        } else if (!this.validatePassword()) {
            this.setState({
                error1: false,
                error2: false, error3: false, error4: false, error5: false, error6: false, error7: true
            });
        }
        else {
            console.log("going data", data);
            const result = await this.props.saveUser(data)
            if (result.status === 200) {

                this.setState({isModalVisible:true})
                //this.props.navigation.navigate('Auth')
            }
            console.log('result', result);

            // if (result.status === 200) {
            //     // Set auth token auth
            //     const token = result.data.data.token;
            //     setAuthToken(token);
            //     this.props.navigation.navigate('App')
            // }

            // try {
            //     const res = await register(data);
            //     console.log("respoin", res);
            //     if (res.data) {
            //         alert('User created successfully');
            //         this.props.navigation.navigate('Reminder');
            //     } else {
            //         this.setState({ error: 'Invalid username/password' });
            //     }
            // } catch (e) {
            //     console.log('error', e);
            //     alert(e);

            // }
        }
    }

    _popup(){
        this.setState({isModalVisible:true})
    }
    cancel(){
        this.setState({isModalVisible:false})
        this.props.navigation.navigate('Auth')
    }
    _out(){
        this.setState({isModalVisible:false})
        this.props.navigation.navigate('Auth')
    }

    render() {
        console.log('loder', this.props.loder);
        if (this.props.loder) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <ActivityIndicator />
                </View>
            );
        }
        return (
            <ScrollView
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}>
                <View style={styles.container}>

                    <Image
                        style={{ width: scale(150), height: scale(150), borderWidth: 0, }}
                        source={require('../../assets/logo1.png')}
                        resizeMode='contain'
                    />

                    <KeyboardAvoidingView behavior="padding" enabled>

                        <Popup isModalVisible={this.state.isModalVisible}
                            onCancel={() => this.cancel()}
                            onSubmit={() => this._out()}
                            label="NO THANKS"
                            cancel_label="SURE"
                            text ="Let us Remind you about Upcoming Vaccinations"
                            image={require('../../assets/vaccinereminderpopup.png')} />

                        <View style={[styles.SectionStyle, shadowStyle]}>
                            <TextInput
                                style={[styles.input]}
                                placeholder="Name"
                                keyboardType="default"
                                onChangeText={(name) => this.setState({ name })} />
                        </View>
                        {this.state.error1 ?
                            <View style={{ width: scale(300), height: scale(20), marginTop: verticalScale(2) }}>
                                <Text style={{ color: "red", marginLeft: scale(15) }}>Please Enter Name</Text>
                            </View> : null}

                        <View style={[styles.SectionStyle, shadowStyle]}>
                            <TextInput
                                style={[styles.input]}
                                placeholder="Email"
                                keyboardType="email-address"
                                autoCapitalize="none"
                                onChangeText={(email) => this.setState({ email })} />
                        </View>
                        {this.state.error2 ?
                            <View style={{ width: scale(300), height: scale(20), marginTop: verticalScale(2) }}>
                                <Text style={{ color: "red", marginLeft: scale(15) }}>Please Enter Email</Text>
                            </View> :
                            this.state.error3 ?
                                <View style={{ width: scale(300), height: scale(20), marginTop: verticalScale(2) }}>
                                    <Text style={{ color: "red", marginLeft: scale(15) }}>Email Incorrect. Please Try Again</Text></View> : null}

                        <View style={[styles.SectionStyle, shadowStyle]}>
                            <TextInput
                                style={[styles.input]}
                                placeholder="Mobile"
                                keyboardType="phone-pad"
                                onChangeText={(mobile) => this.setState({ mobile })} />

                        </View>
                        {this.state.error4 ?
                            <View style={{ width: scale(300), height: scale(20), marginTop: verticalScale(2) }}>
                                <Text style={{ color: "red", marginLeft: scale(15) }}>Please Enter Mobile</Text>
                            </View> :
                            this.state.error5 ?
                                <View style={{ width: scale(300), height: scale(20), marginTop: verticalScale(2) }}>
                                    <Text style={{ color: "red", marginLeft: scale(15) }}>Mobile Number Incorrect.</Text></View> : null}

                        <View style={[styles.SectionStyle, shadowStyle]}>
                            <TextInput
                                style={{

                                    height: scale(50),
                                    width: scale(250),

                                    fontSize: scale(15),
                                    borderRadius: 10,
                                    backgroundColor: '#fff',
                                }}
                                placeholder="Password"
                                keyboardType="default"
                                onChangeText={(password) => this.setState({ password })}
                                value={this.state.password}
                                secureTextEntry={this.state.showPassword} />

                            <TouchableOpacity activeOpacity={0.8} style={{
                                position: 'absolute',
                                right: 20,
                                alignSelf: "center",
                                height: 40,
                                width: 35,
                            }} onPress={this.toggleSwitch}>
                                <Image source={(this.state.showPassword) ? require('../../assets/HidePassword.png') : require('../../assets/ShowPassword.png')} style={{
                                    resizeMode: 'contain',
                                    height: '100%',
                                    alignSelf: "center",
                                    width: '100%',
                                }} />
                            </TouchableOpacity>

                        </View>

                        {this.state.error6 ?
                            <View style={{ width: scale(300), height: scale(20), marginTop: verticalScale(2) }}>
                                <Text style={{ color: "red", marginLeft: scale(15) }}>Please Enter Password</Text>
                            </View> :
                            this.state.error7 ?
                                <View style={{ width: scale(300), height: scale(20), marginTop: verticalScale(2) }}>
                                    <Text style={{ color: "red", marginLeft: scale(15), textAlign: "justify" }}>Password must contain: one capital letter, one number, one special character & a minimum of 8 characters</Text></View> : null}


                    </KeyboardAvoidingView>

                    <TouchableOpacity
                        style={[styles.btnSignin, shadowStyle]}
                        onPress={this._signUp}
                        //onPress={()=>this._popup()}
                        >
                        <LinearGradient
                            start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                            colors={['#a076e8', '#5dc4dd']}
                            style={styles.linearGradient}>
                            <Text style={{ color: '#FFF', fontWeight: 'bold', fontSize: scale(15) }}>Sign Up </Text>
                        </LinearGradient>
                    </TouchableOpacity>

                    <View style={{ flexDirection: 'row', width: scale(300), justifyContent: 'space-between' }}>
                        <TouchableOpacity
                            style={[styles.btn, shadowStyle]}
                            onPress={this.onGoogleLogin}>
                            <Image
                                style={{ width: scale(25), height: scale(25), borderWidth: 0, }}
                                source={require('../../assets/googleplus.png')}
                                resizeMode='contain' />
                            <Text style={{ color: '#db4a39', fontWeight: 'bold', fontSize: scale(14), marginLeft: scale(10) }}>Google </Text>
                        </TouchableOpacity>

                        {/* <TouchableOpacity
                            style={[styles.btn, shadowStyle]}>
                            <Image
                                style={{ width: 25, height: 25, borderWidth: 0, }}
                                source={require('../../assets/facebook.png')}
                                resizeMode='contain' />
                            <Text style={{ color: '#3b5998', fontWeight: 'bold', fontSize: 14, marginLeft: 10 }}>Facebook</Text>
                        </TouchableOpacity> */}
                        <FBLogin
                            buttonView={<FBLoginView />}
                            ref={(fbLogin) => { this.fbLogin = fbLogin }}
                            loginBehavior={FBLoginManager.LoginBehaviors.WebView}
                            permissions={["email", "user_friends"]}
                            onLogin={this.onFBLogin}
                            onLoginFound={this.onFBLogin}
                            onLoginNotFound={function (e) { console.log(e) }}
                            onLogout={function (e) { console.log(e) }}
                            onCancel={function (e) { console.log(e) }}
                            onPermissionsMissing={function (e) { console.log(e) }}
                        />
                    </View>

                    <View style={{ width: scale(300), justifyContent: 'center', marginTop: verticalScale(30), alignItems: "center" }}>

                        <TouchableOpacity
                            style={styles.txt}
                            onPress={() => this.props.navigation.navigate('Auth')}>
                            <Text style={{ color: '#707070', fontSize: scale(13), marginLeft: scale(10), alignSelf: "center" }}>Log In</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </ScrollView>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        margin: scale(15),
        height: scale(50),
        width: scale(300),
        paddingLeft: scale(28),
        fontSize: scale(15),
        borderRadius: 10,
        backgroundColor: '#fff',
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 10,
        position: "relative",
        height: scale(50),
        width: scale(300),
        marginTop: verticalScale(20)
    },
    linearGradient: {
        marginLeft: scale(20),
        borderRadius: 10,
        justifyContent: 'center',
        width: scale(300),
        alignItems: 'center',
        marginRight: scale(20),
        padding: scale(10),
        height: scale(50),
    },
    btnSignin: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        marginLeft: scale(20),
        borderRadius: 10,
        marginTop: verticalScale(40),
        width: scale(300),
        marginRight: scale(20),
        padding: scale(10),
        height: scale(50),
    },
    btn: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 10,
        padding: scale(10),
        height: scale(40),
        marginTop: verticalScale(30),
        width: scale(135),
    },
    txt: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: scale(10),
        height: scale(40),
        width: scale(135),
    }
})

function mapStateToProps(state) {
    return {
        userdata: state.register.userdata,
        loder: state.loder,
    }
}

export default connect(mapStateToProps, { saveUser,fbloginUser })(Signup)