import React from 'react';
import { StyleSheet, Dimensions, View,BackHandler } from 'react-native';
import Pdf from 'react-native-pdf';
import {connect} from 'react-redux';
import { userinfo ,loadingOn,loadingOff,changeSelectedChild} from '../../Action'
import { scale, moderateScale, verticalScale } from '../../Components/Scale';
//import RNHTMLtoPDF from 'react-native-html-to-pdf';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import Header from "../../Components/Header";
import HamburgerIcon from "../../Components/HamburgerIcon"
import { HeaderBackButton } from 'react-navigation-stack';
import axios from 'axios';
import RNFetchBlob from 'rn-fetch-blob'
import Spinner from "react-native-spinkit";
 
class View_As_PDF extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HamburgerIcon />,
        title: 'Vaccine Digital Report',
        headerBackground: (
            <Header />
        ),
        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: 20,
            fontFamily: "Roboto-Bold",
            fontWeight: "bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        }
    });
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            dataSource: [],
            isLoading: true,
            pdf:"",
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Vaccinedigitalreport');
        return true;
    }
    render() {
         const source = {uri:this.props.pdf,cache:true};
        if (this.props.loder) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: "center" }}>
                    {/* <ActivityIndicator /> */}
                    <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }} />
                </View>
            )
        }
        return (
            <View style={styles.container}>
                <Pdf
                    source={source}
                    style={styles.pdf}/>
            </View>
        )
  }
}
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: verticalScale(25),
    },
    pdf: {
        flex:1,
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
    }
});

const pdfurl = (data) => {
    console.log('data',data);
    return data.pdfUrl;
    
}


function mapStateToProps(state){
    let selectedChild = state.userinfo.selectedChild
    return{
        user:state.userinfo.user,
        pdf: pdfurl(state.userinfo.user[selectedChild]),
        loder: state.loder,
        
    }
}

export default connect(mapStateToProps,{userinfo,loadingOn,loadingOff,changeSelectedChild})(View_As_PDF)