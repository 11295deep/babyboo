import React from 'react';
import {
  StyleSheet,
  Dimensions,
  StatusBar,
  BackHandler,
} from 'react-native';
import Header from '../../Components/Header';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import HamburgerIcon from "../../Components/HamburgerIcon"
import Food from './Food';
import Developmental from './Developmental';
import { scale, moderateScale, verticalScale } from '../../Components/Scale';

class Milestone extends React.Component {
  static navigationOptions = ({navigation}) => ({
    title: '  Milestone  ',
    headerLeft: <HamburgerIcon />,
    headerBackground: <Header />,
    headerTitleStyle: {
      color: '#FFFFFF',
      fontSize: scale(20),
      fontFamily:"Roboto-Bold",
      fontWeight:"bold",
    },
    headerStyle: {
      backgroundColor: 'transparent',
    },
  });
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      index: 0,
      routes: [
        {key: 'developmental', title: ' Developmental '},
        {key: 'food', title: ' Food '},
      ],
    };
  }
  componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    this.props.navigation.navigate('Home');
    return true;
  }

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap({
          developmental: () => (
            <Developmental navigation={this.props.navigation} />
          ),
          food: Food,
        })}
        onIndexChange={index => this.setState({index})}
        initialLayout={{width: Dimensions.get('window').width}}
        renderTabBar={props => (
          <TabBar
            {...props}
            indicatorStyle={styles.indicator}
            // renderIcon={
            //     props => getTabBarIcon(props)
            // }
            activeColor="#A076E8"
            inactiveColor="#707070"
            style={styles.tabbar}
            labelStyle={styles.label}
          />
        )}
      />
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f4f7f9',
  },
  container2: {
    marginTop: StatusBar.currentHeight,
  },
  scene: {
    flex: 1,
  },
  tabbar: {
    backgroundColor: '#F4F7F9',
  },
  indicator: {
    backgroundColor: '#A076E8',
  },
  label: {
    fontWeight: 'bold',
    fontSize: scale(16),
  },
  input: {
    margin: scale(15),
    height: scale(50),
    width: scale(300),
    padding: scale(5),
    fontSize: scale(16),
    borderBottomWidth: 1,
    borderBottomColor: 'black',
  },
  btnSignin: {
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#707070',
    alignItems: 'center',
    marginLeft: scale(15),
    width: scale(300),
    marginRight: scale(15),
    padding: scale(10),
  },
});

export default Milestone;
