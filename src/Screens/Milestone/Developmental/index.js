import React from 'react';
import { View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    StatusBar,
    Picker,
    ScrollView,
} from 'react-native';
import MilestonePopUp from "../../../Components/MilestonePopUp";
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import Spinner from "react-native-spinkit";
import { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes,changeMilestone,Update_Milestone } from '../../../Action'
import { scale, moderateScale, verticalScale } from '../../../Components/Scale';

const shadowStyle = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 4,
  },
  shadowOpacity: 0.30,
  shadowRadius: 4.65,
  
  elevation: 8,
  };

class Developmental extends React.Component {
    constructor() {
        super();
        this.state = {
          PickerValueHolder: 'NewBorn',
          index: 0,
          data: [],
          dataSource: [],
          selected: [],
          Selected_data: [],
          milestoneData: [],
          dob: '',
          age: '',
          isModalVisible:false
        };
      }

      Save = async () => {
        const data = this.props.dataSource 
        const { selectedChild } = this.props;
        //alert('Saved successfully');
        const res = await this.props.Update_Milestone(selectedChild,data);
        if (res.status === 200) {
          this.setState({ isModalVisible: true })
          //alert('Saved successfully');
        } else{
          alert('Something is wrong');
        }
      };

      checkBox = (item1, item2, index) => {
        const { dataSource,selectedChild } = this.props;
        
        const dataSourceClone = JSON.parse(JSON.stringify(dataSource));
        dataSourceClone[item1][item2].values[index].selected = !dataSourceClone[item1][item2].values[index].selected;
        console.log("dataSourceClone",dataSourceClone);
        this.props.changeMilestone({[selectedChild]:dataSourceClone});
        };
        cancel() {
          this.setState({ isModalVisible: false })
      }

    render() {
      if (this.props.loder) {
        return (
          <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
            <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }} />
          </View>
        );
      }
      console.log('selectedChild',this.props.selectedChild);
        return (
          <View style={styles.container}>
          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
              <MilestonePopUp isModalVisible={this.state.isModalVisible}
                    onCancel={() => this.cancel()}
                    cancel_label="Continue"
                    text="Your baby crossed the milestone.."
                    image={require('../../../assets/popup3.png')} />
            <View style={{ flexDirection: 'row', marginTop: verticalScale(15), marginLeft: scale(20) }}>
              <Text style={{ alignSelf: 'center', fontSize: scale(16) }}>Select Age</Text>
              <View
                style={{
                  marginLeft: scale(15),
                  borderRadius: 8,
                  backgroundColor: '#ffffff',
                  width: '40%',
                }}>
                <Picker
                  selectedValue={this.state.PickerValueHolder}
                  style={{ height: scale(40), width: '100%' }}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({
                      PickerValueHolder: itemValue,
                      index: itemIndex
                    })
                  }>
                  {Object.keys(this.props.dataSource).map((item, key) => (
                    <Picker.Item label={item} value={item} key={key} />
                  ))}
                </Picker>
              </View>
              <View style={{ marginLeft: scale(15), marginRight: scale(15), flex: 1 }}>
                <TouchableOpacity style={styles.btnSignin} 
                onPress={this.Save}
                >
                  <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={['#a076e8', '#5dc4dd']}
                    style={styles.linearGradient}>
                    <Text
                      style={{
                        color: '#FFFFFF',
                        fontFamily: "Roboto-Bold",
                        fontWeight: "bold",
                        fontSize: scale(15),
                      }}>
                      SAVE
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </View>
  
            <View style={{ marginTop: verticalScale(15), marginLeft: scale(15) }}>
              {Object.keys(this.props.dataSource).map(item => (
                <View>
                  {this.state.PickerValueHolder === item
                    ? Object.keys(this.props.dataSource[item]).map(item2 => (
                      <View
                        style={{
                          borderTopStartRadius: 10,
                          width: scale(322),
                          borderTopEndRadius: 10,
                          borderBottomStartRadius: 10,
                          borderBottomEndRadius: 10,
                          backgroundColor: 'transparent',
                          marginTop: verticalScale(20),
                        }}>
                        {/* <LinearGradient
                          colors={[
                            this.props.dataSource[item][item2].color1,
                            this.props.dataSource[item][item2].color2,
                          ]} */}
                        <View
                          style={{
                            flex: 1,
                            borderTopStartRadius: 10,
                            borderTopEndRadius: 10,
                            height: scale(50),
                            alignItems: 'center',
                            backgroundColor: this.props.dataSource[item][item2].iconName ===
                            'grossMotor' ? "#EDDEFC":this.props.dataSource[item][item2].iconName ===
                            'fineMotor' ?"#FFEED1" : this.props.dataSource[item][item2].iconName ===
                            'cognitive' ? "#C5E8E9" : this.props.dataSource[item][item2].iconName ===
                            'speech' ? "#FFD2E1" : this.props.dataSource[item][item2].iconName ===
                            'sensory' ? "#D2F6FF" : this.props.dataSource[item][item2].iconName ===
                            'selfCare' ? "#FFEED1" : "#DFE3FC",
                            overflow: 'hidden',
                            flexDirection: 'row-reverse',
                          }}
                        // start={{ x: 0, y: 0 }}
                        // end={{ x: 1, y: 0 }}
                        >
                          <View style={{
                          width:"15%",
                        }}>
                          {this.props.dataSource[item][item2].iconName ===
                            'grossMotor' ? (
                              <Image
                                style={{ width: scale(20.95), height: scale(20),  }}
                                source={require('../../../assets/grossMotor.png')}
                                resizeMode="contain"
                              />
                            ) : this.props.dataSource[item][item2].iconName ===
                              'fineMotor' ? (
                                <Image
                                  style={{ width: scale(18.83), height: scale(20), }}
                                  source={require('../../../assets/fineMotor.png')}
                                  resizeMode="contain"
                                />
                              ) : this.props.dataSource[item][item2].iconName ===
                                'cognitive' ? (
                                  <Image
                                    style={{ width: scale(22.63), height: scale(20),  }}
                                    source={require('../../../assets/cognitive.png')}
                                    resizeMode="contain"
                                  />
                                ) : this.props.dataSource[item][item2].iconName ===
                                  'speech' ? (
                                    <Image
                                      style={{ width: scale(27.02), height: scale(20),  }}
                                      source={require('../../../assets/speech.png')}
                                      resizeMode="contain"
                                    />
                                  ) : this.props.dataSource[item][item2].iconName ===
                                    'sensory' ? (
                                      <Image
                                        style={{ width: scale(25.11), height: scale(18),  }}
                                        source={require('../../../assets/sensory.png')}
                                        resizeMode="contain"
                                      />
                                    ) : this.props.dataSource[item][item2].iconName ===
                                      'selfCare' ? (
                                        <Image
                                          style={{ width: scale(25.61), height: scale(20),  }}
                                          source={require('../../../assets/selfCare.png')}
                                          resizeMode="contain"
                                        />
                                      ) : (
                                        <Image
                                          style={{ width: scale(21.15), height: scale(20),  }}
                                          source={require('../../../assets/socialEmotional.png')}
                                          resizeMode="contain"
                                        />
                                      )}
                                      </View>
                        <View style={{
                          width:"85%",
                          //marginLeft: scale(20),
                        }}>
                          <Text
                            style={{
                              fontSize: scale(15),
                              color: '#000000',
                              fontFamily: "Roboto-Bold",
                              fontWeight: "bold",
                               marginLeft: scale(20),
                            }}>
                            {item2}
                          </Text>
                          </View>
                        </View>
                        <View
                          style={{
                            backgroundColor: '#FFFFFF',
                            borderBottomStartRadius: 10,
                            borderBottomEndRadius: 10,
                          }}>
                          {this.props.dataSource[item][item2].values.map(
                            (name, index) => {
                              let isDisabled;
                              
                              if (this.props.milestoneData[item][item2].values[index].selected || this.state.index > this.props.age ) {
                                //|| this.state.index > this.props.age
                                isDisabled = true;
                              }
                              return (
                                <View
                                style={{
                                  marginTop: verticalScale(15),
                                  flexDirection: 'row',
                                  justifyContent:"center",
                                  alignItems:"center",
                                  marginBottom: verticalScale(15),
                                }}>
                                <TouchableOpacity
                                 // disabled={isDisabled}
                                 disabled={isDisabled}
                                  onPress={() =>
                                    this.checkBox(
                                      item, item2, index
                                    )
                                  }
                                  underlayColor="transparent"
                                  style={{ marginVertical: verticalScale(10),  alignSelf: "center",width:"10%" }}>
                                  <View
                                    style={[
                                      shadowStyle,
                                      { borderRadius: 400 },
                                    ]}>
                                    {name.selected === true ? (
                                      <Image
                                        source={require('../../../assets/Checked.png')}
                                        style={{
                                          width: scale(20),
                                          height: scale(20),
                                          borderRadius: 400,
                                        }}
                                        resizeMode="contain"
                                      />
                                    ) : (
                                        <Image
                                          source={require('../../../assets/unChecked.png')}
                                          style={{
                                            width: scale(20),
                                            height: scale(20),
                                            borderRadius: 400,
                                          }}
                                          resizeMode="contain"
                                        />
                                      )}
                                  </View>
                                </TouchableOpacity>
                                <Text
                                  style={{
                                    fontSize: scale(15),
                                    color: '#707070',
                                    
                                    textAlign: 'justify',
                                    width:"85%"
                                  }}>
                                  {name.label}
                                </Text>
                              </View>
                              );
                            },
                          )}
                        </View>
                      </View>
                    ))
                    : null}
                </View>
              ))}
              <View style={{ height: scale(20) }} />
            </View>
          </ScrollView>
        </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f7f9',
      },
      MainContainer: {
        flex: 1,
        justifyContent: 'center',
      },
    
      container2: {
        marginTop: StatusBar.currentHeight,
      },
      scene: {
        flex: 1,
      },
      tabbar: {
        backgroundColor: '#F4F7F9',
      },
      indicator: {
        backgroundColor: '#A076E8',
      },
      label: {
        fontWeight: 'bold',
        fontSize: scale(12),
      },
      linearGradient: {
        borderRadius: 10,
        justifyContent: 'center',
        width: '100%',
        alignItems: 'center',
        height: scale(40),
      },
      input: {
        margin: scale(15),
        height: scale(50),
        width: scale(300),
        padding: scale(5),
        fontSize: scale(16),
        borderBottomWidth: 1,
        borderBottomColor: 'black',
      },
      btnSignin: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        borderRadius: 10,
        width: '100%',
        height: scale(40),
      },
})

const calculateAge = (dob) => {

  var dateString = dob;
      var dateParts = dateString.split("/");
      var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);

      var today = new Date();
      var birthDate = new Date(dateObject.toString());
      var age = today.getFullYear() - birthDate.getFullYear();
      var m = today.getMonth() - birthDate.getMonth();
      age = age * 12 + m;

      return age;
   
}

function mapStateToProps(state) {
  let selectedChild = state.userinfo.selectedChild
  console.log('selectedChild', state.userinfo.selectedChild);

  return {
    user: state.userinfo.user[selectedChild],
    selectedChild: selectedChild,
    age: calculateAge(state.userinfo.user[selectedChild].child.dob),
    loder: state.loder,
    dataSource: JSON.parse(JSON.stringify(state.milestone.milestonedata[selectedChild])),
    milestoneData: state.milestone.milestoneSavedata[selectedChild],
  }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes,changeMilestone,Update_Milestone })(Developmental)