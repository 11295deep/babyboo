import React from 'react';
import { Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    TouchableOpacity,
    Dimensions, StatusBar,
    BackHandler,
    Picker,
    ScrollView } from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../Components/Scale';

class Food extends React.Component {
    constructor() {
        super();
        this.state = {
            PickerValueHolder: 'NewBorn to 4 Months',
            age: [{
                label: "NewBorn to 4 Months"
            }, {
                label: "4 to 6 Months"
            }, {
                label: "6 to 8 Months"
            }, {
                label: "8 to 10 Months"
            }, {
                label: "10 to 12 Months"
            }, {
                label: "1 to 2 Years"
            }]
        }
    }

    render() {
        return (
            <View style={styles.container}>

                <View style={{ flexDirection: 'row', marginTop: verticalScale(15), marginLeft: scale(20) }}>
                    <Text style={{ alignSelf: "center", fontSize: scale(16) }}>Select Age</Text>
                    <View style={{ marginLeft: scale(25), borderRadius: 8, backgroundColor: "#ffffff" }}>
                        <Picker
                            selectedValue={this.state.PickerValueHolder}
                            style={{ height: scale(40), width: scale(220), }}
                            onValueChange={(itemValue, itemIndex) => this.setState({ PickerValueHolder: itemValue })} >
                            {this.state.age.map((item, key) => (
                                <Picker.Item label={item.label} value={item.label} key={key} />
                            )
                            )}
                        </Picker>
                    </View>
                </View>
                <View style={{ alignItems: "center", marginTop: verticalScale(20) }}>
                    {this.state.PickerValueHolder === "NewBorn to 4 Months" ?
                        <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(141), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                            <View
                                //colors={['#4300DB', '#C3A8FF']}
                                style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFF9CC", alignItems: 'center', }}
                            //start={{ x: 0, y: 0 }}
                            //end={{ x: 1, y: 0 }}
                            >
                                <View style={{
                                    width: "15%",
                                }}>
                                    <Image
                                        style={{ width: scale(11), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                        source={require('../../../assets/Milk.png')}
                                        resizeMode='contain'
                                    />
                                </View>
                                <View style={{
                                    width: "85%",
                                }}>
                                    <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Milk</Text>
                                </View>
                            </View>

                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Breast milk</Text>
                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Formula</Text>
                        </View>
                        :
                        this.state.PickerValueHolder === "4 to 6 Months" ?
                            <ScrollView showsHorizontalScrollIndicator={false}
                                showsVerticalScrollIndicator={false}>
                                <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(141), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                    <View
                                        //colors={['#4300DB', '#C3A8FF']}
                                        style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFF9CC", alignItems: 'center' }}
                                    //start={{ x: 0, y: 0 }}
                                    //end={{ x: 1, y: 0 }}
                                    >
                                        <View style={{
                                            width: "15%",
                                        }}>
                                            <Image
                                                style={{ width: scale(11), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                source={require('../../../assets/Milk.png')}
                                                resizeMode='contain'
                                            />
                                        </View>
                                        <View style={{
                                            width: "85%",
                                        }}>
                                            <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Milk</Text>
                                        </View>
                                    </View>

                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Breast milk</Text>
                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Formula</Text>
                                </View>
                                <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(141), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                    <View
                                        //colors={['#FF5B13', '#FFD7C6']}
                                        style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#DEE3FC", alignItems: 'center' }}
                                    //start={{ x: 0, y: 0 }}
                                    //end={{ x: 1, y: 0 }}
                                    >
                                        <View style={{
                                            width: "15%",
                                        }}>
                                            <Image
                                                style={{ width: scale(24), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                source={require('../../../assets/Fruits.png')}
                                                resizeMode='contain'
                                            />
                                        </View>
                                        <View style={{
                                            width: "85%",
                                        }}>
                                            <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Fruits</Text>
                                        </View>
                                    </View>

                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Banana</Text>
                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Apple</Text>
                                </View>
                                <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(181), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                    <View
                                        //colors={['#FF9D00', '#FFE2B2']}
                                        style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFEDD1", alignItems: 'center' }}
                                    //start={{ x: 0, y: 0 }}
                                    //end={{ x: 1, y: 0 }}
                                    ><View style={{
                                        width: "15%",
                                    }}>
                                            <Image
                                                style={{ width: scale(20), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                source={require('../../../assets/Wheat.png')}
                                                resizeMode='contain'
                                            />
                                        </View><View style={{
                                            width: "85%",
                                        }}>
                                            <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Grains</Text>
                                        </View>
                                    </View>

                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Single grain</Text>
                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Rice cereal</Text>
                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Oatmeal</Text>
                                </View>
                                <View style={{ height: scale(70) }} />
                            </ScrollView>
                            :
                            this.state.PickerValueHolder === "6 to 8 Months" ?
                                <ScrollView showsHorizontalScrollIndicator={false}
                                    showsVerticalScrollIndicator={false}>
                                    <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(141), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                        <View
                                            //colors={['#4300DB', '#C3A8FF']}
                                            style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFF9CC", alignItems: 'center' }}
                                        //start={{ x: 0, y: 0 }}
                                        //end={{ x: 1, y: 0 }}
                                        >
                                            <View style={{
                                                width: "15%",
                                            }}>
                                                <Image
                                                    style={{ width: scale(11), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                    source={require('../../../assets/Milk.png')}
                                                    resizeMode='contain'
                                                />
                                            </View>
                                            <View style={{
                                                width: "85%",
                                            }}>
                                                <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Milk</Text>
                                            </View>
                                        </View>

                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Breast milk</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Formula</Text>
                                    </View>
                                    <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(255), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                        <View
                                            //colors={['#FF5B13', '#FFD7C6']}
                                            style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#DEE3FC", alignItems: 'center' }}
                                        //start={{ x: 0, y: 0 }}
                                        //end={{ x: 1, y: 0 }}
                                        ><View style={{
                                            width: "15%",
                                        }}>
                                                <Image
                                                    style={{ width: scale(24), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                    source={require('../../../assets/Fruits.png')}
                                                    resizeMode='contain'
                                                /></View><View style={{
                                                    width: "85%",
                                                }}>
                                                <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Fruits</Text>
                                            </View>
                                        </View>

                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Banana</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Pears</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Apple Sauce</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Peaches</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Avocado</Text>
                                    </View>
                                    <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(181), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                        <View
                                            //colors={['#00CE96', '#BEFFED']}
                                            style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#C4E8E9", alignItems: 'center' }}
                                        //start={{ x: 0, y: 0 }}
                                        //end={{ x: 1, y: 0 }}
                                        ><View style={{
                                            width: "15%",
                                        }}>
                                                <Image
                                                    style={{ width: scale(32), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                    source={require('../../../assets/Vegetables.png')}
                                                    resizeMode='contain'
                                                />
                                            </View><View style={{
                                                width: "85%",
                                            }}>
                                                <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Vegetables</Text>
                                            </View>
                                        </View>

                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Carrots</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Squash</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Sweet Potato</Text>
                                    </View>
                                    <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(181), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                        <View
                                            //colors={['#FF9D00', '#FFE2B2']}
                                            style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFEDD1", alignItems: 'center' }}
                                        //start={{ x: 0, y: 0 }}
                                        //end={{ x: 1, y: 0 }}
                                        ><View style={{
                                            width: "15%",
                                        }}>
                                                <Image
                                                    style={{ width: scale(20), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                    source={require('../../../assets/Wheat.png')}
                                                    resizeMode='contain'
                                                />
                                            </View><View style={{
                                                width: "85%",
                                            }}>
                                                <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Grains</Text>
                                            </View>
                                        </View>

                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Oats</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Barley</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Ragi</Text>
                                    </View>
                                    <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(331), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                        <View
                                            //colors={['#00A1EF', '#89D9FF']}
                                            style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#D1F6FF", alignItems: 'center' }}
                                        //start={{ x: 0, y: 0 }}
                                        //end={{ x: 1, y: 0 }}
                                        ><View style={{
                                            width: "15%",
                                        }}>
                                                <Image
                                                    style={{ width: scale(23), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                    source={require('../../../assets/Beans.png')}
                                                    resizeMode='contain'
                                                />
                                            </View><View style={{
                                                width: "85%",
                                            }}>
                                                <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Beans</Text>
                                            </View>
                                        </View>

                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Black Beans</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Chickpeas</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Edamame</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Fava Beans</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Black-Eyed Peas</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Lentils</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Kidney Beans</Text>
                                    </View>
                                    <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(217), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                        <View
                                            //colors={['#CB0016', '#FFA5AF']}
                                            style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFD2E1", alignItems: 'center' }}
                                        //start={{ x: 0, y: 0 }}
                                        //end={{ x: 1, y: 0 }}
                                        ><View style={{
                                            width: "15%",
                                        }}>
                                                <Image
                                                    style={{ width: scale(23), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                    source={require('../../../assets/NonVeg.png')}
                                                    resizeMode='contain'
                                                />
                                            </View><View style={{
                                                width: "85%",
                                            }}>
                                                <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Non- Vegetarian</Text>
                                            </View>
                                        </View>

                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Egg</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Chicken</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Pork</Text>
                                        <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Beef</Text>
                                    </View>
                                    <View style={{ height: scale(70) }} />

                                </ScrollView>
                                :
                                this.state.PickerValueHolder === "8 to 10 Months" ?
                                    <ScrollView showsHorizontalScrollIndicator={false}
                                        showsVerticalScrollIndicator={false}>
                                        <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(255), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                            <View
                                                //colors={['#4300DB', '#C3A8FF']}
                                                style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFF9CC", alignItems: 'center' }}
                                            //start={{ x: 0, y: 0 }}
                                            //end={{ x: 1, y: 0 }}
                                            ><View style={{
                                                width: "15%",
                                            }}>
                                                    <Image
                                                        style={{ width: scale(11), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                        source={require('../../../assets/Milk.png')}
                                                        resizeMode='contain'
                                                    />
                                                </View><View style={{
                                                    width: "85%",
                                                }}>
                                                    <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Milk / Dairy</Text>
                                                </View>
                                            </View>

                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Breast milk</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Formula</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Soft Pasteurized Cheese</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Cottage Cheese</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Unsweetened Yogurt</Text>
                                        </View>
                                        <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(255), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                            <View
                                                //colors={['#FF5B13', '#FFD7C6']}
                                                style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#DEE3FC", alignItems: 'center' }}
                                            //start={{ x: 0, y: 0 }}
                                            //end={{ x: 1, y: 0 }}
                                            >
                                                <View style={{
                                                    width: "15%",
                                                }}>
                                                    <Image
                                                        style={{ width: scale(24), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                        source={require('../../../assets/Fruits.png')}
                                                        resizeMode='contain'
                                                    />
                                                </View>
                                                <View style={{
                                                    width: "85%",
                                                }}>
                                                    <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Fruits</Text>
                                                </View>
                                            </View>

                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Banana</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Pears</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Peaches</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Avocado</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Mango</Text>
                                        </View>
                                        <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(217), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                            <View
                                                //colors={['#00CE96', '#BEFFED']}
                                                style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#C4E8E9", alignItems: 'center' }}
                                            //start={{ x: 0, y: 0 }}
                                            //end={{ x: 1, y: 0 }}
                                            ><View style={{
                                                width: "15%",
                                            }}>
                                                    <Image
                                                        style={{ width: scale(32), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                        source={require('../../../assets/Vegetables.png')}
                                                        resizeMode='contain'
                                                    />
                                                </View><View style={{
                                                    width: "85%",
                                                }}>
                                                    <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Vegetables</Text>
                                                </View>
                                            </View>

                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Carrots</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Squash</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Sweet Potato</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Potato</Text>
                                        </View>
                                        <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(255), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                            <View
                                                //colors={['#FF9D00', '#FFE2B2']}
                                                style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFEDD1", alignItems: 'center' }}
                                            //start={{ x: 0, y: 0 }}
                                            //end={{ x: 1, y: 0 }}
                                            ><View style={{
                                                width: "15%",
                                            }}>
                                                    <Image
                                                        style={{ width: scale(20), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                        source={require('../../../assets/Wheat.png')}
                                                        resizeMode='contain'
                                                    />
                                                </View><View style={{
                                                    width: "85%",
                                                }}>
                                                    <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Grains</Text>
                                                </View>
                                            </View>

                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Oats</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Barley</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Ragi</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Wheat</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Mixed Cereals</Text>
                                        </View>
                                        <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(217), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                            <View
                                                //colors={['#00A1EF', '#89D9FF']}
                                                style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#D1F6FF", alignItems: 'center' }}
                                            //start={{ x: 0, y: 0 }}
                                            //end={{ x: 1, y: 0 }}
                                            ><View style={{
                                                width: "15%",
                                            }}>
                                                    <Image
                                                        style={{ width: scale(23), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                        source={require('../../../assets/Beans.png')}
                                                        resizeMode='contain'
                                                    />
                                                </View><View style={{
                                                    width: "85%",
                                                }}>
                                                    <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Beans</Text>
                                                </View>
                                            </View>

                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Lentils</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Pintos</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Split beans</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Black Beans</Text>
                                        </View>
                                        <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(141), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                            <View
                                                //colors={['#CB0016', '#FFA5AF']}
                                                style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFD2E1", alignItems: 'center' }}
                                            //start={{ x: 0, y: 0 }}
                                            //end={{ x: 1, y: 0 }}
                                            ><View style={{
                                                width: "15%",
                                            }}>
                                                    <Image
                                                        style={{ width: scale(23), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                        source={require('../../../assets/NonVeg.png')}
                                                        resizeMode='contain'
                                                    />
                                                </View><View style={{
                                                    width: "85%",
                                                }}>
                                                    <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Non- Vegetarian</Text>
                                                </View>
                                            </View>

                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Meat</Text>
                                            <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Fish</Text>
                                        </View>
                                        <View style={{ height: scale(70) }} />

                                    </ScrollView>
                                    :
                                    this.state.PickerValueHolder === "10 to 12 Months" ?
                                        <ScrollView showsHorizontalScrollIndicator={false}
                                            showsVerticalScrollIndicator={false}>
                                            <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(141), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                                <View
                                                    //colors={['#4300DB', '#C3A8FF']}
                                                    style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFF9CC", alignItems: 'center' }}
                                                //start={{ x: 0, y: 0 }}
                                                //end={{ x: 1, y: 0 }}
                                                ><View style={{
                                                    width: "15%",
                                                }}>
                                                        <Image
                                                            style={{ width: scale(11), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                            source={require('../../../assets/Milk.png')}
                                                            resizeMode='contain'
                                                        />
                                                    </View><View style={{
                                                        width: "85%",
                                                    }}>
                                                        <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Milk / Dairy</Text>
                                                    </View>
                                                </View>

                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Breast milk</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Formula</Text>
                                            </View>
                                            <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(255), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                                <View
                                                    //colors={['#FF5B13', '#FFD7C6']}
                                                    style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#DEE3FC", alignItems: 'center' }}
                                                //start={{ x: 0, y: 0 }}
                                                //end={{ x: 1, y: 0 }}
                                                ><View style={{
                                                    width: "15%",
                                                }}>
                                                        <Image
                                                            style={{ width: scale(24), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                            source={require('../../../assets/Fruits.png')}
                                                            resizeMode='contain'
                                                        />
                                                    </View><View style={{
                                                        width: "85%",
                                                    }}>
                                                        <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Fruits</Text>
                                                    </View>
                                                </View>

                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Banana</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Pears</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Apple</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Mango</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Avocado</Text>
                                            </View>
                                            <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(141), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                                <View
                                                    //colors={['#00CE96', '#BEFFED']}
                                                    style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#C4E8E9", alignItems: 'center' }}
                                                //start={{ x: 0, y: 0 }}
                                                //end={{ x: 1, y: 0 }}
                                                ><View style={{
                                                    width: "15%",
                                                }}>
                                                        <Image
                                                            style={{ width: scale(32), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                            source={require('../../../assets/Vegetables.png')}
                                                            resizeMode='contain'
                                                        />
                                                    </View><View style={{
                                                        width: "85%",
                                                    }}>
                                                        <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Vegetables</Text>
                                                    </View>
                                                </View>

                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Carrots</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Pea</Text>
                                            </View>
                                            <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(217), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                                <View
                                                    //colors={['#FF9D00', '#FFE2B2']}
                                                    style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFEDD1", alignItems: 'center' }}
                                                //start={{ x: 0, y: 0 }}
                                                //end={{ x: 1, y: 0 }}
                                                ><View style={{
                                                    width: "15%",
                                                }}>
                                                        <Image
                                                            style={{ width: scale(20), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                            source={require('../../../assets/Wheat.png')}
                                                            resizeMode='contain'
                                                        />
                                                    </View><View style={{
                                                        width: "85%",
                                                    }}>
                                                        <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Grains</Text>
                                                    </View>
                                                </View>


                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Wheat</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Oats</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Barley</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Ragi</Text>
                                            </View>
                                            <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(331), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                                <View
                                                    //colors={['#00A1EF', '#89D9FF']}
                                                    style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#D1F6FF", alignItems: 'center' }}
                                                //start={{ x: 0, y: 0 }}
                                                //end={{ x: 1, y: 0 }}
                                                ><View style={{
                                                    width: "15%",
                                                }}>
                                                        <Image
                                                            style={{ width: scale(23), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                            source={require('../../../assets/Beans.png')}
                                                            resizeMode='contain'
                                                        />
                                                    </View><View style={{
                                                        width: "85%",
                                                    }}>
                                                        <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Beans</Text>
                                                    </View>
                                                </View>

                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Black Beans</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Chickpeas</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Edamame</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Fava Beans</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Black-Eyed Peas</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Lentils</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Kidney Beans</Text>
                                            </View>
                                            <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(187), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                                <View
                                                    //colors={['#CB0016', '#FFA5AF']}
                                                    style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFD2E1", alignItems: 'center' }}
                                                //start={{ x: 0, y: 0 }}
                                                //end={{ x: 1, y: 0 }}
                                                ><View style={{
                                                    width: "15%",
                                                }}>
                                                        <Image
                                                            style={{ width: scale(23), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                            source={require('../../../assets/NonVeg.png')}
                                                            resizeMode='contain'
                                                        />
                                                    </View><View style={{
                                                        width: "85%",
                                                    }}>
                                                        <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Non- Vegetarian</Text>
                                                    </View>
                                                </View>

                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Meat</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Boneless Fish</Text>
                                                <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Poultry</Text>
                                            </View>
                                            <View style={{ height: scale(70) }} />

                                        </ScrollView>
                                        :
                                        this.state.PickerValueHolder === "1 to 2 Years" ?
                                            <ScrollView showsHorizontalScrollIndicator={false}
                                                showsVerticalScrollIndicator={false}>
                                                <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(255), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                                    <View
                                                        //colors={['#4300DB', '#C3A8FF']}
                                                        style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFF9CC", alignItems: 'center' }}
                                                    //start={{ x: 0, y: 0 }}
                                                    //end={{ x: 1, y: 0 }}
                                                    ><View style={{
                                                        width: "15%",
                                                    }}>
                                                            <Image
                                                                style={{ width: scale(11), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                                source={require('../../../assets/Milk.png')}
                                                                resizeMode='contain'
                                                            />
                                                        </View><View style={{
                                                            width: "85%",
                                                        }}>
                                                            <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Milk / Dairy</Text>
                                                        </View>
                                                    </View>

                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Breast milk</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Formula</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Soft Pasteurized Cheese</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Cottage Cheese</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Unsweetened Yogurt</Text>
                                                </View>
                                                <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(255), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                                    <View
                                                        //colors={['#FF5B13', '#FFD7C6']}
                                                        style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#DEE3FC", alignItems: 'center' }}
                                                    //start={{ x: 0, y: 0 }}
                                                    //end={{ x: 1, y: 0 }}
                                                    ><View style={{
                                                        width: "15%",
                                                    }}>
                                                            <Image
                                                                style={{ width: scale(24), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                                source={require('../../../assets/Fruits.png')}
                                                                resizeMode='contain'
                                                            />
                                                        </View><View style={{
                                                            width: "85%",
                                                        }}>
                                                            <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Fruits</Text>
                                                        </View>
                                                    </View>

                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Banana</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Pears</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Papaya</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Avocado</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Apple</Text>
                                                </View>
                                                <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(217), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                                    <View
                                                        //colors={['#00CE96', '#BEFFED']}
                                                        style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#C4E8E9", alignItems: 'center' }}
                                                    //start={{ x: 0, y: 0 }}
                                                    //end={{ x: 1, y: 0 }}
                                                    ><View style={{
                                                        width: "15%",
                                                    }}>
                                                            <Image
                                                                style={{ width: scale(32), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                                source={require('../../../assets/Vegetables.png')}
                                                                resizeMode='contain'
                                                            />
                                                        </View><View style={{
                                                            width: "85%",
                                                        }}>
                                                            <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Vegetables</Text>
                                                        </View>
                                                    </View>

                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Carrots</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Squash</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Sweet Potato</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Potato</Text>
                                                </View>
                                                <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(255), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                                    <View
                                                        //colors={['#FF9D00', '#FFE2B2']}
                                                        style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFEDD1", alignItems: 'center' }}
                                                    //start={{ x: 0, y: 0 }}
                                                    //end={{ x: 1, y: 0 }}
                                                    ><View style={{
                                                        width: "15%",
                                                    }}>
                                                            <Image
                                                                style={{ width: scale(20), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                                source={require('../../../assets/Wheat.png')}
                                                                resizeMode='contain'
                                                            />
                                                        </View><View style={{
                                                            width: "85%",
                                                        }}>
                                                            <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Grains</Text>
                                                        </View>
                                                    </View>

                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Oats</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Barley</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Ragi</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Wheat</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Mixed Cereals</Text>
                                                </View>
                                                <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(217), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                                    <View
                                                        //colors={['#00A1EF', '#89D9FF']}
                                                        style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#D1F6FF", alignItems: 'center' }}
                                                    //start={{ x: 0, y: 0 }}
                                                    //end={{ x: 1, y: 0 }}
                                                    ><View style={{
                                                        width: "15%",
                                                    }}>
                                                            <Image
                                                                style={{ width: scale(23), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                                source={require('../../../assets/Beans.png')}
                                                                resizeMode='contain'
                                                            />
                                                        </View><View style={{
                                                            width: "85%",
                                                        }}>
                                                            <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Beans</Text>
                                                        </View>
                                                    </View>

                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Lentils</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Pintos</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Split beans</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Black Beans</Text>

                                                </View>
                                                <View style={{ borderTopStartRadius: 10, width: scale(322), height: scale(255), marginTop: verticalScale(25), borderTopEndRadius: 10, borderBottomStartRadius: 10, borderBottomEndRadius: 10, backgroundColor: '#FFFFFF' }}>
                                                    <View
                                                        //colors={['#CB0016', '#FFA5AF']}
                                                        style={{ borderTopStartRadius: 10, borderTopEndRadius: 10, height: scale(50), overflow: 'hidden', flexDirection: "row-reverse", backgroundColor: "#FFD2E1", alignItems: 'center' }}
                                                    //start={{ x: 0, y: 0 }}
                                                    //end={{ x: 1, y: 0 }}
                                                    ><View style={{
                                                        width: "15%",
                                                    }}>
                                                            <Image
                                                                style={{ width: scale(23), height: scale(23), alignSelf: "center", marginLeft: scale(15) }}
                                                                source={require('../../../assets/NonVeg.png')}
                                                                resizeMode='contain'
                                                            />
                                                        </View><View style={{
                                                            width: "85%",
                                                        }}>
                                                            <Text style={{ fontSize: scale(18), marginLeft: scale(20), color: "#000000" }}>Non- Vegetarian</Text>
                                                        </View>
                                                    </View>

                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Chicken soup</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Egg Dishes</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Pork</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Fish</Text>
                                                    <Text style={{ marginLeft: scale(45), marginTop: verticalScale(15), fontSize: scale(16), color: "#707070" }}>Beef</Text>
                                                </View>
                                                <View style={{ height: scale(70) }} />
                                            </ScrollView> : null
                    }
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f7f9',
    },
    MainContainer: {
        flex: 1,
        justifyContent: 'center',
    },

    container2: {
        marginTop: StatusBar.currentHeight,
    },
    scene: {
        flex: 1,
    },
    tabbar: {
        backgroundColor: '#F4F7F9',
    },
    indicator: {
        backgroundColor: '#A076E8',
    },
    label: {
        fontWeight: 'bold',
        fontSize: scale(12),

    },
    input: {
        margin: scale(15),
        height: scale(50),
        width: scale(300),
        padding: scale(5),
        fontSize: scale(16),
        borderBottomWidth: 1,
        borderBottomColor: 'black',
    },
    btnSignin: {
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: '#707070',
        alignItems: 'center',
        marginLeft: scale(15),
        width: scale(300),
        marginRight: scale(15),
        padding: scale(10),
    }
})

export default Food;