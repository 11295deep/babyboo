import React from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  ImageBackground,
  Image,
  FlatList,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  BackHandler,
  RefreshControl,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes, Milestone } from '../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import Header from '../../Components/Header';
import Spinner from "react-native-spinkit";
import firebase from 'react-native-firebase';
import HamburgerIcon from "../../Components/HamburgerIcon"
import { BackgroundCarousel } from "../../Components/BackgroundCarousel";
import { scale, moderateScale, verticalScale } from '../../Components/Scale';

const screenDimension = Dimensions.get('window');
const isBigDevice = screenDimension.height > 600;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const shadowStyle = {
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.23,
  shadowRadius: 2.62,

  elevation: 4,
};

const SCROLLVIEW_REF = 'scrollview'

const images = [
  "babyboo1",
  "babyboo2",
  "babyboo3",
];

class Home extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: '  BabyBoo  ',
    headerLeft: <HamburgerIcon />,
    headerBackground: <Header />,
    headerTitleStyle: {
      color: '#FFFFFF',
      fontSize: scale(20),
      fontFamily: "Roboto-Bold",
      fontWeight: "bold",
    },
    headerStyle: {
      backgroundColor: 'transparent',
    },
  });

  constructor(props) {
    super(props);
    this._currentIndex = 0;
    this._childrenCount = 3;
    this.state = {
      vaccineDetails: [{
        "BCG": {
          "Description": "BCG, or bacille Calmette-Guerin, is a vaccine for tuberculosis (TB) disease. BCG is used in many countries with a high prevalence of TB to prevent childhood tuberculous meningitis and miliary disease.",
          "work": "The BCG vaccine contains a weakened strain of TB bacteria, which builds up immunity and encourages the body to fight TB if infected with it, without causing the disease itself"
        },
        "OPV 0": {
          "Description": "The action of oral polio vaccine (OPV) is two-pronged. OPV produces antibodies in the blood ('humoral' or serum immunity) to all three types of poliovirus, and in the event of infection, this protects the individual against polio paralysis by preventing the spread of poliovirus to the nervous system.",
          "work": "Inactivated polio vaccine (IPV)   ... IVP produces antibodies in the blood to all three types of poliovirus. In the event of infection, these antibodies prevent the spread of the virus to the central nervous system and protect against paralysis."
        },
        "OPV 1": {
          "Description": "The action of oral polio vaccine (OPV) is two-pronged. OPV produces antibodies in the blood ('humoral' or serum immunity) to all three types of poliovirus, and in the event of infection, this protects the individual against polio paralysis by preventing the spread of poliovirus to the nervous system.",
          "work": "Inactivated polio vaccine (IPV)   ... IVP produces antibodies in the blood to all three types of poliovirus. In the event of infection, these antibodies prevent the spread of the virus to the central nervous system and protect against paralysis."
        },
        "OPV 2": {
          "Description": "The action of oral polio vaccine (OPV) is two-pronged. OPV produces antibodies in the blood ('humoral' or serum immunity) to all three types of poliovirus, and in the event of infection, this protects the individual against polio paralysis by preventing the spread of poliovirus to the nervous system.",
          "work": "Inactivated polio vaccine (IPV)   ... IVP produces antibodies in the blood to all three types of poliovirus. In the event of infection, these antibodies prevent the spread of the virus to the central nervous system and protect against paralysis."
        },
        "HEP-B1": {
          "Description": "Hepatitis B is an infection of your liver. It can cause scarring of the organ, liver failure, and cancer. It can be fatal if it isn’t treated. It’s spread when people come in contact with the blood, open sores, or body fluids of someone who has the hepatitis B virus. It's serious, but if you get the disease as an adult, it shouldn’t last a long time. Your body fights it off within a few months, and you’re immune for the rest of your life. That means you can't get it again. But if you get it at birth, it’ unlikely to go away.",
          "work": "The hepatitis B vaccine stimulates your natural immune system to protect against the hepatitis B e hepatitis B virus in the future."
        },
        "Hep-B2": {
          "Description": "Hepatitis B is an infection of your liver. It can cause scarring of the organ, liver failure, and cancer. It can be fatal if it isn’t treated. It’s spread when people come in contact with the blood, open sores, or body fluids of someone who has the hepatitis B virus. It's serious, but if you get the disease as an adult, it shouldn’t last a long time. Your body fights it off within a few months, and you’re immune for the rest of your life. That means you can't get it again. But if you get it at birth, it’ unlikely to go away.",
          "work": "The hepatitis B vaccine stimulates your natural immune system to protect against the hepatitis B e hepatitis B virus in the future."
        },
        "HEP-B3": {
          "Description": "Hepatitis B is an infection of your liver. It can cause scarring of the organ, liver failure, and cancer. It can be fatal if it isn’t treated. It’s spread when people come in contact with the blood, open sores, or body fluids of someone who has the hepatitis B virus. It's serious, but if you get the disease as an adult, it shouldn’t last a long time. Your body fights it off within a few months, and you’re immune for the rest of your life. That means you can't get it again. But if you get it at birth, it’ unlikely to go away.",
          "work": "The hepatitis B vaccine stimulates your natural immune system to protect against the hepatitis B e hepatitis B virus in the future."
        },
        "DTwP 1": {
          "Description": "DPT (also DTP and DTwP) is a class of combination vaccines against three infectious diseases in humans: diphtheria, pertussis (whooping cough), and tetanus in which the pertussis component is acellular. This is in contrast to whole-cell, inactivated DTP (DTwP)",
          "work": "The acellular vaccine uses selected antigens of the pertussis pathogen to induce immunity. Because it uses fewer antigens than the whole-cell vaccines, it is considered to cause fewer side effects, but it is also more expensive."
        },
        "IPV 1": {
          "Description": "The vaccines available for vaccination against polio are the IPV (inactivated polio vaccine) and the OPV (oral polio vaccine). IPV (inactivated polio vaccine) is given as a shot in the arm or leg. OPV (oral polio vaccine) is the preferred vaccine for most children.",
          "work": "IPV is given by intramuscular or intradermal injection and needs to be administered by a trained health worker. IVP produces antibodies in the blood to all three types of poliovirus. In the event of infection, these antibodies prevent the spread of the virus to the central nervous system and protect against paralysis."
        },
        "Hib 1": {
          "Description": "The Hib vaccine is an injection that helps protect children from contracting infections due to Haemophilus influenzae type B (Hib), a bacterium that is capable of causing serious illness and potential death in children under age five.",
          "work": "The vaccine provides long-term protection from Haemophilus influenzae type b. Those who are immunized have protection against Hib meningitis; pneumonia; pericarditis (an infection of the membrane covering the heart); and infections of the blood, bones, and joints caused by the bacteria."
        },
        "RV 1": {
          "Description": "Rotavirus vaccine is a vaccine used to protect against rotavirus infections, which are the leading cause of severe diarrhea among young children. The vaccines prevent 15–34% of severe diarrhea in the developing world and 37–96% of severe diarrhea in the developed world.",
          "work": "The vaccine contains live human rotavirus that has been weakened (attenuated), so that it stimulates the immune system but does not cause disease in healthy people. However it should not be given to people who are clinically immunosuppressed (either due to drug treatment or underlying illness). This is because the vaccine strain could replicate too much and cause a serious infection. This includes babies whose mothers have had immunosuppressive treatment while they were pregnant or breastfeeding."
        },
        "PCV 1": {
          "Description": "Pneumococcal conjugate vaccine (PCV) is a pneumococcal vaccine and a conjugate vaccine used to protect infants, young children, and adults against disease caused by the bacterium Streptococcus pneumoniae (the pneumococcus).",
          "work": "The pneumococcal conjugate vaccine (PCV13) and the pneumococcal polysaccharide vaccine (PPSV23) protect against pneumococcal infections, which are caused by bacteria. The bacteria spread through person-to-person contact and can cause such serious infections as pneumonia, blood infections, and bacterial meningitis."
        },
        "DTwP 2": {
          "Description": "DPT (also DTP and DTwP) is a class of combination vaccines against three infectious diseases in humans: diphtheria, pertussis (whooping cough), and tetanus in which the pertussis component is acellular. This is in contrast to whole-cell, inactivated DTP (DTwP)",
          "work": "The acellular vaccine uses selected antigens of the pertussis pathogen to induce immunity. Because it uses fewer antigens than the whole-cell vaccines, it is considered to cause fewer side effects, but it is also more expensive."
        },
        "IPV 2": {
          "Description": "The vaccines available for vaccination against polio are the IPV (inactivated polio vaccine) and the OPV (oral polio vaccine). IPV (inactivated polio vaccine) is given as a shot in the arm or leg. OPV (oral polio vaccine) is the preferred vaccine for most children.",
          "work": "IPV is given by intramuscular or intradermal injection and needs to be administered by a trained health worker. IVP produces antibodies in the blood to all three types of poliovirus. In the event of infection, these antibodies prevent the spread of the virus to the central nervous system and protect against paralysis."
        },
        "Hib 2": {
          "Description": "The Hib vaccine is an injection that helps protect children from contracting infections due to Haemophilus influenzae type B (Hib), a bacterium that is capable of causing serious illness and potential death in children under age five.",
          "work": "The vaccine provides long-term protection from Haemophilus influenzae type b. Those who are immunized have protection against Hib meningitis; pneumonia; pericarditis (an infection of the membrane covering the heart); and infections of the blood, bones, and joints caused by the bacteria."
        },
        "RV 2": {
          "Description": "Rotavirus vaccine is a vaccine used to protect against rotavirus infections, which are the leading cause of severe diarrhea among young children. The vaccines prevent 15–34% of severe diarrhea in the developing world and 37–96% of severe diarrhea in the developed world.",
          "work": "The vaccine contains live human rotavirus that has been weakened (attenuated), so that it stimulates the immune system but does not cause disease in healthy people. However it should not be given to people who are clinically immunosuppressed (either due to drug treatment or underlying illness). This is because the vaccine strain could replicate too much and cause a serious infection. This includes babies whose mothers have had immunosuppressive treatment while they were pregnant or breastfeeding."
        },
        "PCV 2": {
          "Description": "Pneumococcal conjugate vaccine (PCV) is a pneumococcal vaccine and a conjugate vaccine used to protect infants, young children, and adults against disease caused by the bacterium Streptococcus pneumoniae (the pneumococcus).",
          "work": "The pneumococcal conjugate vaccine (PCV13) and the pneumococcal polysaccharide vaccine (PPSV23) protect against pneumococcal infections, which are caused by bacteria. The bacteria spread through person-to-person contact and can cause such serious infections as pneumonia, blood infections, and bacterial meningitis."
        },
        "DTwP 3": {
          "Description": "DPT (also DTP and DTwP) is a class of combination vaccines against three infectious diseases in humans: diphtheria, pertussis (whooping cough), and tetanus in which the pertussis component is acellular. This is in contrast to whole-cell, inactivated DTP (DTwP)",
          "work": "The acellular vaccine uses selected antigens of the pertussis pathogen to induce immunity. Because it uses fewer antigens than the whole-cell vaccines, it is considered to cause fewer side effects, but it is also more expensive."
        },
        "IPV 3": {
          "Description": "The vaccines available for vaccination against polio are the IPV (inactivated polio vaccine) and the OPV (oral polio vaccine). IPV (inactivated polio vaccine) is given as a shot in the arm or leg. OPV (oral polio vaccine) is the preferred vaccine for most children.",
          "work": "IPV is given by intramuscular or intradermal injection and needs to be administered by a trained health worker. IVP produces antibodies in the blood to all three types of poliovirus. In the event of infection, these antibodies prevent the spread of the virus to the central nervous system and protect against paralysis."
        },
        "Hib 3": {
          "Description": "The Hib vaccine is an injection that helps protect children from contracting infections due to Haemophilus influenzae type B (Hib), a bacterium that is capable of causing serious illness and potential death in children under age five.",
          "work": "The vaccine provides long-term protection from Haemophilus influenzae type b. Those who are immunized have protection against Hib meningitis; pneumonia; pericarditis (an infection of the membrane covering the heart); and infections of the blood, bones, and joints caused by the bacteria."
        },
        "RV 3": {
          "Description": "Rotavirus vaccine is a vaccine used to protect against rotavirus infections, which are the leading cause of severe diarrhea among young children. The vaccines prevent 15–34% of severe diarrhea in the developing world and 37–96% of severe diarrhea in the developed world.",
          "work": "The vaccine contains live human rotavirus that has been weakened (attenuated), so that it stimulates the immune system but does not cause disease in healthy people. However it should not be given to people who are clinically immunosuppressed (either due to drug treatment or underlying illness). This is because the vaccine strain could replicate too much and cause a serious infection. This includes babies whose mothers have had immunosuppressive treatment while they were pregnant or breastfeeding."
        },
        "PCV 3": {
          "Description": "Pneumococcal conjugate vaccine (PCV) is a pneumococcal vaccine and a conjugate vaccine used to protect infants, young children, and adults against disease caused by the bacterium Streptococcus pneumoniae (the pneumococcus).",
          "work": "The pneumococcal conjugate vaccine (PCV13) and the pneumococcal polysaccharide vaccine (PPSV23) protect against pneumococcal infections, which are caused by bacteria. The bacteria spread through person-to-person contact and can cause such serious infections as pneumonia, blood infections, and bacterial meningitis."
        },
        "MMR-1": {
          "Description": "The MMR vaccine is a vaccine against measles, mumps, and rubella.The vaccine is also recommended in those who do not have evidence of immunity,those with well controlled HIV/AIDSand within 72 hours of exposure to measles among those who are incompletely immunized.It is given by injection.",
          "work": "The MMR vaccine contains weakened versions of live measles, mumps and rubella viruses. The vaccine works by triggering the immune system to produce antibodies against measles, mumps and rubella."
        },
        "TCV": {
          "Description": "TCV is a typhoid conjugate vaccine manufactured by Bharat Biotech International Limited. It contains purified Vi capsular polysaccharide of Salmonella enterica serovar typhi Ty2 conjugated to a tetanus toxoid carrier protein.",
          "work": "This vaccine works by exposing you to a small amount of the bacteria, which causes your body to develop immunity to the disease. Typhoid vaccine will not treat an active infection that has already developed in the body, and will not prevent any disease caused by bacteria other than Salmonella typhi. typhoid conjugate vaccine is safe, effective, and can provide protection for infants and children under two years of age."
        },
        "VARICELLA": {
          "Description": "Varicella vaccine, also known as chickenpox vaccine, is a vaccine that protects against chickenpox. One dose of vaccine prevents 95% of moderate disease and 100% of severe disease.",
          "work": "The chickenpox vaccine is a live vaccine and contains a small amount of weakened chickenpox-causing virus. The vaccine stimulates your immune system to produce antibodies that will help protect against chickenpox."
        },
        "HPV": {
          "Description": "A vaccine that helps protect the body against infection with certain types of human papillomavirus (HPV). HPV infection can cause abnormal tissue growth, such as warts, and other changes to cells. Infection for a long time with certain types of HPV can cause cancers of the cervix, vagina, vulva, anus, penis, and oropharynx. HPV vaccines are being used to prevent some of these cancers. They are also being used to prevent genital warts and abnormal lesions that may lead to some of these cancers. Also called human papillomavirus vaccine.",
          "work": "HPV vaccine can prevent most genital warts and most cases of cervical cancer. Protection from HPV vaccine is expected to be long-lasting. But vaccinated women still need cervical cancer screening because the vaccine does not protect against all HPV types that cause cervical cancer."
        }
      }],
      isLoading: true,
      isModalVisible: false,
      date: '',
      isModalVisible2: false,
      height: 0,
      autoPlay: true,
      image: '',
      alert: false,
    };
  }
  componentDidMount() {
    this.GetData();
  }

  GetData = async () => {
    this.checkPermission();
    this.messageListener();
    await this.props.userinfo()
    await this.props.changeSelectedChild(0)
    await this.props.rhymes()
    await this.props.Milestone()
  }

  checkPermission = async () => {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getFcmToken();
    } else {
      this.requestPermission();
    }
  }

  getFcmToken = async () => {
    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      console.log(fcmToken);
      const token = await AsyncStorage.getItem('token');
      axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
      const response = await axios.post(`${url}/updateDeviceToken`, { deviceToken: fcmToken });
      // this.showAlert('Your Firebase Token is:', fcmToken);
      console.log('Your Firebase Token is:', fcmToken);
    } else {
      //  this.showAlert('Failed', 'No token received');
      console.log('Failed', 'No token received');
    }
  }

  requestPermission = async () => {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getFcmToken();
    } catch (error) {
      // User has rejected permissions
      console.log('User has rejected permissions');
    }
  }

  messageListener = async () => {
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { title, body } = notification;
      if (!this.state.alert) {
        this.showAlert(title, body);
      }
      console.log(title, body);
    });

    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { title, body } = notificationOpen.notification._data;
      // this.showAlert(title, body);
      console.log(title, body);
    });

    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification._data;
      // this.showAlert(title, body);
      console.log(title, body);
    }

    this.messageListener = firebase.messaging().onMessage((message) => {
      console.log(JSON.stringify(message));
    });
  }

  showAlert = (title, message) => {
    this.setState({ alert: true });
    Alert.alert(
      title,
      message,
      [
        {
          text: 'OK', onPress: () => {
            console.log('OK Pressed')
            this.setState({ alert: false })
          }
        },
      ],
      { cancelable: false },
    );
  }

  add_another_child = async () => {
    await AsyncStorage.removeItem('token');
    return this.props.navigation.navigate('Auth');
  }

  render() {
    const selectedchild = 0;
    console.log('mychild', this.props.user);
    //console.log('ccc',this.props.child.child.name);


    if (this.props.loder) {
      return (
        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
          <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }} />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        {/* <Text onPress={this.add_another_child()}>hiiii</Text> */}
        <Modal
          isVisible={this.state.isModalVisible}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 20,
          }}
          animationInTiming={1000}
          animationOutTiming={1000}
          backdropTransitionInTiming={1000}
          backdropTransitionOutTiming={1000}>
          <View style={{ width: scale(300), height: scale(300), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>
            <Image
              style={{ width: scale(120), height: scale(120), }}
              source={require('../../assets/reminder.png')}
              resizeMode="contain"
            />
            <Text style={{ fontSize: scale(18), textAlign: 'justify', marginLeft: scale(7), marginRight: scale(7), fontWeight: "bold", marginTop: verticalScale(5) }}>Vaccine Reminder</Text>
            <Text style={{ fontSize: scale(15), textAlign: 'center', marginLeft: scale(7), marginRight: scale(7), marginTop: verticalScale(10) }}>Did you book the appointment for your Child's Next Due Vaccination?</Text>
            <Text style={{ fontSize: scale(15), textAlign: 'center', marginLeft: scale(7), marginRight: scale(7), fontWeight: "bold" }}>Just 2 Days Left!</Text>

            <TouchableOpacity
              style={styles.btnSignin}
              onPress={() => this.setState({ isModalVisible: false })}>
              <LinearGradient
                start={{ x: 0.0, y: 0.25 }} end={{ x: 0.5, y: 1.0 }}
                locations={[0, 0.5, 0.6]}
                colors={['#a076e8', '#5dc4dd']}
                style={styles.linearGradient}>
                <Text style={{ color: '#FFFFFF', fontWeight: 'bold', fontSize: scale(15) }}>Got it!</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>

        </Modal>

        <Modal
          isVisible={this.state.isModalVisible2}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 20,
          }}
          animationInTiming={1000}
          animationOutTiming={1000}
          backdropTransitionInTiming={1000}
          backdropTransitionOutTiming={1000}>
          <View style={{ width: scale(300), height: scale(300), backgroundColor: "#ffffff", borderRadius: 20, justifyContent: "center", alignItems: "center" }}>
            <Image
              style={{ width: scale(120), height: scale(120), }}
              source={require('../../assets/reminder.png')}
              resizeMode="contain"
            />
            <Text style={{ fontSize: scale(18), textAlign: 'justify', marginLeft: scale(7), marginRight: scale(7), fontWeight: "bold", marginTop: verticalScale(5) }}>Vaccine Reminder</Text>
            <Text style={{ fontSize: scale(15), textAlign: 'center', marginLeft: scale(7), marginRight: scale(7), marginTop: verticalScale(10) }}>Hurry up! Just 1 day left for your Child’s next Vaccine!</Text>

            <TouchableOpacity
              style={styles.btnSignin}
              onPress={() => this.setState({ isModalVisible2: false })}>
              <LinearGradient
                start={{ x: 0.0, y: 0.25 }} end={{ x: 0.5, y: 1.0 }}
                locations={[0, 0.5, 0.6]}
                colors={['#a076e8', '#5dc4dd']}
                style={styles.linearGradient}>
                <Text style={{ color: '#FFFFFF', fontWeight: 'bold', fontSize: scale(15) }}>Got it!</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>

        </Modal>

        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}

        >
          <View style={{ height: scale(20) }} />

          <View
            style={[
              {
                width: scale(322),
                height: scale(200),
                backgroundColor: '#FFFFFF',
                borderRadius: 10,
              },
              shadowStyle,
            ]}>
            <BackgroundCarousel images={images} />
          </View>

          <View style={{
            marginTop: verticalScale(21.18),
            height: scale(90),
            width: scale(322),
            borderRadius: 8,
            backgroundColor: "#fff",
            flexDirection: "row"
          }}>

            <View style={{
              width: "30%",
              justifyContent: "center",
              alignItems: "center"
            }}>
              <Image
                style={{
                  width: scale(60),
                  height: scale(60),
                  borderRadius: 400,
                }}
                source={{ uri: this.props.user.imageUrl }}
                resizeMode="cover"
              />

            </View>

            <View style={{
              width: "70%",
              flexDirection: "column",
              justifyContent: "center"
            }}>
              <Text
                style={{
                  fontSize: scale(18),
                  fontFamily: "Roboto-Bold",
                  fontWeight: "bold",
                  color: '#000',
                }}>
                {this.props.user.child.name}
              </Text>
              <Text
                style={{
                  fontSize: scale(13),
                  fontFamily: "Roboto-Regular",
                  marginTop: verticalScale(14),
                  color: '#000',
                }}>
                {this.props.ageInWeek}
              </Text>
            </View>
          </View>

          {/* <View style={{
            marginTop: verticalScale(20),
            width: scale(322),
            flexDirection: "row",
            alignItems: "center"
          }}>
            <View style={{
              width: "80%"
            }}>
              <Text style={{
                fontSize: scale(18),
                fontFamily: "Roboto-Bold",
                color: '#707070',
              }}>Growth & Developmental Milestones</Text>
            </View>

            <View style={{
              width: "20%"
            }}>
              <TouchableOpacity>
                <Text style={{
                  fontSize: scale(13),
                  fontFamily: "Roboto-Regular",
                  color: '#707070',
                }}>View All</Text>
              </TouchableOpacity>
            </View>

          </View> */}

          <View style={{
            marginTop: verticalScale(20),
            height: scale(214),
            width: scale(322),
            backgroundColor: "transparent",
            flexDirection: "row",
            justifyContent: "space-between"
          }}>

            <View style={{
              height: scale(214),
              width: scale(150),
              backgroundColor: "#fff",
              borderRadius: 8,
              flexDirection: "column",
            }}>
              <View style={{
                flexDirection: "row",
                marginTop: verticalScale(10),
                height: scale(28),
                marginRight: scale(10),
                marginLeft: scale(15)
              }}>
                <View style={{
                  width: "80%",

                }}>
                  <Text
                    style={{
                      fontSize: scale(18),
                      fontFamily: "Roboto-Regular",
                      color: '#000',
                    }}>
                    Vaccines
                  </Text>
                </View>
                <View style={{
                  width: "20%",
                  backgroundColor: "transparent",

                  borderRadius: 400
                }}>
                  <LinearGradient
                    start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                    colors={['#a076e8', '#5dc4dd']}
                    style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                    <Image
                      style={{
                        width: scale(16.08),
                        height: scale(16.08),
                      }}
                      source={require('../../assets/injection2.png')}
                    />
                  </LinearGradient>
                </View>
              </View>

              <View style={{
                marginTop: verticalScale(29),
                justifyContent: "center",
                alignItems: "center"
              }}>
                <AnimatedCircularProgress
                  size={scale(116)}
                  width={scale(10)}
                  backgroundWidth={scale(10)}
                  fill={this.props.completePerc}
                  tintColor="#A076E8"
                  arcSweepAngle={360}
                  rotation={360}
                  backgroundColor="#DFF2FF"
                  lineCap="round">
                  {(fill) => (
                    <View>
                      <Text style={{ fontSize: scale(18), fontFamily: "Roboto-Bold", alignSelf: "center", color: "#A076E8" }}>{this.props.completeVaccines}/14</Text>
                      <Text style={{ fontSize: scale(11), fontFamily: "Roboto-Bold", alignSelf: "center", color: "#707070" }}>Completed</Text>
                    </View>
                  )}
                </AnimatedCircularProgress>
              </View>

            </View>

            <View style={{
              height: scale(214),
              width: scale(150),
              backgroundColor: "#fff",
              borderRadius: 8
            }}>
              <View style={{
                flexDirection: "row",
                marginTop: verticalScale(10),
                height: scale(28),
                marginRight: scale(10),
                marginLeft: scale(15)
              }}>
                <View style={{
                  width: "80%",
                }}>
                  <Text
                    style={{
                      fontSize: scale(18),
                      fontFamily: "Roboto-Regular",
                      color: '#000',
                    }}>
                    Next Due Vaccine
                  </Text>
                </View>
                <View style={{
                  width: "20%",
                  backgroundColor: "transparent",

                  borderRadius: 400
                }}>
                  <LinearGradient
                    start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                    colors={['#a076e8', '#5dc4dd']}
                    style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                    <Image
                      style={{
                        width: scale(15.7),
                        height: scale(15.7),
                      }}
                      source={require('../../assets/calendar.png')}
                    />
                  </LinearGradient>
                </View>
              </View>
              <View style={{
                marginTop: verticalScale(15),
                // marginLeft: scale(81),
                marginRight: scale(10),
                alignItems: "flex-end"
              }}>
                <Text
                  style={{
                    fontSize: scale(11),
                    fontFamily: "Roboto-Bold",
                    color: '#A076E8',
                  }}>
                  {this.props.nextVaccine[0].datenum} {this.props.nextVaccine[0].month}, {this.props.nextVaccine[0].year}
                </Text>
                <Text
                  style={{
                    fontSize: scale(11),
                    fontFamily: "Roboto-Regular",
                    color: '#707070',
                  }}>
                  {this.props.nextVaccine[0].day}
                </Text>
              </View>
              <View style={{
                marginLeft: scale(15),
              }}>
                <View style={{
                  flexDirection: "row",
                  alignItems: "center"
                }}>
                  <Image
                    style={{
                      width: scale(13.67),
                      height: scale(13.67),
                    }}
                    source={require('../../assets/treatment.png')}
                  />
                  <Text style={{
                    fontSize: scale(12),
                    fontFamily: "Roboto-Regular",
                    color: '#000000',
                    marginLeft: scale(10.33)
                  }}>{this.props.nextVaccine[`0`].vaccine1}</Text>
                </View>

                {this.props.nextVaccine[`0`].vaccine2 ?
                  <View style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: verticalScale(5)
                  }}>
                    <Image
                      style={{
                        width: scale(13.67),
                        height: scale(13.67),
                      }}
                      source={require('../../assets/treatment.png')}
                    />
                    <Text style={{
                      fontSize: scale(12),
                      fontFamily: "Roboto-Regular",
                      color: '#000000',
                      marginLeft: scale(10.33)
                    }}>{this.props.nextVaccine[`0`].vaccine2}</Text>
                  </View> : null}

                {this.props.nextVaccine[`0`].vaccine3 ?
                  <View style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: verticalScale(5)
                  }}>
                    <Image
                      style={{
                        width: scale(13.67),
                        height: scale(13.67),
                      }}
                      source={require('../../assets/treatment.png')}
                    />
                    <Text style={{
                      fontSize: scale(12),
                      fontFamily: "Roboto-Regular",
                      color: '#000000',
                      marginLeft: scale(10.33)
                    }}>{this.props.nextVaccine[`0`].vaccine3}</Text>
                  </View> : null}

                {this.props.nextVaccine[`0`].vaccine4 ?
                  <View style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: verticalScale(5)
                  }}>
                    <Image
                      style={{
                        width: scale(13.67),
                        height: scale(13.67),
                      }}
                      source={require('../../assets/treatment.png')}
                    />
                    <Text style={{
                      fontSize: scale(12),
                      fontFamily: "Roboto-Regular",
                      color: '#000000',
                      marginLeft: scale(10.33)
                    }}>{this.props.nextVaccine[`0`].vaccine4}</Text>
                  </View> : null}

                {this.props.nextVaccine[`0`].vaccine5 ?
                  <View style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: verticalScale(5)
                  }}>
                    <Image
                      style={{
                        width: scale(13.67),
                        height: scale(13.67),
                      }}
                      source={require('../../assets/treatment.png')}
                    />
                    <Text style={{
                      fontSize: scale(12),
                      fontFamily: "Roboto-Regular",
                      color: '#000000',
                      marginLeft: scale(10.33)
                    }}>{this.props.nextVaccine[`0`].vaccine5}</Text>
                  </View> : null}

                {this.props.nextVaccine[`0`].vaccine6 ?
                  <View style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: verticalScale(5)
                  }}>
                    <Image
                      style={{
                        width: scale(13.67),
                        height: scale(13.67),
                      }}
                      source={require('../../assets/treatment.png')}
                    />
                    <Text style={{
                      fontSize: scale(12),
                      fontFamily: "Roboto-Regular",
                      color: '#000000',
                      marginLeft: scale(10.33)
                    }}>{this.props.nextVaccine[`0`].vaccine6}</Text>
                  </View> : null}


              </View>
            </View>

          </View>

          <View style={{
            marginTop: verticalScale(20),
            width: scale(322),
            flexDirection: "row",
            alignItems: "center"
          }}>
            <View style={{
              width: "80%"
            }}>
              <Text style={{
                fontSize: scale(18),
                fontFamily: "Roboto-Bold",
                color: '#707070',
              }}>Learn more about Vaccines</Text>
            </View>

            <View style={{
              width: "20%"
            }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('About_Vaccine')}>
                <Text style={{
                  fontSize: scale(13),
                  fontFamily: "Roboto-Regular",
                  color: '#707070',
                }}>View All</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ width: scale(322), }}>

            <FlatList
              data={Object.keys(this.state.vaccineDetails[`0`])}
              extraData={this.state}
              enableEmptySections={true}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              keyExtractor={(item, index) => index.toString()}
              horizontal={true}
              renderItem={({ item, index }) => (
                <View>
                  {this.props.nextVaccine[`0`].vaccine1 === item ? <View style={{ marginTop: verticalScale(15), width: scale(165), height: scale(170), borderRadius: 8, backgroundColor: "#FFFFFF", alignItems: "center", justifyContent: "center", marginLeft: index === 0 ? null : scale(10) }}>
                    <View style={{ flexDirection: "row", marginTop: verticalScale(10), alignItems: "center", justifyContent: "center" }}>
                      <Text style={{
                        fontSize: scale(13),
                        fontFamily: "Roboto-Regular",
                        color: '#000000',
                        width: "70%"
                      }}>What is {item}</Text>

                      <View style={{
                        width: "20%",
                        backgroundColor: "transparent",
                        borderRadius: 400
                      }}>
                        <LinearGradient
                          start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                          colors={['#a076e8', '#5dc4dd']}
                          style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                          <Image
                            style={{
                              width: scale(16.08),
                              height: scale(16.08),
                            }}
                            source={require('../../assets/injection2.png')}
                          />
                        </LinearGradient>
                      </View>
                    </View>

                    <Text style={{
                      width: "90%", fontSize: scale(11), marginTop: verticalScale(23),
                      fontFamily: "Roboto-Regular",
                      color: '#707070',
                    }} numberOfLines={3}>{this.state.vaccineDetails[`0`][item].Description}</Text>

                    <TouchableOpacity style={{ marginTop: verticalScale(11), width: scale(104), height: scale(34), borderColor: "#A173E8", borderRadius: 8, borderWidth: 1, backgroundColor: "transparent", justifyContent: "center", alignItems: "center" }}
                      onPress={() => this.props.navigation.navigate('About_Vaccine')}>
                      <Text style={{
                        fontSize: scale(15),
                        fontFamily: "Roboto-Regular",
                        color: '#000000',
                      }}>Read more</Text>
                    </TouchableOpacity>
                  </View> : this.props.nextVaccine[`0`].vaccine2 === item ? <View style={{ marginTop: verticalScale(15), width: scale(165), height: scale(170), borderRadius: 8, backgroundColor: "#FFFFFF", alignItems: "center", justifyContent: "center", marginLeft: index === 0 ? null : scale(10) }}>
                    <View style={{ flexDirection: "row", marginTop: verticalScale(10), alignItems: "center", justifyContent: "center" }}>
                      <Text style={{
                        fontSize: scale(13),
                        fontFamily: "Roboto-Regular",
                        color: '#000000',
                        width: "70%"
                      }}>What is {item}</Text>

                      <View style={{
                        width: "20%",
                        backgroundColor: "transparent",
                        borderRadius: 400
                      }}>
                        <LinearGradient
                          start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                          colors={['#a076e8', '#5dc4dd']}
                          style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                          <Image
                            style={{
                              width: scale(16.08),
                              height: scale(16.08),
                            }}
                            source={require('../../assets/injection2.png')}
                          />
                        </LinearGradient>
                      </View>
                    </View>

                    <Text style={{
                      width: "90%", fontSize: scale(11), marginTop: verticalScale(23),
                      fontFamily: "Roboto-Regular",
                      color: '#707070',
                    }} numberOfLines={3}>{this.state.vaccineDetails[`0`][item].Description}</Text>

                    <TouchableOpacity style={{ marginTop: verticalScale(11), width: scale(104), height: scale(34), borderColor: "#A173E8", borderRadius: 8, borderWidth: 1, backgroundColor: "transparent", justifyContent: "center", alignItems: "center" }}
                      onPress={() => this.props.navigation.navigate('About_Vaccine')}>
                      <Text style={{
                        fontSize: scale(15),
                        fontFamily: "Roboto-Regular",
                        color: '#000000',
                      }}>Read more</Text>
                    </TouchableOpacity>
                  </View> : this.props.nextVaccine[`0`].vaccine3 === item ? <View style={{ marginTop: verticalScale(15), width: scale(165), height: scale(170), borderRadius: 8, backgroundColor: "#FFFFFF", alignItems: "center", justifyContent: "center", marginLeft: index === 0 ? null : scale(10) }}>
                    <View style={{ flexDirection: "row", marginTop: verticalScale(10), alignItems: "center", justifyContent: "center" }}>
                      <Text style={{
                        fontSize: scale(13),
                        fontFamily: "Roboto-Regular",
                        color: '#000000',
                        width: "70%"
                      }}>What is {item}</Text>

                      <View style={{
                        width: "20%",
                        backgroundColor: "transparent",
                        borderRadius: 400
                      }}>
                        <LinearGradient
                          start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                          colors={['#a076e8', '#5dc4dd']}
                          style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                          <Image
                            style={{
                              width: scale(16.08),
                              height: scale(16.08),
                            }}
                            source={require('../../assets/injection2.png')}
                          />
                        </LinearGradient>
                      </View>
                    </View>

                    <Text style={{
                      width: "90%", fontSize: scale(11), marginTop: verticalScale(23),
                      fontFamily: "Roboto-Regular",
                      color: '#707070',
                    }} numberOfLines={3}>{this.state.vaccineDetails[`0`][item].Description}</Text>

                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('About_Vaccine')}
                      style={{ marginTop: verticalScale(11), width: scale(104), height: scale(34), borderColor: "#A173E8", borderRadius: 8, borderWidth: 1, backgroundColor: "transparent", justifyContent: "center", alignItems: "center" }}>
                      <Text style={{
                        fontSize: scale(15),
                        fontFamily: "Roboto-Regular",
                        color: '#000000',
                      }}>Read more</Text>
                    </TouchableOpacity>
                  </View> : this.props.nextVaccine[`0`].vaccine4 === item ? <View style={{ marginTop: verticalScale(15), width: scale(165), height: scale(170), borderRadius: 8, backgroundColor: "#FFFFFF", alignItems: "center", justifyContent: "center", marginLeft: index === 0 ? null : scale(10) }}>
                    <View style={{ flexDirection: "row", marginTop: verticalScale(10), alignItems: "center", justifyContent: "center" }}>
                      <Text style={{
                        fontSize: scale(13),
                        fontFamily: "Roboto-Regular",
                        color: '#000000',
                        width: "70%"
                      }}>What is {item}</Text>

                      <View style={{
                        width: "20%",
                        backgroundColor: "transparent",
                        borderRadius: 400
                      }}>
                        <LinearGradient
                          start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                          colors={['#a076e8', '#5dc4dd']}
                          style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                          <Image
                            style={{
                              width: scale(16.08),
                              height: scale(16.08),
                            }}
                            source={require('../../assets/injection2.png')}
                          />
                        </LinearGradient>
                      </View>
                    </View>

                    <Text style={{
                      width: "90%", fontSize: scale(11), marginTop: verticalScale(23),
                      fontFamily: "Roboto-Regular",
                      color: '#707070',
                    }} numberOfLines={3}>{this.state.vaccineDetails[`0`][item].Description}</Text>

                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('About_Vaccine')}
                      style={{ marginTop: verticalScale(11), width: scale(104), height: scale(34), borderColor: "#A173E8", borderRadius: 8, borderWidth: 1, backgroundColor: "transparent", justifyContent: "center", alignItems: "center" }}>
                      <Text style={{
                        fontSize: scale(15),
                        fontFamily: "Roboto-Regular",
                        color: '#000000',
                      }}>Read more</Text>
                    </TouchableOpacity>
                  </View> : this.props.nextVaccine[`0`].vaccine5 === item ? <View style={{ marginTop: verticalScale(15), width: scale(165), height: scale(170), borderRadius: 8, backgroundColor: "#FFFFFF", alignItems: "center", justifyContent: "center", marginLeft: index === 0 ? null : scale(10) }}>
                    <View style={{ flexDirection: "row", marginTop: verticalScale(10), alignItems: "center", justifyContent: "center" }}>
                      <Text style={{
                        fontSize: scale(13),
                        fontFamily: "Roboto-Regular",
                        color: '#000000',
                        width: "70%"
                      }}>What is {item}</Text>

                      <View style={{
                        width: "20%",
                        backgroundColor: "transparent",
                        borderRadius: 400
                      }}>
                        <LinearGradient
                          start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                          colors={['#a076e8', '#5dc4dd']}
                          style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                          <Image
                            style={{
                              width: scale(16.08),
                              height: scale(16.08),
                            }}
                            source={require('../../assets/injection2.png')}
                          />
                        </LinearGradient>
                      </View>
                    </View>

                    <Text style={{
                      width: "90%", fontSize: scale(11), marginTop: verticalScale(23),
                      fontFamily: "Roboto-Regular",
                      color: '#707070',
                    }} numberOfLines={3}>{this.state.vaccineDetails[`0`][item].Description}</Text>

                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('About_Vaccine')}
                      style={{ marginTop: verticalScale(11), width: scale(104), height: scale(34), borderColor: "#A173E8", borderRadius: 8, borderWidth: 1, backgroundColor: "transparent", justifyContent: "center", alignItems: "center" }}>
                      <Text style={{
                        fontSize: scale(15),
                        fontFamily: "Roboto-Regular",
                        color: '#000000',
                      }}>Read more</Text>
                    </TouchableOpacity>
                  </View> : this.props.nextVaccine[`0`].vaccine6 === item ? <View style={{ marginTop: verticalScale(15), width: scale(165), height: scale(170), borderRadius: 8, backgroundColor: "#FFFFFF", alignItems: "center", justifyContent: "center", marginLeft: index === 0 ? null : scale(10) }}>
                    <View style={{ flexDirection: "row", marginTop: verticalScale(10), alignItems: "center", justifyContent: "center" }}>
                      <Text style={{
                        fontSize: scale(13),
                        fontFamily: "Roboto-Regular",
                        color: '#000000',
                        width: "70%"
                      }}>What is {item}</Text>

                      <View style={{
                        width: "20%",
                        backgroundColor: "transparent",
                        borderRadius: 400
                      }}>
                        <LinearGradient
                          start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                          colors={['#a076e8', '#5dc4dd']}
                          style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                          <Image
                            style={{
                              width: scale(16.08),
                              height: scale(16.08),
                            }}
                            source={require('../../assets/injection2.png')}
                          />
                        </LinearGradient>
                      </View>
                    </View>

                    <Text style={{
                      width: "90%", fontSize: scale(11), marginTop: verticalScale(23),
                      fontFamily: "Roboto-Regular",
                      color: '#707070',
                    }} numberOfLines={3}>{this.state.vaccineDetails[`0`][item].Description}</Text>

                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('About_Vaccine')}
                      style={{ marginTop: verticalScale(11), width: scale(104), height: scale(34), borderColor: "#A173E8", borderRadius: 8, borderWidth: 1, backgroundColor: "transparent", justifyContent: "center", alignItems: "center" }}>
                      <Text style={{
                        fontSize: scale(15),
                        fontFamily: "Roboto-Regular",
                        color: '#000000',
                      }}>Read more</Text>
                    </TouchableOpacity>
                  </View> : null}

                </View>
              )}

            />
          </View>

          <View style={{ height: scale(20) }} />

        </ScrollView>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f4f7f9',
  },
  innerCircle: {
    borderRadius: 400
  },
  vaccinestext: {
    fontSize: scale(15),
    color: '#707070',
    marginLeft: scale(10),
    marginTop: verticalScale(7),
  },
  card: {
    height: scale(234),
    width: scale(322),
    backgroundColor: '#ffffff',
    borderRadius: 10,
    marginTop: verticalScale(15),
  },
  cardvaccine: {
    height: scale(151),
    width: scale(200),
    backgroundColor: 'transparent',
    borderRadius: 10,
    marginLeft: scale(15),
    marginTop: verticalScale(20),
  },
  input: {
    margin: scale(15),
    height: scale(50),
    width: scale(300),
    padding: scale(5),
    fontSize: scale(16),
    borderBottomWidth: 1,
    borderBottomColor: 'black',
  },
  digitalreport: {
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    alignItems: 'center',
    width: scale(322),
    marginTop: verticalScale(25),
    height: scale(69),
    borderRadius: 10,
  },
  linearGradient: {
    borderRadius: 10,
    justifyContent: 'center',
    marginTop: verticalScale(10),
    width: scale(125),
    alignItems: 'center',
    height: scale(40),
  },
  btnSignin: {
    justifyContent: 'center',
    backgroundColor: '#fff',
    alignItems: 'center',

    borderRadius: 10,
    width: scale(125),
    marginTop: verticalScale(10),
    height: scale(40),
  }
})

const calculateAge = (dob) => {
  var dateString = dob
  var now = new Date();
  var today = new Date(now.getYear(), now.getMonth(), now.getDate());
  var yearNow = now.getYear();
  var monthNow = now.getMonth();
  var dateNow = now.getDate();

  var dateParts = dateString.split("/");
  var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);

  var yearDob = dateObject.getYear();
  var monthDob = dateObject.getMonth();
  var dateDob = dateObject.getDate();
  var age = {};
  var ageString = "";
  var yearString = "";
  var monthString = "";
  var dayString = "";

  yearAge = yearNow - yearDob;

  if (monthNow >= monthDob)
    var monthAge = monthNow - monthDob;
  else {
    yearAge--;
    var monthAge = 12 + monthNow - monthDob;
  }

  if (dateNow >= dateDob)
    var dateAge = dateNow - dateDob;
  else {
    monthAge--;
    var dateAge = 31 + dateNow - dateDob;

    if (monthAge < 0) {
      monthAge = 11;
      yearAge--;
    }
  }

  age = {
    years: yearAge,
    months: monthAge,
    days: dateAge
  };

  if (age.years > 1) yearString = " Years";
  else yearString = " Year";
  if (age.months > 1) monthString = " Months";
  else monthString = " Month";
  if (age.days > 1) dayString = " Days";
  else dayString = " Day";

  if ((age.years > 0) && (age.months > 0) && (age.days > 0))
    ageString = age.years + yearString + ", " + age.months + monthString + " and " + age.days + dayString + " old.";
  else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
    ageString = "Only " + age.days + dayString + " old!";
  else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
    ageString = age.years + yearString + " old. Happy Birthday!!";
  else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
    ageString = age.years + yearString + " and " + age.months + monthString + " old.";
  else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
    ageString = age.months + monthString + " and " + age.days + dayString + " old.";
  else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
    ageString = age.years + yearString + " and " + age.days + dayString + " old.";
  else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
    ageString = age.months + monthString + " old.";
  else ageString = "Oops! Could not calculate age!";

  return ageString;
}

const calculateVaccines = (childVaccines) => {

  const res = childVaccines;

  let data = [];
  for (let key in res.childVaccines) {
    const vaccineObject = {};
    var com = 0;
    res.childVaccines[key].vaccines.forEach((vaccine, index) => {
      vaccineObject[`vaccine${index + 1}`] = vaccine.name;
      if (vaccine.givenDate !== "") {
        com = com + 1;
      }
      vaccineObject[`complete_count`] = com;
    });
    vaccineObject['date'] = res.childVaccines[key].dueDate;
    vaccineObject['name'] = res.child.name;
    vaccineObject['duration'] = key;
    vaccineObject[`count`] = res.childVaccines[key].vaccines.length;

    var dateString1 = res.childVaccines[key].dueDate;
    var dateParts1 = dateString1.split("/");
    var dateObject1 = new Date(+dateParts1[2], dateParts1[1] - 1, +dateParts1[0]);
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    vaccineObject['day'] = weekday[dateObject1.getDay()]
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    vaccineObject['month'] = month[dateObject1.getMonth()]
    vaccineObject['datenum'] = dateObject1.getDate()
    vaccineObject['year'] = dateObject1.getUTCFullYear()

    data.push(vaccineObject);
  }

  var give = 0;
  for (var i = 0; i < data.length; i++) {
    if (data[i].count === data[i].complete_count) {
      give = give + 1;
    }
  }
  return give;
}

const calculatePerc = (childVaccines) => {

  const res = childVaccines;

  let data = [];
  for (let key in res.childVaccines) {
    const vaccineObject = {};
    var com = 0;
    res.childVaccines[key].vaccines.forEach((vaccine, index) => {
      vaccineObject[`vaccine${index + 1}`] = vaccine.name;
      if (vaccine.givenDate !== "") {
        com = com + 1;
      }
      vaccineObject[`complete_count`] = com;
    });
    vaccineObject['date'] = res.childVaccines[key].dueDate;
    vaccineObject['name'] = res.child.name;
    vaccineObject['duration'] = key;
    vaccineObject[`count`] = res.childVaccines[key].vaccines.length;

    var dateString1 = res.childVaccines[key].dueDate;
    var dateParts1 = dateString1.split("/");
    var dateObject1 = new Date(+dateParts1[2], dateParts1[1] - 1, +dateParts1[0]);
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    vaccineObject['day'] = weekday[dateObject1.getDay()]
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    vaccineObject['month'] = month[dateObject1.getMonth()]
    vaccineObject['datenum'] = dateObject1.getDate()
    vaccineObject['year'] = dateObject1.getUTCFullYear()

    data.push(vaccineObject);
  }

  var give = 0;
  for (var i = 0; i < data.length; i++) {
    if (data[i].count === data[i].complete_count) {
      give = give + 1;
    }
  }
  var pos = 14;
  var perc = ((give / pos) * 100).toFixed(2);
  return perc;
}

const calculateNextVaccine = (childVaccines) => {

  const res = childVaccines;

  let data = [];
  for (let key in res.childVaccines) {
    const vaccineObject = {};
    var com = 0;
    res.childVaccines[key].vaccines.forEach((vaccine, index) => {
      vaccineObject[`vaccine${index + 1}`] = vaccine.name;
      if (vaccine.givenDate !== "") {
        com = com + 1;
      }
      vaccineObject[`complete_count`] = com;
    });
    vaccineObject['date'] = res.childVaccines[key].dueDate;
    vaccineObject['name'] = res.child.name;
    vaccineObject['duration'] = key;
    vaccineObject[`count`] = res.childVaccines[key].vaccines.length;

    var dateString1 = res.childVaccines[key].dueDate;
    var dateParts1 = dateString1.split("/");
    var dateObject1 = new Date(+dateParts1[2], dateParts1[1] - 1, +dateParts1[0]);
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    vaccineObject['day'] = weekday[dateObject1.getDay()]
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    vaccineObject['month'] = month[dateObject1.getMonth()]
    vaccineObject['datenum'] = dateObject1.getDate()
    vaccineObject['year'] = dateObject1.getUTCFullYear()

    data.push(vaccineObject);
  }

  let final = [];
  for (var j = 0; j < data.length; j++) {
    var dateString = data[j].date
    var dateParts = dateString.split("/");
    var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    var g1 = new Date();
    var g2 = new Date(dateObject);
    if (g1.getTime() < g2.getTime()) {
      final.push(data[j])
    }
  }

  return final;
}
function mapStateToProps(state) {
  let selectedChild = state.userinfo.selectedChild
  return {
    //child:state.userinfo.child[`0`],
    user: state.userinfo.user[selectedChild],
    ageInWeek: calculateAge(state.userinfo.user[selectedChild].child.dob),
    completeVaccines: calculateVaccines(state.userinfo.user[selectedChild]),
    completePerc: calculatePerc(state.userinfo.user[selectedChild]),
    nextVaccine: calculateNextVaccine(state.userinfo.user[selectedChild]),
    loder: state.loder,

  }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes, Milestone })(Home)




// <View style={{ marginTop: verticalScale(15), width: scale(165), height: scale(170), borderRadius: 8, backgroundColor: "#FFFFFF", alignItems: "center", justifyContent: "center",marginLeft: index === 0 ? null:scale(10) }}>
            //   <View style={{ flexDirection: "row", marginTop: verticalScale(10), alignItems: "center", justifyContent: "center" }}>
            //     <Text style={{
            //       fontSize: scale(16),
            //       fontFamily: "Roboto-Regular",
            //       color: '#000000',
            //       width: "70%"
            //     }}>{item}</Text>

            //     <View style={{
            //       width: "20%",
            //       backgroundColor: "transparent",
            //       borderRadius: 400
            //     }}>
            //       <LinearGradient
            //         start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
            //         colors={['#a076e8', '#5dc4dd']}
            //         style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
            //         <Image
            //           style={{
            //             width: scale(16.08),
            //             height: scale(16.08),
            //           }}
            //           source={require('../../assets/injection2.png')}
            //         />
            //       </LinearGradient>
            //     </View>
            //   </View>

            //   <Text style={{
            //     width: "90%", fontSize: scale(11), marginTop: verticalScale(23),
            //     fontFamily: "Roboto-Regular",
            //     color: '#707070',
            //   }} numberOfLines={4}>DPT (also DTP and DTwP) is a class of combination vaccines against three infectious diseases in humans: diphther</Text>

            //   <TouchableOpacity style={{ marginTop: verticalScale(11), width: scale(104), height: scale(34), borderColor: "#A173E8", borderRadius: 8, borderWidth: 1, backgroundColor: "transparent", justifyContent: "center", alignItems: "center" }}>
            //     <Text style={{
            //       fontSize: scale(15),
            //       fontFamily: "Roboto-Regular",
            //       color: '#000000',
            //     }}>Read more</Text>
            //   </TouchableOpacity>
            // </View>

             //   refreshControl={
        //     <RefreshControl
        //       //refresh control used for the Pull to Refresh
        //       refreshing={this.state.isLoading}
        //       onRefresh={this.onRefresh.bind(this)}
        //       colors={['#a076e8', '#5dc4dd']}
        //     />
        //   }