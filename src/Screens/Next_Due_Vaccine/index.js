import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ScrollView,
    ImageBackground,
    Image,
    FlatList,
    HeaderBackButton,
    TouchableOpacity,
    Alert,
    Dimensions,
    StatusBar,
    LayoutAnimation,
    TouchableHighlight,
    Platform,
    UIManager,
    ActivityIndicator,
    BackHandler,
} from 'react-native';
import { connect } from 'react-redux';
import CheckField from "../../Components/CheckField";
import Picker from 'react-native-picker';
import { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes, Milestone, Issue_Vaccine } from '../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import Header from '../../Components/Header';
import HamburgerIcon from "../../Components/HamburgerIcon"
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Modal from 'react-native-modal';
import { scale, moderateScale, verticalScale } from '../../Components/Scale';
import DatePicker from 'react-native-datepicker';
import Spinner from "react-native-spinkit";

class Next_Due_Vaccine extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: <HamburgerIcon />,
        title: ' Next Due Vaccine ',
        headerBackground: <Header />,

        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: scale(20),
            fontFamily: "Roboto-Bold",
            fontWeight: "bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        },
    });

    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            data: null,

            date: null,
            height: '',
            weight: '',
            brand1: '',
            mfgdate1: '',
            expdate1: '',
            batch1: '',
            hospital1: '',
            brand2: '',
            mfgdate2: '',
            expdate2: '',
            batch2: '',
            hospital2: '',
            brand3: '',
            mfgdate3: '',
            expdate3: '',
            batch3: '',
            hospital3: '',
            duration: '',

            dataSource: [],
            isLoading: true,

            expanded: false,
            status: true,
            expanded1: false,
            status1: true,
            status2: true,

            checked: false,
            isModalVisible: false,

            index: 0,
            routes: [
                { key: 'about', title: 'About' },
                { key: 'doses', title: 'Doses' },
                { key: 'sideeffects', title: 'Side effects' },
            ],
            enableSave: false,

            selectedVaccine: [],
            activSelectedVaccineIndex: 0,
            nextVaccine: [],
        };
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    async componentDidMount() {
        this.setState({
            nextVaccine: await this.props.nextVaccine,
            duration: await this.props.nextVaccine[`0`].duration,
            isLoading: false,
        })
        console.log('this', this.state.nextVaccine, this.state.duration);
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Vaccine');
        return true;
    }

    _createDateData() {
        let date = [];
        for (let i = 2016; i < 2050; i++) {
            let month = [];
            for (let j = 1; j < 13; j++) {
                let _month = {};
                _month[j] = j;
                month.push(j);
            }
            let _date = {};
            _date[i] = month;
            date.push(_date);
        }
        return date;
    }

    changeLayout = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        for (var i = 0; i < this.state.nextVaccine[`0`].vaccines.length; i++) {
            if (this.state.nextVaccine[`0`].vaccines[i].status === true) {

                this.setState({ expanded: !this.state.expanded, })
                return
            }
        }
        alert('select vaccine')
    }

    changeLayout1 = () => {

        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (this.state.nextVaccine[`0`].givenDate && this.state.nextVaccine[`0`].height && this.state.nextVaccine[`0`].weight) {
            for (var i = 0; i < this.state.selectedVaccine.length; i++) {
                this.state.selectedVaccine[i].givenDate = this.state.nextVaccine[`0`].givenDate
            }
            this.setState({ expanded1: true, status: false, });
            return
        }
        this.setState({ isModalVisible: true })
        //alert('fill all the details')
    };

    backHandler = () => {
        if (this.state.activSelectedVaccineIndex === 0) {
            this.setState({ expanded1: false, status: true, });
        } else {
            this.setState({
                activSelectedVaccineIndex: this.state.activSelectedVaccineIndex - 1,
            })
        }
    }

    nextHandler = () => {
        if (this.state.selectedVaccine[this.state.activSelectedVaccineIndex].brand && this.state.selectedVaccine[this.state.activSelectedVaccineIndex].mfgDate && this.state.selectedVaccine[this.state.activSelectedVaccineIndex].expDate && this.state.selectedVaccine[this.state.activSelectedVaccineIndex].batchNumber && this.state.selectedVaccine[this.state.activSelectedVaccineIndex].hospital) {
            this.setState({
                activSelectedVaccineIndex: this.state.activSelectedVaccineIndex + 1,
            })
            return
        }
        this.setState({ isModalVisible: true })
        //alert('fill all the details')
    }

    saveHandler = async () => {
        const { duration } = this.state
        if (this.state.selectedVaccine[this.state.activSelectedVaccineIndex].brand && this.state.selectedVaccine[this.state.activSelectedVaccineIndex].mfgDate && this.state.selectedVaccine[this.state.activSelectedVaccineIndex].expDate && this.state.selectedVaccine[this.state.activSelectedVaccineIndex].batchNumber && this.state.selectedVaccine[this.state.activSelectedVaccineIndex].hospital) {
            const dataSourceClone = JSON.parse(JSON.stringify(this.state.nextVaccine[`0`]));
            for (var i = 0; i < dataSourceClone.vaccines.length; i++) {
                for (var j = 0; j < this.state.selectedVaccine.length; j++) {
                    if (dataSourceClone.vaccines[i].name === this.state.selectedVaccine[j].name) {
                        dataSourceClone.vaccines[i].brand = this.state.selectedVaccine[j].brand
                        dataSourceClone.vaccines[i].mfgDate = this.state.selectedVaccine[j].mfgDate
                        dataSourceClone.vaccines[i].expDate = this.state.selectedVaccine[j].expDate
                        dataSourceClone.vaccines[i].batchNumber = this.state.selectedVaccine[j].batchNumber
                        dataSourceClone.vaccines[i].hospital = this.state.selectedVaccine[j].hospital
                        dataSourceClone.vaccines[i].givenDate = this.state.selectedVaccine[j].givenDate
                    }
                }
            }
            console.log('da', dataSourceClone);
            const childVaccine = { [duration]: dataSourceClone };
            console.log('da', childVaccine);
            const res = await this.props.Issue_Vaccine(this.props.selectedChild, childVaccine)
            if (res.status === 200) {
                alert('Saved successfully');
                this.props.navigation.navigate('Vaccine');
            } else {
                alert('Something is wrong');
            }
            return
        }
        this.setState({ isModalVisible: true })
        //alert('fill all the details')

    }

    onCheckboxClick = (item, vaccinename) => {
        console.log('onCheckboxClick', item, vaccinename);
        const dataSourceClone = JSON.parse(JSON.stringify(this.state.nextVaccine));
        let clone = this.state.selectedVaccine
        for (var i = 0; i < dataSourceClone.length; i++) {
            if (dataSourceClone[i].duration === item) {
                for (var j = 0; j < dataSourceClone[i].vaccines.length; j++) {
                    if (vaccinename.name === dataSourceClone[i].vaccines[j].name) {
                        if (!dataSourceClone[i].vaccines[j].status) {
                            this.setState({
                                selectedVaccine: [...this.state.selectedVaccine, { ...dataSourceClone[i].vaccines[j] }]
                            })
                        }
                        else {
                            this.setState({
                                selectedVaccine: this.state.selectedVaccine.filter(elem => elem.name !== dataSourceClone[i].vaccines[j].name)
                            })
                        }
                        dataSourceClone[i].vaccines[j].status = !dataSourceClone[i].vaccines[j].status
                    }

                }
            }
        }
        console.log('dataSourceClone', dataSourceClone);

        this.setState({
            nextVaccine: dataSourceClone
        })
    }

    _showMfgDatePicker(index) {
        Picker.init({
            pickerData: this._createDateData(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Selet Manufacturing Date",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                console.log(date);
                const dataSourceClone = this.state.selectedVaccine;
                dataSourceClone[index].mfgDate = date;
                this.setState({
                    selectedVaccine: dataSourceClone
                })

            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);

            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                console.log(date);
                const dataSourceClone = this.state.selectedVaccine;
                dataSourceClone[index].mfgDate = date;
                this.setState({
                    selectedVaccine: dataSourceClone
                })
            }
        });
        Picker.show();
    }

    _showExpDatePicker(index) {
        Picker.init({
            pickerData: this._createDateData(),
            pickerFontColor: [0, 0, 0, 1],
            pickerTitleText: "Selet Expiry Date",
            onPickerConfirm: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                const dataSourceClone = this.state.selectedVaccine;
                dataSourceClone[index].expDate = date;
                this.setState({
                    selectedVaccine: dataSourceClone
                })
            },
            onPickerCancel: (pickedValue, pickedIndex) => {
                console.log('date', pickedValue, pickedIndex);

            },
            onPickerSelect: (pickedValue, pickedIndex) => {
                var date = pickedValue[1] + "/" + pickedValue[0]
                const dataSourceClone = this.state.selectedVaccine;
                dataSourceClone[index].expDate = date;
                this.setState({
                    selectedVaccine: dataSourceClone
                })
            }
        });
        Picker.show();
    }

    changeSelectedVaccineElement = (name, value, index) => {
        let reg = /^[A-Za-z0-9]*$/;
        if (name === "brand") {
            const dataSourceClone = this.state.selectedVaccine;
            dataSourceClone[this.state.activSelectedVaccineIndex].brand = value;
            console.log("dataSourceClone", dataSourceClone);
            this.setState({
                selectedVaccine: dataSourceClone
            })
        } else if (name === "batchNumber") {
            if (reg.test(value)) {
            const dataSourceClone = this.state.selectedVaccine;
            dataSourceClone[this.state.activSelectedVaccineIndex].batchNumber = value;
            console.log("dataSourceClone", dataSourceClone);
            this.setState({
                selectedVaccine: dataSourceClone
            })
        }
            else {
                alert("Only letters or numbers accepted")
            }
        } else if (name === "hospital") {
            const dataSourceClone = this.state.selectedVaccine;
            dataSourceClone[this.state.activSelectedVaccineIndex].hospital = value;
            console.log("dataSourceClone", dataSourceClone);
            this.setState({
                selectedVaccine: dataSourceClone
            })
        }
    }

    changeinput = (name, value) => {
        let reg = /^[0-9]*$/;
        if (name === "date") {
            const dataSourceClone = JSON.parse(JSON.stringify(this.state.nextVaccine));
            dataSourceClone[`0`].givenDate = value;
            console.log("dataSourceClone", dataSourceClone);
            this.setState({
                nextVaccine: dataSourceClone
            })
        } else if (name === "height") {
            if (reg.test(value)) {
                const dataSourceClone = JSON.parse(JSON.stringify(this.state.nextVaccine));
                dataSourceClone[`0`].height = value;
                console.log("dataSourceClone", dataSourceClone);
                this.setState({
                    nextVaccine: dataSourceClone
                })
            } else {
                alert("Only Numeric Values are accetped")
            }
        } else if (name === "weight") {
            if (reg.test(value)) {
                const dataSourceClone = JSON.parse(JSON.stringify(this.state.nextVaccine));
                dataSourceClone[`0`].weight = value;
                console.log("dataSourceClone", dataSourceClone);
                this.setState({
                    nextVaccine: dataSourceClone
                })
            } else {
                alert("Only Numeric Values are accetped")
            }
        }
    }

    cancel() {
        this.setState({ isModalVisible: false })
    }

    render() {
        console.log("selectedVaccine", this.state.selectedVaccine, this.state.activSelectedVaccineIndex);

        if (this.state.isLoading) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    {/* <ActivityIndicator /> */}
                    <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }} />
                </View>
            );
        } else if (this.props.loder) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    {/* <ActivityIndicator /> */}
                    <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center', }} />
                </View>
            );
        }
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: '#f4f7f9', }}>
                <CheckField isModalVisible={this.state.isModalVisible}
                    onCancel={() => this.cancel()}
                    cancel_label="Continue"
                    text="Please fill all the details to Proceed"
                    image={require('../../assets/popup.png')} />
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}>

                    <View style={{ flexDirection: "row" }}>

                        <View style={{ flex: 1 }}>
                            <ScrollView
                                showsHorizontalScrollIndicator={false}
                                showsVerticalScrollIndicator={false}
                                nestedScrollEnabled={true}>
                                <LinearGradient
                                    colors={['#a076e8', '#5dc4dd']}
                                    style={{ alignItems: 'center', flex: 1, width: (322), marginTop: verticalScale(20), marginLeft: scale(22), borderTopRightRadius: 30, borderTopLeftRadius: 30, borderBottomLeftRadius: 30, borderBottomRightRadius: 30 }}
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}>
                                    <View style={{ flexDirection: "row", marginTop: verticalScale(15), marginLeft: scale(15), }}>
                                        <View style={{ width: "40%" }}>
                                            <View style={{ width: scale(86), height: scale(86), borderRadius: 400, }}>
                                                <Image
                                                    style={{ width: scale(86), height: scale(86), borderRadius: 400 }}
                                                    source={{ uri: this.props.ChildDetails.imageUrl }}
                                                    resizeMode='cover'
                                                />
                                            </View>
                                        </View>

                                        <View style={{ flexDirection: "column", width: "60%", }}>
                                            <View>
                                                <Text style={{ fontSize: scale(14), fontWeight: "bold", color: "#FFFFFF", }}>{this.state.nextVaccine[`0`].due_date}</Text>
                                            </View>
                                            <View style={{ marginTop: verticalScale(20) }}>
                                                <Text style={{ fontSize: scale(15), color: "#FFFFFF", fontWeight: "bold" }}>{this.props.ChildDetails.child.name}</Text>
                                            </View>
                                            <View style={{ marginTop: verticalScale(3) }}>
                                                <Text style={{ fontSize: scale(11), color: "#FFFFFF" }}>{this.state.nextVaccine[`0`].duration}</Text>
                                            </View>
                                        </View>

                                    </View>
                                    <View style={{ height: this.state.expanded ? scale(250) : scale(300), }}>
                                        {this.state.nextVaccine[`0`].vaccines.map((vaccinename, index2) => (
                                            <View style={{ flexDirection: "row", marginLeft: scale(15), marginRight: scale(15), marginTop: index2 === 0 ? verticalScale(7) : verticalScale(7), justifyContent: 'center', alignItems: 'center', }}>
                                                <View style={{ width: "80%" }}>
                                                    <View style={{ flexDirection: "row", }}>
                                                        <Image
                                                            style={{ width: scale(17), height: scale(17), alignSelf: "center" }}
                                                            source={require('../../assets/injectionwhite.png')}
                                                            resizeMode='contain'
                                                        />
                                                        <Text style={styles.vaccinestext}>{vaccinename.name}</Text>
                                                        <Image
                                                            style={{ width: scale(11), height: scale(11), alignSelf: "center", marginLeft: scale(7) }}
                                                            source={require('../../assets/info.png')}
                                                            resizeMode='contain'
                                                        />
                                                    </View>
                                                </View>
                                                <View style={{ width: "20%", justifyContent: 'center', alignItems: 'center' }}>
                                                    <TouchableHighlight
                                                        disabled={this.props.userSave[`0`].vaccines[index2].status}
                                                        onPress={() => this.onCheckboxClick(this.state.nextVaccine[`0`].duration, vaccinename)}
                                                        underlayColor="transparent"
                                                        style={{ marginVertical: verticalScale(10) }}>
                                                        <View style={{ alignItems: 'center' }}>
                                                            {vaccinename.status ? (
                                                                <Image
                                                                    source={require('../../assets/checkboxselected.png')}
                                                                    style={{
                                                                        width: scale(14),
                                                                        height: scale(14),
                                                                        borderRadius: 400,
                                                                    }}
                                                                    resizeMode="contain"
                                                                />
                                                            ) : (
                                                                    <View
                                                                        style={{
                                                                            width: scale(14),
                                                                            height: scale(14),
                                                                            borderRadius: 400,
                                                                            backgroundColor: '#FFFFFF',
                                                                        }}
                                                                    />
                                                                )}
                                                        </View>
                                                    </TouchableHighlight>

                                                </View>
                                            </View>
                                        ))}
                                    </View>
                                    {!this.state.expanded ?
                                        <View style={{ marginTop: 0 }}>
                                            <TouchableOpacity
                                                style={{
                                                    justifyContent: 'space-between',
                                                    backgroundColor: '#fff',
                                                    flexDirection: 'row',
                                                    borderRadius: 15,
                                                    width: scale(222),
                                                    height: scale(40),
                                                }}
                                                onPress={() => this.changeLayout()}
                                            // disabled={index === 0 ? false : true}
                                            >
                                                <Text style={{ marginLeft: scale(20), alignSelf: 'center' }}>
                                                    Vaccine given on
                                  </Text>
                                                <Image
                                                    style={{
                                                        width: scale(20),
                                                        height: scale(20),
                                                        alignSelf: 'center',
                                                        marginRight: scale(20),
                                                    }}
                                                    source={require('../../assets/calandergrey.png')}
                                                    resizeMode="cover"
                                                />
                                            </TouchableOpacity>
                                            <View style={{ height: scale(10) }} />
                                        </View>
                                        : null}

                                    <View style={{ height: this.state.expanded ? null : 0, overflow: 'hidden', alignItems: 'center', }}>
                                        <DatePicker
                                            style={{
                                                height: scale(50),
                                                width: scale(222),
                                                justifyContent: 'center',
                                                marginTop: verticalScale(20),
                                                backgroundColor: '#FFFFFF',
                                                borderRadius: 10,

                                            }}

                                            date={this.state.nextVaccine[`0`].givenDate} //initial date from state
                                            mode="date" //The enum of date, datetime and time
                                            //placeholder={this.state.nextVaccine[`0`].dueDate}
                                            placeholder="Select Date"
                                            placeHolderTextStyle={{ color: '#707070', fontSize: scale(15) }}
                                            format="DD/MM/YYYY"
                                            //minDate={this.state.dataSource.child.dob}
                                            minDate={this.state.nextVaccine[`0`].dueDate}
                                            maxDate="01/01/2100"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            iconComponent={
                                                <Image
                                                    style={{
                                                        width: 20,
                                                        height: 20,
                                                        marginRight: 20,
                                                        alignSelf: 'center',
                                                    }}
                                                    source={require('../../assets/calandergrey.png')}
                                                    resizeMode="cover"
                                                />
                                            }
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'relative',
                                                    alignItems: 'flex-end',
                                                },
                                                dateInput: {
                                                    borderWidth: 0,
                                                    fontSize: scale(15),
                                                    height: scale(50),
                                                    width: scale(300),
                                                    alignSelf: 'center',
                                                },
                                            }}
                                            onDateChange={date => {
                                                this.setState({
                                                    date: date
                                                });
                                                this.changeinput("date", date)
                                            }}
                                            value={this.state.date}
                                        />
                                        <View
                                            style={{
                                                width: scale(230),
                                                borderColor: '#FFFFFF',
                                                borderWidth: 1,
                                                marginTop: verticalScale(10),
                                            }}
                                        />
                                        {this.state.status ?
                                            <View>
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        marginTop: verticalScale(15),
                                                        alignItems: 'flex-start',
                                                        width: scale(222),
                                                        justifyContent: 'space-between',

                                                    }}>
                                                    <Text
                                                        style={{
                                                            color: '#FFFFFF',
                                                            fontSize: scale(16),
                                                            alignSelf: 'center',
                                                        }}>
                                                        Height
                                </Text>
                                                    <TextInput
                                                        style={[styles.input]}
                                                        placeholder="Height in cm"
                                                        keyboardType="phone-pad"
                                                        onChangeText={height =>
                                                            this.changeinput("height", height)
                                                        }
                                                        value={this.state.nextVaccine[`0`].height}
                                                    />
                                                </View>
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        marginTop: verticalScale(15),
                                                        alignItems: 'flex-start',
                                                        width: scale(222),
                                                        justifyContent: 'space-between',
                                                    }}>
                                                    <Text
                                                        style={{
                                                            color: '#FFFFFF',
                                                            fontSize: scale(16),
                                                            alignSelf: 'center',
                                                        }}>
                                                        Weight
                        </Text>
                                                    <TextInput
                                                        style={[styles.input]}
                                                        placeholder="Weight in kg"
                                                        keyboardType="phone-pad"
                                                        onChangeText={weight =>
                                                            this.changeinput("weight", weight)
                                                        }
                                                        value={this.state.nextVaccine[`0`].weight}
                                                    />
                                                </View>
                                                <TouchableOpacity
                                                    style={{
                                                        justifyContent: 'center',
                                                        backgroundColor: '#fff',
                                                        marginTop: verticalScale(15),
                                                        alignItems: 'center',
                                                        borderRadius: 15,
                                                        width: scale(222),
                                                        padding: scale(10),
                                                        height: scale(50),
                                                    }}
                                                    activeOpacity={0.8}
                                                    onPress={() => {
                                                        this.changeLayout1();
                                                        //this.HideComponent1();
                                                    }}
                                                >
                                                    <Text
                                                        style={{
                                                            color: '#A076E8',
                                                            fontSize: scale(16),
                                                            fontWeight: 'bold',
                                                        }}>
                                                        NEXT
                          </Text>
                                                </TouchableOpacity>
                                                <View style={{ height: scale(10) }} />
                                            </View> : null}

                                        {this.state.selectedVaccine.length ?

                                            <View style={{ height: this.state.expanded1 ? null : 0, overflow: 'hidden', alignItems: 'center', }}>
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        marginTop: verticalScale(10),
                                                        width: scale(222),

                                                    }}>
                                                    <Image
                                                        style={{
                                                            width: scale(25),
                                                            height: scale(25),
                                                            alignSelf: 'center',
                                                        }}
                                                        source={require('../../assets/injectionwhite.png')}
                                                        resizeMode="contain"
                                                    />
                                                    <Text style={{ fontSize: scale(23), color: '#FFFFFF', alignSelf: "center", fontWeight: "bold", marginLeft: scale(15) }}>
                                                        {this.state.selectedVaccine[this.state.activSelectedVaccineIndex].name}
                                                    </Text>
                                                </View>
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        marginTop: verticalScale(10),
                                                        width: scale(222),
                                                        justifyContent: 'space-between',


                                                    }}>
                                                    <Text style={{ color: '#FFFFFF', fontSize: scale(16), alignSelf: "center", marginLeft: scale(10) }}>
                                                        Brand
                                        </Text>
                                                    <TextInput
                                                        style={[styles.input]}
                                                        placeholder="Brand Name"
                                                        keyboardType="default"
                                                        onChangeText={val => this.changeSelectedVaccineElement('brand', val, this.state.activSelectedVaccineIndex)}
                                                        value={this.state.selectedVaccine[this.state.activSelectedVaccineIndex].brand}

                                                    />
                                                </View>
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        marginTop: verticalScale(10),
                                                        width: scale(222),
                                                        justifyContent: 'space-between',

                                                    }}>
                                                    <Text style={{ color: '#FFFFFF', fontSize: scale(16), alignSelf: "center", marginLeft: scale(10) }}>
                                                        Mfg Date
                                         </Text>

                                                    <TouchableOpacity style={{
                                                        height: scale(40),
                                                        width: scale(118), borderRadius: 15,
                                                        backgroundColor: '#fff',
                                                        justifyContent: "center"
                                                    }}
                                                        onPress={() => this._showMfgDatePicker(this.state.activSelectedVaccineIndex)}>
                                                        <Text style={{ fontSize: scale(15), color: this.state.selectedVaccine[this.state.activSelectedVaccineIndex].mfgDate === "" ? "#A8A8A8" : "#000" }}>{this.state.selectedVaccine[this.state.activSelectedVaccineIndex].mfgDate === "" ? "MM/YYYY" : this.state.selectedVaccine[this.state.activSelectedVaccineIndex].mfgDate}</Text>
                                                    </TouchableOpacity>

                                                    {/* <TextInput
                                                    style={[styles.input]}
                                                    placeholder="MM/YYYY"
                                                    keyboardType="default"

                                                /> */}
                                                </View>
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        marginTop: verticalScale(10),
                                                        width: scale(222),
                                                        justifyContent: 'space-between',

                                                    }}>
                                                    <Text style={{ color: '#FFFFFF', fontSize: scale(16), alignSelf: "center", marginLeft: scale(10) }}>
                                                        Exp Date
                                         </Text>
                                                    <TouchableOpacity style={{
                                                        height: scale(40),
                                                        width: scale(118), borderRadius: 15,
                                                        backgroundColor: '#fff',
                                                        justifyContent: "center"
                                                    }}
                                                        onPress={() => this._showExpDatePicker(this.state.activSelectedVaccineIndex)}>
                                                        <Text style={{ fontSize: scale(15), color: this.state.selectedVaccine[this.state.activSelectedVaccineIndex].expDate === "" ? "#A8A8A8" : "#000" }}>{this.state.selectedVaccine[this.state.activSelectedVaccineIndex].expDate === "" ? "MM/YYYY" : this.state.selectedVaccine[this.state.activSelectedVaccineIndex].expDate}</Text>
                                                    </TouchableOpacity>
                                                    {/* <TextInput
                                                    style={[styles.input]}
                                                    placeholder="MM/YYYY"
                                                    keyboardType="default"
                                                    keyboardType="numeric"

                                                /> */}
                                                </View>
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        marginTop: verticalScale(10),
                                                        width: scale(222),
                                                        justifyContent: 'space-between',

                                                    }}>
                                                    <Text style={{ color: '#FFFFFF', fontSize: scale(16), alignSelf: "center", marginLeft: scale(10) }}>
                                                        Batch No
                                         </Text>
                                                    <TextInput
                                                        style={[styles.input]}
                                                        placeholder="Batch No"
                                                        keyboardType="default"
                                                        onChangeText={val => this.changeSelectedVaccineElement('batchNumber', val, this.state.activSelectedVaccineIndex)}
                                                        value={this.state.selectedVaccine[this.state.activSelectedVaccineIndex].batchNumber}
                                                    />
                                                </View>
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        marginTop: verticalScale(10),
                                                        width: scale(222),
                                                        justifyContent: 'space-between',

                                                    }}>
                                                    <Text style={{ color: '#FFFFFF', fontSize: scale(16), alignSelf: "center", marginLeft: scale(10) }}>
                                                        Hospital
                                        </Text>
                                                    <TextInput
                                                        style={[styles.input]}
                                                        placeholder="Hospital"
                                                        keyboardType="default"
                                                        onChangeText={val => this.changeSelectedVaccineElement('hospital', val, this.state.activSelectedVaccineIndex)}
                                                        value={this.state.selectedVaccine[this.state.activSelectedVaccineIndex].hospital}
                                                    />
                                                </View>
                                                <View
                                                    style={{
                                                        marginTop: verticalScale(10),
                                                        justifyContent: 'space-between',
                                                        alignItems: 'center',
                                                        flexDirection: 'row',
                                                        width: scale(222),
                                                    }}>
                                                    <TouchableOpacity
                                                        style={{
                                                            justifyContent: 'center',
                                                            backgroundColor: 'transparent',
                                                            alignItems: 'center',

                                                            borderRadius: 15,
                                                            borderWidth: 1,
                                                            borderColor: '#FFFFFF',
                                                            width: scale(106),

                                                            padding: scale(10),
                                                            height: scale(50),
                                                        }}
                                                        activeOpacity={0.8}
                                                        onPress={() => this.backHandler()}
                                                    >
                                                        <Text style={{ color: '#FFFFFF', fontSize: scale(16), }}>BACK</Text>
                                                    </TouchableOpacity>
                                                    {(this.state.selectedVaccine.length - 1) > this.state.activSelectedVaccineIndex ?
                                                        <TouchableOpacity
                                                            style={{
                                                                justifyContent: 'center',
                                                                backgroundColor: '#fff',
                                                                alignItems: 'center',

                                                                borderRadius: 15,
                                                                width: scale(106),

                                                                padding: scale(10),
                                                                height: scale(50),
                                                            }}
                                                            activeOpacity={0.8}
                                                            onPress={() => this.nextHandler()}
                                                        >
                                                            <Text style={{ color: '#A076E8', fontSize: scale(16), fontWeight: "bold" }}>NEXT</Text>
                                                        </TouchableOpacity>
                                                        :
                                                        <TouchableOpacity
                                                            style={{
                                                                justifyContent: 'center',
                                                                backgroundColor: '#fff',
                                                                alignItems: 'center',

                                                                borderRadius: 15,
                                                                width: scale(106),

                                                                padding: scale(10),
                                                                height: scale(50),
                                                            }}
                                                            activeOpacity={0.8}
                                                            onPress={() => this.saveHandler()}
                                                        >
                                                            <Text style={{ color: '#14B8BE', fontSize: scale(16), fontWeight: "bold" }}>SAVE</Text>
                                                        </TouchableOpacity>
                                                    }

                                                </View>
                                                <View style={{ height: scale(10) }} />
                                            </View>
                                            : null}
                                    </View>
                                </LinearGradient>
                            </ScrollView>
                        </View>

                        <FlatList
                            data={this.state.nextVaccine}
                            horizontal={true}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) => (
                                <>
                                    {index === 0 ? null :
                                        <LinearGradient
                                            colors={["#D6D6D6", "#9B9B9B"]}
                                            style={{ borderRadius: 30, alignItems: 'center', height: scale(410), width: (322), marginTop: verticalScale(20), marginLeft: scale(22) }}
                                            start={{ x: 0, y: 0 }}
                                            end={{ x: 1, y: 0 }}>
                                            <View style={{ flexDirection: "row", marginTop: verticalScale(15), marginLeft: scale(15), }}>
                                                <View style={{ width: "40%" }}>
                                                    <View style={{ width: scale(86), height: scale(86), borderRadius: 400, }}>
                                                        <Image
                                                            style={{ width: scale(86), height: scale(86), borderRadius: 400 }}
                                                            source={{ uri: this.props.ChildDetails.imageUrl }}
                                                            resizeMode='cover'
                                                        />
                                                    </View>
                                                </View>

                                                <View style={{ flexDirection: "column", width: "60%", }}>
                                                    <View>
                                                        <Text style={{ fontSize: scale(14), fontWeight: "bold", color: "#FFFFFF", }}>{item.due_date}</Text>
                                                    </View>
                                                    <View style={{ marginTop: verticalScale(20) }}>
                                                        <Text style={{ fontSize: scale(15), color: "#FFFFFF", fontWeight: "bold" }}>{this.props.ChildDetails.child.name}</Text>
                                                    </View>
                                                    <View style={{ marginTop: verticalScale(3) }}>
                                                        <Text style={{ fontSize: scale(11), color: "#FFFFFF" }}>{item.duration}</Text>
                                                    </View>
                                                </View>

                                            </View>

                                            {item.vaccines.map((vaccinename, index2) => (
                                                <View style={{ flexDirection: "row", marginLeft: scale(15), marginRight: scale(15), marginTop: index2 === 0 ? verticalScale(7) : verticalScale(7), justifyContent: 'center', alignItems: 'center', }}>
                                                    <View style={{ width: "80%" }}>
                                                        <View style={{ flexDirection: "row", }}>
                                                            <Image
                                                                style={{ width: scale(17), height: scale(17), alignSelf: "center" }}
                                                                source={require('../../assets/injectionwhite.png')}
                                                                resizeMode='contain'
                                                            />
                                                            <Text style={styles.vaccinestext}>{vaccinename.name}</Text>
                                                            <Image
                                                                style={{ width: scale(11), height: scale(11), alignSelf: "center", marginLeft: scale(7) }}
                                                                source={require('../../assets/info.png')}
                                                                resizeMode='contain'
                                                            />
                                                        </View>
                                                    </View>
                                                    <View style={{ width: "20%", justifyContent: 'center', alignItems: 'center' }}>
                                                        <TouchableHighlight
                                                            disabled={true}
                                                            underlayColor="transparent"
                                                            style={{ marginVertical: verticalScale(10) }}>
                                                            <View style={{ alignItems: 'center' }}>
                                                                {vaccinename.status ? (
                                                                    <Image
                                                                        source={require('../../assets/checkboxselected.png')}
                                                                        style={{
                                                                            width: scale(14),
                                                                            height: scale(14),
                                                                            borderRadius: 400,
                                                                        }}
                                                                        resizeMode="contain"
                                                                    />
                                                                ) : (
                                                                        <View
                                                                            style={{
                                                                                width: scale(14),
                                                                                height: scale(14),
                                                                                borderRadius: 400,
                                                                                backgroundColor: '#FFFFFF',
                                                                            }}
                                                                        />
                                                                    )}
                                                            </View>
                                                        </TouchableHighlight>

                                                    </View>
                                                </View>
                                            ))}

                                            <View style={{ bottom: 10, position: "absolute" }}>
                                                <TouchableOpacity
                                                    style={{
                                                        justifyContent: 'space-between',
                                                        backgroundColor: '#fff',
                                                        flexDirection: 'row',
                                                        borderRadius: 15,
                                                        width: scale(222),
                                                        height: scale(40),
                                                    }}
                                                    disabled={true}>
                                                    <Text style={{ marginLeft: scale(20), alignSelf: 'center' }}>
                                                        Vaccine given on
                                  </Text>
                                                    <Image
                                                        style={{
                                                            width: scale(20),
                                                            height: scale(20),
                                                            alignSelf: 'center',
                                                            marginRight: scale(20),
                                                        }}
                                                        source={require('../../assets/calandergrey.png')}
                                                        resizeMode="cover"
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                        </LinearGradient>
                                    }
                                </>
                            )} />
                    </View>
                </ScrollView>


            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f4f7f9',
    },
    vaccinestext: {
        fontSize: scale(16),
        color: '#FFFFFF',
        marginLeft: scale(10),
        marginTop: verticalScale(7),
    },
    input: {
        height: scale(40),
        width: scale(118),
        fontSize: scale(15),
        borderRadius: 15,
        backgroundColor: '#fff',
    },
    cardvaccine: {
        flex: 1,
        width: scale(272),
        backgroundColor: 'transparent',
        borderRadius: 30,
        marginTop: verticalScale(20),
    },
    btnSignin: {
        justifyContent: 'space-between',
        backgroundColor: '#fff',
        flexDirection: 'row',

        // alignItems: 'center',
        marginLeft: scale(15),
        borderRadius: 15,
        width: scale(222),
        marginRight: scale(20),
        padding: scale(10),
        height: scale(50),
    },
    Alert_Main_View: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#009688',
        height: scale(200),
        width: '90%',
        borderWidth: 1,
        borderColor: '#fff',
        borderRadius: 7,
    },

    Alert_Title: {
        fontSize: scale(25),
        color: '#fff',
        textAlign: 'center',
        padding: scale(10),
        height: '28%',
    },

    Alert_Message: {
        fontSize: scale(22),
        color: '#fff',
        textAlign: 'center',
        padding: scale(10),
        height: '42%',
    },

    buttonStyle: {
        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },

    TextStyle: {
        color: '#fff',
        textAlign: 'center',
        fontSize: scale(22),
        marginTop: verticalScale(-5),
    },

    container2: {
        marginTop: StatusBar.currentHeight,
    },

    scene: {
        flex: 1,
        borderBottomStartRadius: 20,
        borderBottomEndRadius: 20,
    },

    tabbar: {
        backgroundColor: 'transparent',
    },
    tab: {
        width: scale(120),
    },
    indicator: {
        backgroundColor: '#ffeb3b',
    },
    label: {
        fontWeight: 'bold',
        fontSize: scale(12),
        color: '#FFFFFF',
    },
});

const VaccineDetails = (childVaccines) => {
    const res = childVaccines;
    console.log('res', res);

    let data = [];
    for (let key in res) {
        const vaccineObject = {};
        var com = 0;
        vaccineObject = res[key]
        res[key].vaccines.forEach((vaccine, index) => {
            if (vaccine.givenDate !== "") {
                com = com + 1;
            }
            vaccineObject[`complete_count`] = com;
        });
        vaccineObject[`count`] = res[key].vaccines.length;
        var dateString1 = res[key].dueDate;
        var dateParts1 = dateString1.split("/");
        var dateObject1 = new Date(+dateParts1[2], dateParts1[1] - 1, +dateParts1[0]);
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";

        vaccineObject['duration'] = key
        vaccineObject['due_date'] = dateObject1.getDate() + " " + month[dateObject1.getMonth()] + ", " + dateObject1.getUTCFullYear()
        vaccineObject.vaccines.forEach(vaccine => {
            vaccine.status = vaccine.givenDate !== '';
        })
        data.push(vaccineObject);
    }
    //construct data object here for pass to the Flatlist
    console.log('next', data);
    let finalData = [];
    for (var i = 0; i < data.length; i++) {
        const v = {};
        if (data[i].count != data[i].complete_count) {
            finalData.push(data[i]);
        }
    }
    console.log('next', finalData);

    return finalData;
}

const vaccine = (childVaccines) => {
    const res = childVaccines;
    console.log('res', res);

    let data = [];
    for (let key in res) {
        const vaccineObject = {};
        var com = 0;
        vaccineObject = res[key]
        res[key].vaccines.forEach((vaccine, index) => {
            if (vaccine.givenDate !== "") {
                com = com + 1;
            }
            vaccineObject[`complete_count`] = com;
        });
        vaccineObject[`count`] = res[key].vaccines.length;
        var dateString1 = res[key].dueDate;
        var dateParts1 = dateString1.split("/");
        var dateObject1 = new Date(+dateParts1[2], dateParts1[1] - 1, +dateParts1[0]);
        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";

        vaccineObject['duration'] = key
        vaccineObject['due_date'] = dateObject1.getDate() + " " + month[dateObject1.getMonth()] + ", " + dateObject1.getUTCFullYear()
        vaccineObject.vaccines.forEach(vaccine => {
            vaccine.status = vaccine.givenDate !== '';
        })
        data.push(vaccineObject);
    }
    //construct data object here for pass to the Flatlist
    console.log('next', data);
    let finalData = [];
    for (var i = 0; i < data.length; i++) {
        const v = {};
        if (data[i].count != data[i].complete_count) {
            finalData.push(data[i]);
        }
    }
    console.log('next', finalData);

    return finalData;
}

function mapStateToProps(state) {
    let selectedChild = state.userinfo.selectedChild
    return {
        VaccineCard: state.userinfo.user[selectedChild].childVaccines,
        selectedChild: selectedChild,
        ChildDetails: state.userinfo.user[selectedChild],
        nextVaccine: VaccineDetails(state.userinfo.user[selectedChild].childVaccines),
        loder: state.loder,
        userSave: vaccine(state.userinfo.userSave[selectedChild].childVaccines)

    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes, Milestone, Issue_Vaccine })(Next_Due_Vaccine)


{/* <LinearGradient
                        colors={['#a076e8', '#5dc4dd']}
                        style={{ borderRadius: 30, alignItems: 'center', height: scale(410), width: (322),marginTop:verticalScale(15) }}
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}>
                        <View style={{ flexDirection: "row", marginTop: verticalScale(15), marginLeft: scale(15), }}>
                            <View style={{ width: "40%" }}>
                                <View style={{ width: scale(86), height: scale(86), borderRadius: 400, }}>
                                    <Image
                                        style={{ width: scale(86), height: scale(86), borderRadius: 400 }}
                                        source={{ uri: this.props.ChildDetails.imageUrl }}
                                        resizeMode='cover'
                                    />
                                </View>
                            </View>

                            <View style={{ flexDirection: "column", width: "60%", }}>
                                <View>
                                    <Text style={{ fontSize: scale(14), fontWeight: "bold", color: "#FFFFFF", }}>7 November, 2019</Text>
                                </View>
                                <View style={{ marginTop: verticalScale(20) }}>
                                    <Text style={{ fontSize: scale(15), color: "#FFFFFF", fontWeight: "bold" }}>{this.props.ChildDetails.child.name}</Text>
                                </View>
                                <View style={{ marginTop: verticalScale(3) }}>
                                    <Text style={{ fontSize: scale(11), color: "#FFFFFF" }}>6 Weeks</Text>
                                </View>
                            </View>

                        </View>

                        <View style={{ flexDirection: "row", marginLeft: scale(15), marginRight: scale(15), marginTop: verticalScale(20), justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{ width: "80%" }}>
                                <View style={{ flexDirection: "row", }}>
                                    <Image
                                        style={{ width: scale(17), height: scale(17), alignSelf: "center" }}
                                        source={require('../../assets/injectionwhite.png')}
                                        resizeMode='contain'
                                    />
                                    <Text style={styles.vaccinestext}>DTwP 1</Text>
                                    <Image
                                        style={{ width: scale(11), height: scale(11), alignSelf: "center", marginLeft: scale(7) }}
                                        source={require('../../assets/info.png')}
                                        resizeMode='contain'
                                    />
                                </View>
                            </View>
                            <View style={{ width: "20%", justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: scale(14), height: scale(14), borderRadius: 400, backgroundColor: "#FFFFFF" }} />
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", marginLeft: scale(15), marginRight: scale(15), marginTop: verticalScale(10), justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{ width: "80%" }}>
                                <View style={{ flexDirection: "row", }}>
                                    <Image
                                        style={{ width: scale(17), height: scale(17), alignSelf: "center" }}
                                        source={require('../../assets/injectionwhite.png')}
                                        resizeMode='contain'
                                    />
                                    <Text style={styles.vaccinestext}>IPV 1</Text>
                                    <Image
                                        style={{ width: scale(11), height: scale(11), alignSelf: "center", marginLeft: scale(7) }}
                                        source={require('../../assets/info.png')}
                                        resizeMode='contain'
                                    />
                                </View>
                            </View>
                            <View style={{ width: "20%", justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: scale(14), height: scale(14), borderRadius: 400, backgroundColor: "#FFFFFF" }} />
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", marginLeft: scale(15), marginRight: scale(15), marginTop: verticalScale(10), justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{ width: "80%" }}>
                                <View style={{ flexDirection: "row", }}>
                                    <Image
                                        style={{ width: scale(17), height: scale(17), alignSelf: "center" }}
                                        source={require('../../assets/injectionwhite.png')}
                                        resizeMode='contain'
                                    />
                                    <Text style={styles.vaccinestext}>IPV 1</Text>
                                    <Image
                                        style={{ width: scale(11), height: scale(11), alignSelf: "center", marginLeft: scale(7) }}
                                        source={require('../../assets/info.png')}
                                        resizeMode='contain'
                                    />
                                </View>
                            </View>
                            <View style={{ width: "20%", justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: scale(14), height: scale(14), borderRadius: 400, backgroundColor: "#FFFFFF" }} />
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", marginLeft: scale(15), marginRight: scale(15), marginTop: verticalScale(10), justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{ width: "80%" }}>
                                <View style={{ flexDirection: "row", }}>
                                    <Image
                                        style={{ width: scale(17), height: scale(17), alignSelf: "center" }}
                                        source={require('../../assets/injectionwhite.png')}
                                        resizeMode='contain'
                                    />
                                    <Text style={styles.vaccinestext}>IPV 1</Text>
                                    <Image
                                        style={{ width: scale(11), height: scale(11), alignSelf: "center", marginLeft: scale(7) }}
                                        source={require('../../assets/info.png')}
                                        resizeMode='contain'
                                    />
                                </View>
                            </View>
                            <View style={{ width: "20%", justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: scale(14), height: scale(14), borderRadius: 400, backgroundColor: "#FFFFFF" }} />
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", marginLeft: scale(15), marginRight: scale(15), marginTop: verticalScale(10), justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{ width: "80%" }}>
                                <View style={{ flexDirection: "row", }}>
                                    <Image
                                        style={{ width: scale(17), height: scale(17), alignSelf: "center" }}
                                        source={require('../../assets/injectionwhite.png')}
                                        resizeMode='contain'
                                    />
                                    <Text style={styles.vaccinestext}>IPV 1</Text>
                                    <Image
                                        style={{ width: scale(11), height: scale(11), alignSelf: "center", marginLeft: scale(7) }}
                                        source={require('../../assets/info.png')}
                                        resizeMode='contain'
                                    />
                                </View>
                            </View>
                            <View style={{ width: "20%", justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: scale(14), height: scale(14), borderRadius: 400, backgroundColor: "#FFFFFF" }} />
                            </View>
                        </View>

                        <View style={{ flexDirection: "row", marginLeft: scale(15), marginRight: scale(15), marginTop: verticalScale(10), justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{ width: "80%" }}>
                                <View style={{ flexDirection: "row", }}>
                                    <Image
                                        style={{ width: scale(17), height: scale(17), alignSelf: "center" }}
                                        source={require('../../assets/injectionwhite.png')}
                                        resizeMode='contain'
                                    />
                                    <Text style={styles.vaccinestext}>IPV 1</Text>
                                    <Image
                                        style={{ width: scale(11), height: scale(11), alignSelf: "center", marginLeft: scale(7) }}
                                        source={require('../../assets/info.png')}
                                        resizeMode='contain'
                                    />
                                </View>
                            </View>
                            <View style={{ width: "20%", justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: scale(14), height: scale(14), borderRadius: 400, backgroundColor: "#FFFFFF" }} />
                            </View>
                        </View>

                        <View style={{ bottom: 10, position: "absolute" }}>
                            <TouchableOpacity
                                style={{
                                    justifyContent: 'space-between',
                                    backgroundColor: '#fff',
                                    flexDirection: 'row',
                                    //   marginLeft: scale(15),
                                    borderRadius: 15,
                                    width: scale(222),
                                    //   marginRight: scale(20),
                                    height: scale(40),
                                }}>
                                <Text style={{ marginLeft: scale(20), alignSelf: 'center' }}>
                                    Vaccine given on
                      </Text>
                                <Image
                                    style={{
                                        width: scale(20),
                                        height: scale(20),
                                        alignSelf: 'center',
                                        marginRight: scale(20),
                                    }}
                                    source={require('../../assets/calandergrey.png')}
                                    resizeMode="cover"
                                />
                            </TouchableOpacity>
                        </View>
                    </LinearGradient> */}






{/* <FlatList
                    data={this.state.nextVaccine}
                    horizontal={true}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => (
                        <LinearGradient
                            colors={index === 0 ? ['#a076e8', '#5dc4dd'] : ["#D6D6D6", "#9B9B9B"]}
                            style={{ borderRadius: 30, alignItems: 'center', height: scale(410), width: (322), marginTop: verticalScale(20), marginLeft: scale(22) }}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}>
                            <View style={{ flexDirection: "row", marginTop: verticalScale(15), marginLeft: scale(15), }}>
                                <View style={{ width: "40%" }}>
                                    <View style={{ width: scale(86), height: scale(86), borderRadius: 400, }}>
                                        <Image
                                            style={{ width: scale(86), height: scale(86), borderRadius: 400 }}
                                            source={{ uri: this.props.ChildDetails.imageUrl }}
                                            resizeMode='cover'
                                        />
                                    </View>
                                </View>

                                <View style={{ flexDirection: "column", width: "60%", }}>
                                    <View>
                                        <Text style={{ fontSize: scale(14), fontWeight: "bold", color: "#FFFFFF", }}>{item.due_date}</Text>
                                    </View>
                                    <View style={{ marginTop: verticalScale(20) }}>
                                        <Text style={{ fontSize: scale(15), color: "#FFFFFF", fontWeight: "bold" }}>{this.props.ChildDetails.child.name}</Text>
                                    </View>
                                    <View style={{ marginTop: verticalScale(3) }}>
                                        <Text style={{ fontSize: scale(11), color: "#FFFFFF" }}>{item.duration}</Text>
                                    </View>
                                </View>

                            </View>

                            {item.vaccines.map((vaccinename, index2) => (
                                <View style={{ flexDirection: "row", marginLeft: scale(15), marginRight: scale(15), marginTop: index2 === 0 ? verticalScale(20) : verticalScale(10), justifyContent: 'center', alignItems: 'center', }}>
                                    <View style={{ width: "80%" }}>
                                        <View style={{ flexDirection: "row", }}>
                                            <Image
                                                style={{ width: scale(17), height: scale(17), alignSelf: "center" }}
                                                source={require('../../assets/injectionwhite.png')}
                                                resizeMode='contain'
                                            />
                                            <Text style={styles.vaccinestext}>{vaccinename.name}</Text>
                                            <Image
                                                style={{ width: scale(11), height: scale(11), alignSelf: "center", marginLeft: scale(7) }}
                                                source={require('../../assets/info.png')}
                                                resizeMode='contain'
                                            />
                                        </View>
                                    </View>
                                    <View style={{ width: "20%", justifyContent: 'center', alignItems: 'center' }}>
                                        <TouchableHighlight
                                            // disabled={isDisabled}
                                            onPress={() => this.onCheckboxClick(item.duration, vaccinename)}
                                            underlayColor="transparent"
                                            style={{ marginVertical: verticalScale(10) }}>
                                            <View style={{ alignItems: 'center' }}>
                                                {vaccinename.status ? (
                                                    <Image
                                                        source={require('../../assets/checkboxselected.png')}
                                                        style={{
                                                            width: scale(14),
                                                            height: scale(14),
                                                            borderRadius: 400,
                                                        }}
                                                        resizeMode="contain"
                                                    />
                                                ) : (
                                                        <View
                                                            style={{
                                                                width: scale(14),
                                                                height: scale(14),
                                                                borderRadius: 400,
                                                                backgroundColor: '#FFFFFF',
                                                            }}
                                                        />
                                                    )}
                                            </View>
                                        </TouchableHighlight>
                                        
                                    </View>
                                </View>
                            ))}
                            {!this.state.expanded ?
                                <View style={{ bottom: 10, position: "absolute" }}>
                                    <TouchableOpacity
                                        style={{
                                            justifyContent: 'space-between',
                                            backgroundColor: '#fff',
                                            flexDirection: 'row',
                                            borderRadius: 15,
                                            width: scale(222),
                                            height: scale(40),
                                        }}
                                        onPress={()=>this.changeLayout()}
                                        disabled={index === 0 ? false : true}>
                                        <Text style={{ marginLeft: scale(20), alignSelf: 'center' }}>
                                            Vaccine given on
                                  </Text>
                                        <Image
                                            style={{
                                                width: scale(20),
                                                height: scale(20),
                                                alignSelf: 'center',
                                                marginRight: scale(20),
                                            }}
                                            source={require('../../assets/calandergrey.png')}
                                            resizeMode="cover"
                                        />
                                    </TouchableOpacity>
                                </View>
                                : null}

                            <View style={{ height: this.state.expanded ? null : 0, overflow: 'hidden', alignItems: 'center', }}>
                                <DatePicker
                                    style={{
                                        height: scale(50),
                                        width: scale(222),
                                        justifyContent: 'center',
                                        marginLeft: scale(15),
                                        backgroundColor: '#FFFFFF',
                                        borderRadius: 10,
                                        marginRight: scale(20),
                                    }}
                                    //date={childData.givenDate} //initial date from state
                                    mode="date" //The enum of date, datetime and time
                                    //placeholder={childData.dueDate}
                                    placeHolderTextStyle={{ color: '#707070', fontSize: scale(15) }}
                                    format="DD/MM/YYYY"
                                    //minDate={this.state.dataSource.child.dob}
                                    //minDate={childData.dueDate}
                                    maxDate="01/01/2100"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    iconComponent={
                                        <Image
                                            style={{
                                                width: 20,
                                                height: 20,
                                                marginRight: 20,
                                                alignSelf: 'center',
                                            }}
                                            source={require('../../assets/calandergrey.png')}
                                            resizeMode="cover"
                                        />
                                    }
                                    customStyles={{
                                        dateIcon: {
                                            position: 'relative',
                                            alignItems: 'flex-end',
                                        },
                                        dateInput: {
                                            borderWidth: 0,
                                            fontSize: scale(15),
                                            height: scale(50),
                                            width: scale(300),
                                            alignSelf: 'center',
                                        },
                                    }}
                                    // onDateChange={date => {
                                    //   this.setState({
                                    //     childData: {...childData, givenDate: date,},
                                    //     date:date
                                    //   });
                                    // }}
                                    value={this.state.date}
                                />
                                <Text>hello</Text>
                            </View>
                        </LinearGradient>)} /> */}