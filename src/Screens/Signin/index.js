import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    Dimensions,
    ScrollView,
    KeyboardAvoidingView,
    TouchableOpacity,
    PermissionsAndroid,
    Platform,
    Switch,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import { login,fbloginUser } from '../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';
import url from "../../API";
import setAuthToken from "../../utils/setAuthToken";
import { scale, moderateScale, verticalScale } from '../../Components/Scale';
import { FBLogin, FBLoginManager } from 'react-native-facebook-login';
import GoogleSignIn from 'react-native-google-sign-in';
import FBLoginView from '../../Components/FBLoginView';

const screenDimension = Dimensions.get('window');
const isBigDevice = screenDimension.height > 600;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const shadowStyle = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
}

class Signin extends React.Component {
    constructor(props) {
        super(props);
        this.toggleSwitch = this.toggleSwitch.bind(this);
        this.state = {
            email: '',
            password: '',
            error1: false,
            error3: false,
            error2: false,
            loading: false,
            size: '',
            loggedIn: null,
            check: false,
            showPassword: true,
        }
    }

    toggleSwitch() {
        this.setState({ showPassword: !this.state.showPassword });
    }
    validateEmail() {
        let reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return reg_email.test(this.state.email);
    }
    onGoogleLogin = async e => {
        await GoogleSignIn.configure({
            clientID: '361840620293-nhomkhv77sceqhi8benpjevmhq13vn96.apps.googleusercontent.com',
            scopes: ['openid', 'email', 'profile'],
            shouldFetchBasicProfile: true,
        });

        GoogleSignIn.signInPromise().then((user) => {
            console.log('signInPromise resolved', user);
            setTimeout(() => {
                this.storeTokenAndNavigate(user.email);
            }, 1000);
        }, (e) => {
            console.log('signInPromise rejected', e);
            setTimeout(() => {
                alert(`signInPromise error: ${JSON.stringify(e)}`);
            }, 1000);
        });
    }
    onFBLogin = async (e) => {
        if (!e.profile) return;
        this.storeTokenAndNavigate(e.profile.email);
    }
    storeTokenAndNavigate = async (username) => {
        const data = {
            "username": username,
        }
        try {
            //await AsyncStorage.setItem('username', username);
            const response = await this.props.fbLoginUser(data)
            console.log('sigin',response.data);
            if (response.data) {
                await AsyncStorage.setItem('username', username);
                this.props.navigation.navigate('App');

            }
            else this.setState({ error: "Incorrect Username Or Password!!" })
        }
        catch (e) {
            console.log("fbloginUser_error", e.response)
            console.log("fbloginUser_error", e)
            alert('User not registered');
        }
    }
    _signin = async () => {
        var {
            email,
            password
        } = this.state;
        const data = {
            "username": email,
            "password": password
        }

        if (email == "") {
            this.setState({ error1: true });
        } else if (!this.validateEmail()) {
            this.setState({ error2: true,
                error1: false });
        } else if (password == "") {
            this.setState({ error2: false,
                error1: false,
                error3: true });
        }
         else {
            console.log(data);
            console.log(data.username);
            
            const result = await this.props.login(data)
            if (result.status === 200) {
                await AsyncStorage.setItem('username', data.username);
                this.props.navigation.navigate('App');
            }
            console.log('result',result);

        //     const response = await loginUser(data);
        //     console.log(response.data);
        //     console.log(response.data.token);
        //     if (response.data) {
        //         await AsyncStorage.setItem('logged', '1');
        //         //await AsyncStorage.setItem('selectedchild', '0');
        //         await AsyncStorage.setItem('token', response.data.token);

        //         this.props.navigation.navigate('App');

        //     }
        //     else this.setState({ error: "Incorrect Username Or Password!!" })
        // }
         }
    }

      render() {
        return (
<View style={styles.container}>
                <Image
                    style={{ width: scale(150), height: scale(150), borderWidth: 0, }}
                    source={require('../../assets/logo1.png')}
                    resizeMode='contain'
                />

                <View style={[styles.SectionStyle, shadowStyle]}>
                    <TextInput
                        style={[styles.input]}
                        placeholder="Email"
                        autoCapitalize="none"
                        keyboardType="email-address"
                        onChangeText={(email) => this.setState({ email })}
                        value={this.state.email} />
                </View>
                {this.state.error1 ?
                    <View style={{ width: scale(300), height: scale(20),marginTop:verticalScale(2) }}>
                        <Text style={{ color: "red",marginLeft:scale(15) }}>Please Enter Email</Text>
                    </View> :
                    this.state.error2 ?
                        <View style={{ width: scale(300), height: scale(20),marginTop:verticalScale(2) }}>
                            <Text style={{ color: "red",marginLeft:scale(15) }}>Email Incorrect. Please Try Again</Text></View> : null}

                <View style={[styles.SectionStyle, shadowStyle]}>
                    <TextInput
                        style={{

                            height: scale(50),
                            width: scale(250),

                            fontSize: scale(15),
                            borderRadius: 10,
                            backgroundColor: '#fff',
                        }}
                        placeholder="Password"
                        keyboardType="default"
                        onChangeText={(password) => this.setState({ password })}
                        value={this.state.password}
                        secureTextEntry={this.state.showPassword} />

                    <TouchableOpacity activeOpacity={0.8} style={{
                        position: 'absolute',
                        right: 20,
                        alignSelf: "center",
                        height: 40,
                        width: 35,
                    }} onPress={this.toggleSwitch}>
                        <Image source={(this.state.showPassword) ? require('../../assets/HidePassword.png') : require('../../assets/ShowPassword.png')} style={{
                            resizeMode: 'contain',
                            height: '100%',
                            alignSelf: "center",
                            width: '100%',
                        }} />
                    </TouchableOpacity>

                </View>


                {this.state.error3 ?
                    <View style={{ width: scale(300), height: scale(20),marginTop:verticalScale(2) }}>
                        <Text style={{ color: "red",marginLeft:scale(15) }}>Please Enter Password</Text></View> : null}
                


                <TouchableOpacity
                    style={[styles.btnSignin, shadowStyle]}
                    onPress={this._signin}>
                    <LinearGradient
                        start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                        colors={['#a076e8', '#5dc4dd']}
                        style={styles.linearGradient}>
                        <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: scale(15) }}>Log In </Text>
                    </LinearGradient>
                </TouchableOpacity>


                <View style={{ flexDirection: 'row', width: scale(300), justifyContent: 'space-between' }}>
                    <TouchableOpacity
                        style={[styles.btn, shadowStyle]}
                        onPress={this.onGoogleLogin}>
                        <Image
                            style={{ width: scale(25), height: scale(25), borderWidth: 0, }}
                            source={require('../../assets/googleplus.png')}
                            resizeMode='contain' />
                        <Text style={{ color: '#db4a39', fontWeight: 'bold', fontSize: scale(14), marginLeft: scale(10) }}>Google </Text>
                    </TouchableOpacity>
                    <FBLogin
                        buttonView={<FBLoginView />}
                        ref={(fbLogin) => { this.fbLogin = fbLogin }}
                        loginBehavior={FBLoginManager.LoginBehaviors.WebView}
                        permissions={["email", "user_friends"]}
                        onLogin={this.onFBLogin}
                        onLoginFound={this.onFBLogin}
                        onLoginNotFound={function (e) { console.log(e) }}
                        onLogout={function (e) { console.log(e) }}
                        onCancel={function (e) { console.log(e) }}
                        onPermissionsMissing={function (e) { console.log(e) }}
                    />
                </View>

                <View style={{ flexDirection: 'row', width: scale(300), justifyContent: 'space-between', marginTop: verticalScale(30) }}>
                    <TouchableOpacity
                        style={styles.txt}
                        onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                        <Text style={{ color: '#707070', fontSize: scale(13), marginLeft: scale(10) }}>Forgot Password</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.txt}
                        onPress={() => this.props.navigation.navigate('Signup')}>
                        <Text style={{ color: '#707070', fontSize: scale(13), marginLeft: scale(10) }}>Sign Up</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        margin: scale(15),
        height: scale(50),
        width: scale(300),
        paddingLeft: scale(28),
        fontSize: scale(15),
        borderRadius: 10,
        backgroundColor: '#fff',
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        borderRadius: 10,
        position: "relative",
        height: scale(50),
        width: scale(300),
        marginTop: verticalScale(20)
    },
    linearGradient: {
        marginLeft: scale(20),
        borderRadius: 10,
        justifyContent: 'center',
        width: scale(300),
        alignItems: 'center',
        marginRight: scale(20),
        padding: scale(10),
        height: scale(50),
    },
    btnSignin: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        marginLeft: scale(20),
        borderRadius: 10,
        marginTop: verticalScale(40),
        width: scale(300),
        marginRight: scale(20),
        padding: scale(10),
        height: scale(50),
    },
    btn: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        flexDirection: 'row',
        borderRadius: 10,
        padding: scale(10),
        height: scale(40),
        marginTop: verticalScale(30),
        width: scale(135),
    },
    txt: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: scale(10),
        height: scale(40),
        width: scale(135),
    }
})

function mapStateToProps(state) {
    return {
        // otpdata: state.otp.otpdata,
        // userdata: state.register.userdata,
    }
}

export default connect(mapStateToProps, { login,fbloginUser })(Signin)
