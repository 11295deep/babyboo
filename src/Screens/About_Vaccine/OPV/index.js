import React from 'react';
import { Button, View, Text, StyleSheet, ImageBackground, Image, Animated, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../Components/Scale';
import Header from '../../../Components/Header';
import LinearGradient from 'react-native-linear-gradient';
import HamburgerIcon from "../../../Components/HamburgerIcon"
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import RadioButton from "../../../Components/RadioButton"

const ABOUT = () => (
    <View style={{ backgroundColor: '#FFFFFF', borderRadius: 10, }}>
        <Text style={{ fontSize: 15, marginTop: 15, marginLeft: 15, marginRight: 15,textAlign:'justify' }}>
                        The action of oral polio vaccine (OPV) is two-pronged. OPV produces antibodies in the blood ('humoral' or serum immunity) to all three types of poliovirus, and in the event of infection, this protects the individual against polio paralysis by preventing the spread of poliovirus to the nervous system. Virus. After the vaccine is given, your body makes antibodies that protect you against the virus. These antibodies will fight off the infection
                </Text>

                    <Text style={{ fontSize: 15, marginTop: 15, fontWeight: "bold", marginLeft: 15, marginRight: 15,textAlign:'justify' }}>
                        How does it work?
                </Text>

                    <Text style={{ fontSize: 15, marginTop: 15, marginLeft: 15, marginRight: 15,textAlign:'justify' }}>
                        Inactivated polio vaccine (IPV). IVP produces antibodies in the blood to all three types of poliovirus. In the event of infection, these antibodies prevent the spread of the virus to the central nervous system and protect against paralysis.
                </Text>
        <View style={{ height: scale(15) }} />
    </View>
);

const DOSES = () => (
    <View style={{ backgroundColor: '#FFFFFF', borderRadius: 10, }}>
        <Image
                    style={{ width: 298, height: 200,marginTop: 15,alignSelf:"center",justifyContent:"center",alignItems:"center" }}
                    source={require('../../../assets/Oral.png')}
                    resizeMode='cover'
                />

                <Text style={{ fontSize: 15, marginTop: 15, marginLeft: 15, marginRight: 15, fontWeight: "bold", textAlign: 'justify' }}>
                Oral route:
            </Text>
                <Text style={{ fontSize: 15, marginTop: 15, marginLeft: 15, marginRight: 15, textAlign: 'justify' }}>
                Administered by mouth
            </Text>
                <Text style={{ fontSize: 15, marginTop: 15, marginLeft: 15, marginRight: 15,textAlign:'justify' }}>
                    OPV dose at birth (also called 'zero dose'), followed by the primary series of 3 OPV doses and at least 1 IPV dose.
            </Text>
        <View style={{ height: scale(15) }} />
    </View>
);

const SIDE_EFFECTS = () => (
    <View style={{ backgroundColor: '#FFFFFF', borderRadius: 10, }}>
        <Text style={{ fontSize: 15, marginTop: 15, marginLeft: 15, marginRight: 15, fontWeight: "bold",textAlign:'justify' }}>
                        Local Effects:
            </Text >
                    <Text style={{ fontSize: 15, marginTop: 30, marginLeft: 30, marginRight: 15,textAlign:'justify' }}>
                        Redness, pain, swelling, or a lump where the shot was given
            </Text>
                    <Text style={{ fontSize: 15, marginTop: 10, marginLeft: 30, marginRight: 15,textAlign:'justify' }}>
                        Low fever
            </Text>

                    <Text style={{ fontSize: 15, marginTop: 15, marginLeft: 15, marginRight: 15, fontWeight: "bold",textAlign:'justify' }}>
                        Severe Effects:
            </Text>
                    <Text style={{ fontSize: 15, marginTop: 30, marginLeft: 30, marginRight: 15,textAlign:'justify' }}>
                        Joint pain
            </Text>
                    <Text style={{ fontSize: 15, marginTop: 10, marginLeft: 30, marginRight: 15,textAlign:'justify' }}>
                        Body aches
            </Text>
                    <Text style={{ fontSize: 15, marginTop: 10, marginLeft: 30, marginRight: 15,textAlign:'justify' }}>
                        Drowsiness
            </Text>
                    <Text style={{ fontSize: 15, marginTop: 10, marginLeft: 30, marginRight: 15,textAlign:'justify' }}>
                        Mild fussiness
            </Text>
                    <Text style={{ fontSize: 15, marginTop: 10, marginLeft: 30, marginRight: 15,textAlign:'justify' }}>
                        Crying
            </Text>
                    <Text style={{ fontSize: 15, marginTop: 10, marginLeft: 30, marginRight: 15,textAlign:'justify' }}>
                        Vomiting
            </Text>
        <View style={{ height: scale(15) }} />
    </View>
);

class OPV extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: '  About Vaccines  ',
        headerLeft: <HamburgerIcon />,
        headerBackground: <Header />,
        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: scale(20),
            fontFamily: "Roboto-Bold",
            fontWeight: "bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        },
    });
    constructor(props) {
        super(props);
        this.state = {
            radioItems:
                [
                    {
                        label: 'ABOUT',
                        selected: true
                    },
                    {
                        label: 'DOSES',
                        selected: false,
                    },
                    {
                        label: 'SIDE EFFECTS',
                        selected: false
                    }
                ], selectedItem: '',

            index: 0,
            routes: [
                { key: 'ABOUT', title: ' ABOUT ' },
                { key: 'DOSES', title: ' DOSES ' },
                { key: 'SIDE_EFFECTS', title: ' SIDE_EFFECTS ' },
            ],
        };
    }
    componentDidMount() {
        this.state.radioItems.map((item) => {
            if (item.selected == true) {
                this.setState({ selectedItem: item.label });
            }
        });
    }
    changeActiveRadioButton(index) {
        this.state.radioItems.map((item) => {
            item.selected = false;
        });

        this.state.radioItems[index].selected = true;
        //alert(this.state.radioItems[index].label)

        this.setState({ radioItems: this.state.radioItems }, () => {
            this.setState({ selectedItem: this.state.radioItems[index].label });
        });
    }
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={{ marginTop: scale(15), marginLeft: scale(20), width: scale(100), height: scale(35), borderRadius: 10, flexDirection: "row", justifyContent: "space-around", alignItems: "center", backgroundColor: "transparent", borderColor: "#707070", borderWidth: 1 }}
                    onPress={() => this.props.navigation.navigate('About_Vaccine')}>
                    <Image
                        style={{ width: scale(15), height: scale(15) }}
                        source={require('../../../assets/arrow.png')}
                        resizeMode='contain'
                    />
                    <Text style={{ fontSize: scale(18), color: "#707070", }}>Back</Text>
                </TouchableOpacity>

                <View style={{ marginLeft: scale(20), marginRight: scale(20), backgroundColor: "#FFFFFF", borderRadius: 10, marginTop: verticalScale(20), height: scale(124) }}>
                    <View style={{ flexDirection: "row", }}>
                        <View style={{
                            width: "20%",
                            backgroundColor: "transparent",
                            borderRadius: 400,
                            top: scale(10),
                            left: scale(15),
                            position: "relative"
                        }}>
                            <LinearGradient
                                start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                                colors={['#a076e8', '#5dc4dd']}
                                style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                                <Image
                                    style={{
                                        width: scale(16.08),
                                        height: scale(16.08),
                                    }}
                                    source={require('../../../assets/injection2.png')}
                                />
                            </LinearGradient>
                        </View>
                        <View style={{
                            width: "80%",
                            marginTop: verticalScale(12)
                        }}>
                            <Text style={{ fontSize: scale(18), fontWeight: "bold" }}>OPV</Text>
                            <Text style={{ fontSize: scale(16), marginTop: scale(10) }}>Oral Polio Vaccine</Text>
                        </View>
                    </View>

                    <View style={{ height: scale(40), bottom: 0, position: "absolute", flexDirection: "row", justifyContent: "center", marginLeft: scale(10), marginRight: (10) }}>
                        <View style={{ flexDirection: "row", }}>
                            {
                                this.state.radioItems.map((item, key) =>
                                    (
                                        <RadioButton key={key} button={item} onClick={this.changeActiveRadioButton.bind(this, key)} />
                                    ))
                            }
                        </View>
                        {/* <TouchableOpacity style={{ width: "33.33%", borderBottomWidth: 2, borderBottomColor: "#6F2BE2", alignItems: "center", justifyContent: "center", borderTopLeftRadius: 4, borderTopRightRadius: 4 }}>
                            <Text style={{ fontSize: scale(14), fontWeight: "bold" }}>hello</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: "33.33%", alignItems: "center", justifyContent: "center", }}>
                            <Text style={{ fontSize: scale(14) }}>hello</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: "33.33%", alignItems: "center", justifyContent: "center", }}>
                            <Text style={{ fontSize: scale(14) }}>hello</Text>
                        </TouchableOpacity> */}

                    </View>

                </View>

                <View style={{ height: scale(20) }} />
                <ScrollView showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}>
                    <View style={{ marginLeft: scale(20), marginRight: scale(20), borderRadius: 10, }}>

                        {this.state.selectedItem === "ABOUT" ?
                            <ABOUT /> :
                            this.state.selectedItem === "DOSES" ?
                                <DOSES />
                                :
                                <SIDE_EFFECTS />
                        }

                    </View>
                    <View style={{ height: scale(20) }} />
                </ScrollView>


            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f7f9',
    },
    tabbar: {
        backgroundColor: 'transparent',
        height: scale(40),
        //backgroundColor: "#000",

    },
    indicator: {
        backgroundColor: '#6F2BE2',
        width: scale(110),
        borderRadius: 3,
    },
    scene: {
        flex: 1,
    },
})

export default OPV;