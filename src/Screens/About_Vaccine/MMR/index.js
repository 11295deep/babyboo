import React from 'react';
import { Button, View, Text, StyleSheet, ImageBackground, Image, Animated, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../Components/Scale';
import Header from '../../../Components/Header';
import LinearGradient from 'react-native-linear-gradient';
import HamburgerIcon from "../../../Components/HamburgerIcon"
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import RadioButton from "../../../Components/RadioButton"

const ABOUT = () => (
    <View style={{ backgroundColor: '#FFFFFF', borderRadius: 10, }}>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), marginRight: scale(15), textAlign: 'justify' }}>
        The MMR vaccine is a vaccine against measles, mumps, and rubella.The vaccine is also recommended in those who do not have evidence of immunity,those with well controlled HIV/AIDSand within 72 hours of exposure to measles among those who are incompletely immunized.It is given by injection.
                </Text>

        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), fontWeight: "bold", marginLeft: scale(15), marginRight: scale(15), textAlign: 'justify' }}>
            How does it work?
                </Text>

        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), textAlign: 'justify', marginRight: scale(15), }}>
        The MMR vaccine contains weakened versions of live measles, mumps and rubella viruses. The vaccine works by triggering the immune system to produce antibodies against measles, mumps and rubella.
                </Text>
        <View style={{ height: scale(15) }} />
    </View>
);

const DOSES = () => (
    <View style={{ backgroundColor: '#FFFFFF', borderRadius: 10, }}>
        <Image
            style={{ width: scale(298), height: scale(200), marginTop: verticalScale(15), alignSelf: "center", justifyContent: "center", alignItems: "center" }}
            source={require('../../../assets/Subcutaneous.png')}
            resizeMode='cover'
        />

        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), fontWeight: "bold", textAlign: 'justify' }}>
        Subcutaneous route:
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), textAlign: 'justify' }}>
        Injected into the area just beneath the skin into the fatty, connective tissue
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), fontWeight: "bold", textAlign: 'justify' }}>
            Dose:
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15),marginRight: scale(15), textAlign: 'justify' }}>
        All children get two doses of MMR (measles-mumps-rubella) vaccine, starting with the first dose at 12 through 15 months of age, and the second dose at 4 through 6 years of age. Children can receive the second dose earlier as long as it is at least 28 days after the first dose.
            </Text>
        
        <View style={{ height: scale(15) }} />
    </View>
);

const SIDE_EFFECTS = () => (
    <View style={{ backgroundColor: '#FFFFFF', borderRadius: 10, }}>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), fontWeight: "bold", textAlign: 'justify' }}>
            Local Effects:
            </Text >
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(30), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
        Injection site reactions (pain, redness, swelling, or a lump),
            </Text>

        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), fontWeight: "bold", textAlign: 'justify' }}>
            Severe Effects:
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(30), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
            Fever
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
        Rash
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
        Headache
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
        Dizziness
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
        Joint or muscle pain
            </Text>
            <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
            Nausea
            </Text>
            <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
            Vomiting
            </Text>
            <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
            Diarrhea
            </Text>
        <View style={{ height: scale(15) }} />
    </View>
);

class MMR extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: '  About Vaccines  ',
        headerLeft: <HamburgerIcon />,
        headerBackground: <Header />,
        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: scale(20),
            fontFamily: "Roboto-Bold",
            fontWeight: "bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        },
    });
    constructor(props) {
        super(props);
        this.state = {
            radioItems:
                [
                    {
                        label: 'ABOUT',
                        selected: true
                    },
                    {
                        label: 'DOSES',
                        selected: false,
                    },
                    {
                        label: 'SIDE EFFECTS',
                        selected: false
                    }
                ], selectedItem: '',

            index: 0,
            routes: [
                { key: 'ABOUT', title: ' ABOUT ' },
                { key: 'DOSES', title: ' DOSES ' },
                { key: 'SIDE_EFFECTS', title: ' SIDE_EFFECTS ' },
            ],
        };
    }
    componentDidMount() {
        this.state.radioItems.map((item) => {
            if (item.selected == true) {
                this.setState({ selectedItem: item.label });
            }
        });
    }
    changeActiveRadioButton(index) {
        this.state.radioItems.map((item) => {
            item.selected = false;
        });

        this.state.radioItems[index].selected = true;
        //alert(this.state.radioItems[index].label)

        this.setState({ radioItems: this.state.radioItems }, () => {
            this.setState({ selectedItem: this.state.radioItems[index].label });
        });
    }
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={{ marginTop: scale(15), marginLeft: scale(20), width: scale(100), height: scale(35), borderRadius: 10, flexDirection: "row", justifyContent: "space-around", alignItems: "center", backgroundColor: "transparent", borderColor: "#707070", borderWidth: 1 }}
                    onPress={() => this.props.navigation.navigate('About_Vaccine')}>
                    <Image
                        style={{ width: scale(15), height: scale(15) }}
                        source={require('../../../assets/arrow.png')}
                        resizeMode='contain'
                    />
                    <Text style={{ fontSize: scale(18), color: "#707070", }}>Back</Text>
                </TouchableOpacity>

                <View style={{ marginLeft: scale(20), marginRight: scale(20), backgroundColor: "#FFFFFF", borderRadius: 10, marginTop: verticalScale(20), height: scale(124) }}>
                    <View style={{ flexDirection: "row", }}>
                        <View style={{
                            width: "20%",
                            backgroundColor: "transparent",
                            borderRadius: 400,
                            top: scale(10),
                            left: scale(15),
                            position: "relative"
                        }}>
                            <LinearGradient
                                start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                                colors={['#a076e8', '#5dc4dd']}
                                style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                                <Image
                                    style={{
                                        width: scale(16.08),
                                        height: scale(16.08),
                                    }}
                                    source={require('../../../assets/injection2.png')}
                                />
                            </LinearGradient>
                        </View>
                        <View style={{
                            width: "80%",
                            marginTop: verticalScale(12)
                        }}>
                            <Text style={{ fontSize: scale(18), fontWeight: "bold" }}>MMR</Text>
                            <Text style={{ fontSize: scale(16), marginTop: scale(10) }}>Measles, Mumps, & Rubella</Text>
                        </View>
                    </View>

                    <View style={{ height: scale(40), bottom: 0, position: "absolute", flexDirection: "row", justifyContent: "center", marginLeft: scale(10), marginRight: (10) }}>
                        <View style={{ flexDirection: "row", }}>
                            {
                                this.state.radioItems.map((item, key) =>
                                    (
                                        <RadioButton key={key} button={item} onClick={this.changeActiveRadioButton.bind(this, key)} />
                                    ))
                            }
                        </View>
                        {/* <TouchableOpacity style={{ width: "33.33%", borderBottomWidth: 2, borderBottomColor: "#6F2BE2", alignItems: "center", justifyContent: "center", borderTopLeftRadius: 4, borderTopRightRadius: 4 }}>
                            <Text style={{ fontSize: scale(14), fontWeight: "bold" }}>hello</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: "33.33%", alignItems: "center", justifyContent: "center", }}>
                            <Text style={{ fontSize: scale(14) }}>hello</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: "33.33%", alignItems: "center", justifyContent: "center", }}>
                            <Text style={{ fontSize: scale(14) }}>hello</Text>
                        </TouchableOpacity> */}

                    </View>

                </View>

                <View style={{ height: scale(20) }} />
                <ScrollView showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}>
                    <View style={{ marginLeft: scale(20), marginRight: scale(20), borderRadius: 10, }}>

                        {this.state.selectedItem === "ABOUT" ?
                            <ABOUT /> :
                            this.state.selectedItem === "DOSES" ?
                                <DOSES />
                                :
                                <SIDE_EFFECTS />
                        }

                    </View>
                    <View style={{ height: scale(20) }} />
                </ScrollView>


            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f7f9',
    },
    tabbar: {
        backgroundColor: 'transparent',
        height: scale(40),
        //backgroundColor: "#000",

    },
    indicator: {
        backgroundColor: '#6F2BE2',
        width: scale(110),
        borderRadius: 3,
    },
    scene: {
        flex: 1,
    },
})

export default MMR;