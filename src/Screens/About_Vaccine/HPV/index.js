import React from 'react';
import { Button, View, Text, StyleSheet, ImageBackground, Image, Animated, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../Components/Scale';
import Header from '../../../Components/Header';
import LinearGradient from 'react-native-linear-gradient';
import HamburgerIcon from "../../../Components/HamburgerIcon"
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import RadioButton from "../../../Components/RadioButton"

const ABOUT = () => (
    <View style={{ backgroundColor: '#FFFFFF', borderRadius: 10, }}>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), marginRight: scale(15), textAlign: 'justify' }}>
        A vaccine that helps protect the body against infection with certain types of human papillomavirus (HPV). HPV infection can cause abnormal tissue growth, such as warts, and other changes to cells. Infection for a long time with certain types of HPV can cause cancers of the cervix, vagina, vulva, anus, penis, and oropharynx. HPV vaccines are being used to prevent some of these cancers. They are also being used to prevent genital warts and abnormal lesions that may lead to some of these cancers. Also called human papillomavirus vaccine.
                </Text>

        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), fontWeight: "bold", marginLeft: scale(15), marginRight: scale(15), textAlign: 'justify' }}>
            How does it work?
                </Text>

        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), textAlign: 'justify', marginRight: scale(15), }}>
        HPV vaccine can prevent most genital warts and most cases of cervical cancer. Protection from HPV vaccine is expected to be long-lasting. But vaccinated women still need cervical cancer screening because the vaccine does not protect against all HPV types that cause cervical cancer. 
                </Text>
        <View style={{ height: scale(15) }} />
    </View>
);

const DOSES = () => (
    <View style={{ backgroundColor: '#FFFFFF', borderRadius: 10, }}>
        <Image
            style={{ width: scale(298), height: scale(200), marginTop: verticalScale(15), alignSelf: "center", justifyContent: "center", alignItems: "center" }}
            source={require('../../../assets/Intramuscular.png')}
            resizeMode='cover'
        />

        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), fontWeight: "bold", textAlign: 'justify' }}>
        Intramuscular route:
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), textAlign: 'justify' }}>
        Injected into muscle tissue
            </Text>
            <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15),marginRight: scale(15), textAlign: 'justify' }}>
            HPV vaccine is given in the deltoid region of upper arm or in the higher anterolateral of the thigh. The preferred site of administration is in the  deltoid region of upper arm.
            </Text>
            
        
        <View style={{ height: scale(15) }} />
    </View>
);

const SIDE_EFFECTS = () => (
    <View style={{ backgroundColor: '#FFFFFF', borderRadius: 10, }}>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), fontWeight: "bold", textAlign: 'justify' }}>
        Local Effects:
            </Text >
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(30), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
        Redness
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
        Swelling in the arm where the shot was given
            </Text>

        <Text style={{ fontSize: scale(15), marginTop: verticalScale(15), marginLeft: scale(15), fontWeight: "bold", textAlign: 'justify' }}>
            Severe Effects:
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(30), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
            Fever
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
        Dizziness or fainting (fainting after any vaccine, including the HPV vaccine, is more common among adolescents)
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
        Headache or feeling tired
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
        Nausea
            </Text>
        <Text style={{ fontSize: scale(15), marginTop: verticalScale(10), marginLeft: scale(30), marginRight: scale(15), textAlign: 'justify' }}>
        Muscle or joint pain
            </Text>
        <View style={{ height: scale(15) }} />
    </View>
);

class HPV extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: '  About Vaccines  ',
        headerLeft: <HamburgerIcon />,
        headerBackground: <Header />,
        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: scale(20),
            fontFamily: "Roboto-Bold",
            fontWeight: "bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        },
    });
    constructor(props) {
        super(props);
        this.state = {
            radioItems:
                [
                    {
                        label: 'ABOUT',
                        selected: true
                    },
                    {
                        label: 'DOSES',
                        selected: false,
                    },
                    {
                        label: 'SIDE EFFECTS',
                        selected: false
                    }
                ], selectedItem: '',

            index: 0,
            routes: [
                { key: 'ABOUT', title: ' ABOUT ' },
                { key: 'DOSES', title: ' DOSES ' },
                { key: 'SIDE_EFFECTS', title: ' SIDE_EFFECTS ' },
            ],
        };
    }
    componentDidMount() {
        this.state.radioItems.map((item) => {
            if (item.selected == true) {
                this.setState({ selectedItem: item.label });
            }
        });
    }
    changeActiveRadioButton(index) {
        this.state.radioItems.map((item) => {
            item.selected = false;
        });

        this.state.radioItems[index].selected = true;
        //alert(this.state.radioItems[index].label)

        this.setState({ radioItems: this.state.radioItems }, () => {
            this.setState({ selectedItem: this.state.radioItems[index].label });
        });
    }
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={{ marginTop: scale(15), marginLeft: scale(20), width: scale(100), height: scale(35), borderRadius: 10, flexDirection: "row", justifyContent: "space-around", alignItems: "center", backgroundColor: "transparent", borderColor: "#707070", borderWidth: 1 }}
                    onPress={() => this.props.navigation.navigate('About_Vaccine')}>
                    <Image
                        style={{ width: scale(15), height: scale(15) }}
                        source={require('../../../assets/arrow.png')}
                        resizeMode='contain'
                    />
                    <Text style={{ fontSize: scale(18), color: "#707070", }}>Back</Text>
                </TouchableOpacity>

                <View style={{ marginLeft: scale(20), marginRight: scale(20), backgroundColor: "#FFFFFF", borderRadius: 10, marginTop: verticalScale(20), height: scale(124) }}>
                    <View style={{ flexDirection: "row", }}>
                        <View style={{
                            width: "20%",
                            backgroundColor: "transparent",
                            borderRadius: 400,
                            top: scale(10),
                            left: scale(15),
                            position: "relative"
                        }}>
                            <LinearGradient
                                start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                                colors={['#a076e8', '#5dc4dd']}
                                style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                                <Image
                                    style={{
                                        width: scale(16.08),
                                        height: scale(16.08),
                                    }}
                                    source={require('../../../assets/injection2.png')}
                                />
                            </LinearGradient>
                        </View>
                        <View style={{
                            width: "80%",
                            marginTop: verticalScale(12)
                        }}>
                            <Text style={{ fontSize: scale(18), fontWeight: "bold" }}>HPV</Text>
                            <Text style={{ fontSize: scale(16), marginTop: scale(10) }}>Hydrolyzed vegetable protein</Text>
                        </View>
                    </View>

                    <View style={{ height: scale(40), bottom: 0, position: "absolute", flexDirection: "row", justifyContent: "center", marginLeft: scale(10), marginRight: (10) }}>
                        <View style={{ flexDirection: "row", }}>
                            {
                                this.state.radioItems.map((item, key) =>
                                    (
                                        <RadioButton key={key} button={item} onClick={this.changeActiveRadioButton.bind(this, key)} />
                                    ))
                            }
                        </View>
                        {/* <TouchableOpacity style={{ width: "33.33%", borderBottomWidth: 2, borderBottomColor: "#6F2BE2", alignItems: "center", justifyContent: "center", borderTopLeftRadius: 4, borderTopRightRadius: 4 }}>
                            <Text style={{ fontSize: scale(14), fontWeight: "bold" }}>hello</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: "33.33%", alignItems: "center", justifyContent: "center", }}>
                            <Text style={{ fontSize: scale(14) }}>hello</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: "33.33%", alignItems: "center", justifyContent: "center", }}>
                            <Text style={{ fontSize: scale(14) }}>hello</Text>
                        </TouchableOpacity> */}

                    </View>

                </View>

                <View style={{ height: scale(20) }} />
                <ScrollView showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}>
                    <View style={{ marginLeft: scale(20), marginRight: scale(20), borderRadius: 10, }}>

                        {this.state.selectedItem === "ABOUT" ?
                            <ABOUT /> :
                            this.state.selectedItem === "DOSES" ?
                                <DOSES />
                                :
                                <SIDE_EFFECTS />
                        }

                    </View>
                    <View style={{ height: scale(20) }} />
                </ScrollView>


            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f7f9',
    },
    tabbar: {
        backgroundColor: 'transparent',
        height: scale(40),
        //backgroundColor: "#000",

    },
    indicator: {
        backgroundColor: '#6F2BE2',
        width: scale(110),
        borderRadius: 3,
    },
    scene: {
        flex: 1,
    },
})

export default HPV;