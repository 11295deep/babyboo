import React from 'react';
import { Button, View, Text, StyleSheet, ImageBackground, Image, Animated, TouchableOpacity, ScrollView } from 'react-native';
import { scale, moderateScale, verticalScale } from '../../Components/Scale';
import Header from '../../Components/Header';
import LinearGradient from 'react-native-linear-gradient';
import HamburgerIcon from "../../Components/HamburgerIcon"

class About_Vaccine extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: '  About Vaccines  ',
        headerLeft: <HamburgerIcon />,
        headerBackground: <Header />,
        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: scale(20),
            fontFamily: "Roboto-Bold",
            fontWeight: "bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        },
    });
    constructor(props) {
        super(props);
        this.state = {
            vaccineDetails: {
                "BCG": {
                    "fullname": "Bacille Calmette-Guerin",
                    "Description": "BCG, or bacille Calmette-Guerin, is a vaccine for tuberculosis (TB) disease. BCG is used in many countries with a high prevalence of TB to prevent childhood tuberculous meningitis and miliary disease.",
                    "How does it work?": "The BCG vaccine contains a weakened strain of TB bacteria, which builds up immunity and encourages the body to fight TB if infected with it, without causing the disease itself"
                },
                "OPV": {
                    "fullname": "Oral Polio Vaccine",
                    "Description": "The action of oral polio vaccine (OPV) is two-pronged. OPV produces antibodies in the blood ('humoral' or serum immunity) to all three types of poliovirus, and in the event of infection, this protects the individual against polio paralysis by preventing the spread of poliovirus to the nervous system.",
                    "How does it work?": "Inactivated polio vaccine (IPV)   ... IVP produces antibodies in the blood to all three types of poliovirus. In the event of infection, these antibodies prevent the spread of the virus to the central nervous system and protect against paralysis."
                },
                "HEP-B": {
                    "fullname": "Hepatitis B",
                    "Description": "Hepatitis B is an infection of your liver. It can cause scarring of the organ, liver failure, and cancer. It can be fatal if it isn’t treated. It’s spread when people come in contact with the blood, open sores, or body fluids of someone who has the hepatitis B virus. It's serious, but if you get the disease as an adult, it shouldn’t last a long time. Your body fights it off within a few months, and you’re immune for the rest of your life. That means you can't get it again. But if you get it at birth, it’ unlikely to go away.",
                    "How does it work?": "The hepatitis B vaccine stimulates your natural immune system to protect against the hepatitis B e hepatitis B virus in the future."
                },
                "DTwP": {
                    "fullname": "Diphtheria-Tetanus-Pertussis",
                    "Description": "DPT (also DTP and DTwP) is a class of combination vaccines against three infectious diseases in humans: diphtheria, pertussis (whooping cough), and tetanus in which the pertussis component is acellular. This is in contrast to whole-cell, inactivated DTP (DTwP)",
                    "How does it work?": "The acellular vaccine uses selected antigens of the pertussis pathogen to induce immunity. Because it uses fewer antigens than the whole-cell vaccines, it is considered to cause fewer side effects, but it is also more expensive."
                },
                "IPV": {
                    "fullname": "Inactivated Polio Vaccine",
                    "Description": "The vaccines available for vaccination against polio are the IPV (inactivated polio vaccine) and the OPV (oral polio vaccine). IPV (inactivated polio vaccine) is given as a shot in the arm or leg. OPV (oral polio vaccine) is the preferred vaccine for most children.",
                    "How does it work?": "IPV is given by intramuscular or intradermal injection and needs to be administered by a trained health worker. IVP produces antibodies in the blood to all three types of poliovirus. In the event of infection, these antibodies prevent the spread of the virus to the central nervous system and protect against paralysis."
                },
                "Hib": {
                    "fullname": "Haemophilus influenzae type B",
                    "Description": "",
                    "How does it work?": "The vaccine provides long-term protection from Haemophilus influenzae type b. Those who are immunized have protection against Hib meningitis; pneumonia; pericarditis (an infection of the membrane covering the heart); and infections of the blood, bones, and joints caused by the bacteria."
                },
                "RV Vaccine": {
                    "fullname": "Rotavirus Vaccine",
                    "Description": "Rotavirus vaccine is a vaccine used to protect against rotavirus infections, which are the leading cause of severe diarrhea among young children. The vaccines prevent 15–34% of severe diarrhea in the developing world and 37–96% of severe diarrhea in the developed world.",
                    "How does it work?": "The vaccine contains live human rotavirus that has been weakened (attenuated), so that it stimulates the immune system but does not cause disease in healthy people. However it should not be given to people who are clinically immunosuppressed (either due to drug treatment or underlying illness). This is because the vaccine strain could replicate too much and cause a serious infection. This includes babies whose mothers have had immunosuppressive treatment while they were pregnant or breastfeeding."
                },
                "PCV": {
                    "fullname": "Pneumococcal Conjugate Vaccine",
                    "Description": "Pneumococcal conjugate vaccine (PCV) is a pneumococcal vaccine and a conjugate vaccine used to protect infants, young children, and adults against disease caused by the bacterium Streptococcus pneumoniae (the pneumococcus).",
                    "How does it work?": "The pneumococcal conjugate vaccine (PCV13) and the pneumococcal polysaccharide vaccine (PPSV23) protect against pneumococcal infections, which are caused by bacteria. The bacteria spread through person-to-person contact and can cause such serious infections as pneumonia, blood infections, and bacterial meningitis."
                },
                "MMR": {
                    "fullname": "Measles, Mumps, & Rubella",
                    "Description": "The MMR vaccine is a vaccine against measles, mumps, and rubella.The vaccine is also recommended in those who do not have evidence of immunity,those with well controlled HIV/AIDSand within 72 hours of exposure to measles among those who are incompletely immunized.It is given by injection.",
                    "How does it work?": "The MMR vaccine contains weakened versions of live measles, mumps and rubella viruses. The vaccine works by triggering the immune system to produce antibodies against measles, mumps and rubella."
                },
                "TCV": {
                    "fullname": "Typhoid Conjugate Vaccine",
                    "Description": "TCV is a typhoid conjugate vaccine manufactured by Bharat Biotech International Limited. It contains purified Vi capsular polysaccharide of Salmonella enterica serovar typhi Ty2 conjugated to a tetanus toxoid carrier protein.",
                    "How does it work?": "This vaccine works by exposing you to a small amount of the bacteria, which causes your body to develop immunity to the disease. Typhoid vaccine will not treat an active infection that has already developed in the body, and will not prevent any disease caused by bacteria other than Salmonella typhi. typhoid conjugate vaccine is safe, effective, and can provide protection for infants and children under two years of age."
                },
                "Varicella Vaccine": {
                    "fullname": "Chickenpox Vaccine",
                    "Description": "Varicella vaccine, also known as chickenpox vaccine, is a vaccine that protects against chickenpox. One dose of vaccine prevents 95% of moderate disease and 100% of severe disease.",
                    "How does it work?": "The chickenpox vaccine is a live vaccine and contains a small amount of weakened chickenpox-causing virus. The vaccine stimulates your immune system to produce antibodies that will help protect against chickenpox."
                },
                "HPV": {
                    "fullname": "Hydrolyzed vegetable protein",
                    "Description": "",
                    "How does it work?": "HPV vaccine can prevent most genital warts and most cases of cervical cancer. Protection from HPV vaccine is expected to be long-lasting. But vaccinated women still need cervical cancer screening because the vaccine does not protect against all HPV types that cause cervical cancer."
                }
            }
        }
    }

    vaccineDetails_Screen = (item) => {
        if(item === "BCG"){
            this.props.navigation.navigate('BCG')
        }else if(item == "OPV"){
            this.props.navigation.navigate('OPV')
        }else if(item == "HEP-B"){
            this.props.navigation.navigate('HEPB')
        }else if(item == "DTwP"){
            this.props.navigation.navigate('DTwP')
        }else if(item == "IPV"){
            this.props.navigation.navigate('IPV')
        }else if(item == "Hib"){
            this.props.navigation.navigate('Hib')
        }else if(item == "RV Vaccine"){
            this.props.navigation.navigate('RvVaccine')
        }else if(item == "PCV"){
            this.props.navigation.navigate('PCV')
        }else if(item == "MMR"){
            this.props.navigation.navigate('MMR')
        }else if(item == "TCV"){
            this.props.navigation.navigate('TCV')
        }else if(item == "Varicella Vaccine"){
            this.props.navigation.navigate('VaricellaVaccine')
        }else if(item == "HPV"){
            this.props.navigation.navigate('HPV')
        }

    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}>
                    {Object.keys(this.state.vaccineDetails).map((item, index) => (
                        <TouchableOpacity style={{ height: scale(80), marginLeft: scale(20), marginRight: scale(20), flexDirection: "row", backgroundColor: "#FFFFFF", borderRadius: 10, marginTop: verticalScale(27) }}
                        onPress={()=>this.vaccineDetails_Screen(item)}>
                            <View style={{
                                width: "20%",
                                backgroundColor: "transparent",
                                borderRadius: 400,
                                top: scale(10),
                                left: scale(15),
                                position: "relative"
                            }}>
                                <LinearGradient
                                    start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                                    colors={['#a076e8', '#5dc4dd']}
                                    style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                                    <Image
                                        style={{
                                            width: scale(16.08),
                                            height: scale(16.08),
                                        }}
                                        source={require('../../assets/injection2.png')}
                                    />
                                </LinearGradient>
                            </View>
                            <View style={{
                                width: "70%",
                                marginTop: verticalScale(12)
                            }}>
                                <Text style={{ fontSize: scale(18), fontWeight: "bold" }}>{item}</Text>
                                <Text style={{ fontSize: scale(16), marginTop: scale(10) }}>{this.state.vaccineDetails[item].fullname}</Text>
                            </View>
                            <View style={{ width: "10%", alignItems: "center", justifyContent: "center" }}>
                                <Image
                                    style={{ width: scale(15), height: scale(15) }}
                                    source={require('../../assets/leftarrow.png')}
                                    resizeMode='contain'
                                />
                            </View>
                        </TouchableOpacity>
                    ))}
                    {/* <TouchableOpacity style={{ height: scale(80), marginLeft: scale(20), marginRight: scale(20), flexDirection: "row", backgroundColor: "#FFFFFF", borderRadius: 10 }}>
                    <View style={{
                        width: "20%",
                        backgroundColor: "transparent",
                        borderRadius: 400,
                        top: scale(10),
                        left: scale(15),
                        position: "relative"
                    }}>
                        <LinearGradient
                            start={{ x: 0.0, y: 0.0 }} end={{ x: 0.5, y: 1.0 }}
                            colors={['#a076e8', '#5dc4dd']}
                            style={{ width: scale(28), borderRadius: 400, height: scale(28), justifyContent: "center", alignItems: "center" }}>
                            <Image
                                style={{
                                    width: scale(16.08),
                                    height: scale(16.08),
                                }}
                                source={require('../../assets/injection2.png')}
                            />
                        </LinearGradient>
                    </View>
                    <View style={{
                        width: "70%",
                        marginTop: verticalScale(12)
                    }}>
                        <Text style={{ fontSize: scale(18), fontWeight: "bold" }}>OPV</Text>
                        <Text style={{ fontSize: scale(16), marginTop: scale(10) }}>OPV</Text>
                    </View>
                    <View style={{ width: "10%",alignItems:"center",justifyContent:"center" }}>
                        <Image
                            style={{ width: scale(15), height: scale(15) }}
                            source={require('../../assets/leftarrow.png')}
                            resizeMode='contain'
                        />
                    </View>
                </TouchableOpacity> */}
                    <View style={{ height: scale(27) }} />
                </ScrollView>

            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f4f7f9',
    }
})

export default About_Vaccine;