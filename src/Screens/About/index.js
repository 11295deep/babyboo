import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator,
    BackHandler,
    
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Header from "../../Components/Header";
import LinearGradient from 'react-native-linear-gradient';
import { HeaderBackButton } from 'react-navigation-stack';
import { scale, moderateScale, verticalScale } from '../../Components/Scale';
const screenDimension = Dimensions.get('window');
const isBigDevice = screenDimension.height > 600;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;


class About extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: (
            <HeaderBackButton onPress={() => navigation.navigate('Home')} />
        ),
        title: 'About us',
        headerBackground: <Header />,

        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: scale(20),
            fontFamily:"Roboto-Bold",
      fontWeight:"bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        },
    });

    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    handleBackButtonClick() {
        this.props.navigation.navigate('Home');
        return true;
    }

    render() {

        return (
            
            <View style={styles.container}>

                <View style={{ width: scale(325), justifyContent: "center", alignItems: 'center', borderRadius: 10 }}>
                    <Text style={{ textAlign: 'justify', marginLeft: scale(5), marginRight: scale(5),color:"#707070" }}>BabyBoo, built leveraging blockchain technology, will be used to digitize vaccine cards. It is powered by StaTwig, a UNICEF Innovation Fund portfolio startup, which is focused to solve the problem of global wastage in vaccines and food caused due to the inefficiencies in Supply Chain by creating an extra layer of visibility and authenticity leveraging Blockchain and IoT. StaTwig’s scBlockchain cloud platform leverages Blockchain and IoT to deliver visibility, monitoring and tracking of products in your extended supply chain. It enables customers to track products in the extended supply chain, continuously record product health information in a tamper proof data registry,provide permissioned access to the information on-demand, and prevent third-party interference to data or contracts.</Text>
                </View>
                <View style={{marginTop:verticalScale(20),flexDirection:"row",justifyContent:"space-around",width:scale(300)}}>
                    
                    <Image
                        source={require('../../assets/fb.jpg')}
                        style={{ width: scale(75), height: scale(75) }}
                        resizeMode="contain"
                    />
                    <Image
                        source={require('../../assets/airmakernew.png')}
                        style={{ width: scale(75), height: scale(75) }}
                        resizeMode="contain"
                    />
                    <Image
                        source={require('../../assets/ciie.jpg')}
                        style={{ width: scale(75), height: scale(75) }}
                        resizeMode="contain"
                    />
                </View>

                <View style={{marginTop:verticalScale(20),flexDirection:"row",justifyContent:"space-around",width:scale(300)}}>
                    <Image
                        source={require('../../assets/hartfordnew.png')}
                        style={{ width: scale(75), height: scale(75) }}
                        resizeMode="contain"
                    />
                    <Image
                        source={require('../../assets/infus.jpg')}
                        style={{ width: scale(75), height: scale(75) }}
                        resizeMode="contain"
                    />
                    <Image
                        source={require('../../assets/thubnew.png')}
                        style={{ width: scale(75), height: scale(75) }}
                        resizeMode="contain"
                    />
                </View>
                
            </View>
            
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f4f7f9',
        justifyContent: "center",
        alignItems: "center"
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        alignSelf: "center",
        height: scale(50),
        width: scale(300),
        borderRadius: 10,
        marginTop: verticalScale(30)
    },
})

export default About;