import React from 'react';
import { Button, View, Text, StyleSheet, ImageBackground, Image, Animated } from 'react-native';
import { scale, moderateScale, verticalScale } from '../../Components/Scale';
class ImageLoader extends React.Component {
    state = {
        opacity: new Animated.Value(0),
    }

    onLoad = () => {
        Animated.timing(this.state.opacity, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
        }).start();
    }

    render() {
        return (
            <Animated.Image
                onLoad={this.onLoad}
                {...this.props}
                style={[
                    {
                        opacity: this.state.opacity,
                        transform: [
                            {
                                scale: this.state.opacity.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [0.85, 1],
                                })
                            }
                        ]
                    },
                    this.props.style,
                ]}
            />
        )
    }
}

class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        setTimeout(() => {
            console.log("splash_Screen");
            this.props.navigation.navigate('AuthLoading');
        }, 5000);
    }

    render() {
        return (
            <View style={styles.container}>
                <ImageLoader
                    style={{ width: scale(150), height: scale(150), marginBottom: "70%" }}
                    source={require('../../assets/logo1.png')}
                    resizeMode='contain'
                />

                <View style={{ alignItems: "center", marginBottom: "10%" }}>
                    <View style={{ alignItems: 'center', }}>
                        <Text style={{ fontSize: 11, color: '#707070', }}>Powered By</Text>
                        <Image
                            style={{ width: scale(100), height: scale(50), borderWidth: 0, }}
                            source={require('../../assets/logostatwig.jpg')}
                            resizeMode='contain'
                        />
                    </View>
                    <Text style={{ color: "#707070", fontSize: 11 }}>Version 1.0</Text>
                </View>

            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: 'white',
    }
})

export default SplashScreen;