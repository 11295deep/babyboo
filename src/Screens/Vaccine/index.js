import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ScrollView,
    ImageBackground,
    Image,
    YellowBox,
    FlatList,
    TouchableOpacity,
    ActivityIndicator,
    BackHandler,
    LayoutAnimation, UIManager,
    RefreshControl,
    TouchableHighlight,
} from 'react-native';
import { scale, moderateScale, verticalScale, } from '../../Components/Scale';
import { connect } from 'react-redux';
import { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes, Milestone } from '../../Action'
import Spinner from "react-native-spinkit";
import HamburgerIcon from "../../Components/HamburgerIcon"
import Header from '../../Components/Header';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';

const shadowStyle = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
}

class Vaccine extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        title: '  Vaccine  ',
        headerLeft: <HamburgerIcon />,
        headerBackground: (
            <Header />
        ),
        headerTitleStyle: {
            color: '#FFFFFF',
            fontSize: scale(20),
            fontFamily: "Roboto-Bold",
            fontWeight: "bold",
        },
        headerStyle: {
            backgroundColor: 'transparent',
        }
    });
    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            isLoading: true,
            image: '',
            expanded: false,
        }
    }

    componentDidMount = async () => {
        //     await this.props.userinfo()
        // await this.props.changeSelectedChild(0)
        // await this.props.rhymes()
        // await this.props.Milestone()
    }

    changeLayout = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ expanded: !this.state.expanded });
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        // this.listener.remove();

        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Home');
        return true;
    }

    onRefresh = async () => {

        //Clear old data of the list
        await this.props.userinfo()
        await this.props.changeSelectedChild(0)
        await this.props.rhymes()
        await this.props.Milestone()

        //Call the Service to get the latest data
        //this.GetData();
    }

    render() {
        console.log('nextVaccine', this.props.nextVaccine);

        return (
            <View style={styles.container}>
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            //refresh control used for the Pull to Refresh
                            refreshing={this.props.loder}
                            onRefresh={this.onRefresh.bind(this)}
                            colors={['#a076e8', '#5dc4dd']}
                        />
                    }>
                    <View style={[styles.card, shadowStyle]}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: verticalScale(20), marginLeft: scale(15), marginRight: scale(15) }}>
                            <Text style={{
                                fontSize: scale(18), color: "#707070", fontFamily: "Roboto-Bold",
                                fontWeight: "bold",
                            }}>Next Due Vaccine</Text>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('Next_Due_Vaccine')}>
                                <Text style={{ fontSize: scale(10), color: "#707070", alignSelf: "flex-end" }}>View All</Text>
                            </TouchableOpacity>
                        </View>

                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}
                            nestedScrollEnabled={true}>

                            <FlatList
                                data={this.props.nextVaccine}
                                horizontal={true}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item, index }) => (
                                    <TouchableOpacity
                                        disabled={index === 0 ? false : true}
                                        onPress={() => this.props.navigation.navigate('Next_Due_Vaccine')
                                            // this.props.navigation.navigate('Nextduevaccine', item);
                                        }>
                                        <View style={[styles.cardvaccine]}>
                                            <LinearGradient
                                                colors={index === 0 ? ['#A076E8', '#5DC4DD'] : ["#D6D6D6", "#9B9B9B"]}
                                                style={{ flex: 1, borderRadius: 10 }}
                                                start={{ x: 0, y: 1 }}
                                                end={{ x: 1, y: 0 }}>
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        marginTop: verticalScale(7),
                                                        marginLeft: scale(7),
                                                    }}>
                                                    <View
                                                        style={{
                                                            flexDirection: 'column',
                                                            width: '50%',
                                                            justifyContent: 'center',
                                                            alignItems: 'center',
                                                        }}>
                                                        <View
                                                            style={{
                                                                width: scale(86),
                                                                height: scale(86),
                                                                borderRadius: 400,
                                                                backgroundColor: 'white',
                                                            }}>
                                                            <Image
                                                                style={{
                                                                    width: scale(86),
                                                                    height: scale(86),
                                                                    borderRadius: 400,
                                                                }}
                                                                source={{ uri: this.props.user.imageUrl }}
                                                                resizeMode="cover"
                                                            />
                                                        </View>
                                                        <View style={{ marginTop: verticalScale(10) }}>
                                                            <Text
                                                                style={{
                                                                    fontSize: scale(15),
                                                                    fontFamily: "Roboto-Bold",
                                                                    fontWeight: "bold",
                                                                    color: '#FFFFFF',
                                                                }}>
                                                                {item.name}
                                                            </Text>
                                                        </View>
                                                        <View style={{ marginTop: verticalScale(5) }}>
                                                            <Text style={{ fontSize: scale(11), color: '#FFFFFF' }}>
                                                                {item.duration}
                                                            </Text>
                                                        </View>
                                                    </View>

                                                    <View
                                                        style={{
                                                            flexDirection: 'column',
                                                            width: '50%',
                                                            marginLeft: scale(20),
                                                            height: '100%',
                                                        }}>
                                                        <View>
                                                            <Text
                                                                style={{
                                                                    fontSize: scale(11),
                                                                    fontWeight: 'bold',
                                                                    color: '#FFFFFF',
                                                                }}>
                                                                {item.datenum} {item.month}, {item.year}
                                                            </Text>
                                                        </View>
                                                        <View style={{ marginTop: verticalScale(10) }}>
                                                            <Text style={{ fontSize: scale(11), color: '#FFFFFF' }}>
                                                                {item.vaccine1}
                                                            </Text>
                                                        </View>
                                                        <View style={{ marginTop: verticalScale(3) }}>
                                                            <Text style={{ fontSize: scale(11), color: '#FFFFFF' }}>
                                                                {item.vaccine2}
                                                            </Text>
                                                        </View>
                                                        <View style={{ marginTop: verticalScale(3) }}>
                                                            <Text style={{ fontSize: scale(11), color: '#FFFFFF' }}>
                                                                {item.vaccine3}
                                                            </Text>
                                                        </View>
                                                        <View style={{ marginTop: verticalScale(3) }}>
                                                            <Text style={{ fontSize: scale(11), color: '#FFFFFF' }}>
                                                                {item.vaccine4}
                                                            </Text>
                                                        </View>
                                                        <View style={{ marginTop: verticalScale(3) }}>
                                                            <Text style={{ fontSize: scale(11), color: '#FFFFFF' }}>
                                                                {item.vaccine5}
                                                            </Text>
                                                        </View>
                                                        <View style={{ marginTop: verticalScale(3) }}>
                                                            <Text style={{ fontSize: scale(11), color: '#FFFFFF' }}>
                                                                {item.vaccine6}
                                                            </Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </LinearGradient>
                                        </View>
                                    </TouchableOpacity>
                                )}
                            />

                            <View style={{ width: scale(15), backgroundColor: "#FFFFFF" }} />
                        </ScrollView>

                    </View>

                    <TouchableOpacity style={[styles.digitalreport, shadowStyle]}
                        onPress={() => this.props.navigation.navigate('Vaccinedigitalreport')}>
                        <View style={{ flexDirection: "row", marginLeft: scale(15), marginRight: scale(15), justifyContent: "center", alignItems: "center" }}>

                            <View style={{ width: "15%" }}>
                                <Image
                                    style={{ width: scale(31), height: scale(28) }}
                                    source={require('../../assets/digitalreport.png')}
                                    resizeMode='contain'
                                />
                            </View>

                            <View style={{ width: "80%" }}>
                                <View>
                                    <Text style={{ fontSize: scale(12), color: "#747474", }}>
                                        See your Baby’s Vaccine Digital Report
                                    </Text>
                                </View>
                            </View>

                            <View style={{ width: "5%" }}>
                                <Image
                                    style={{ width: scale(15), height: scale(15) }}
                                    source={require('../../assets/leftarrow.png')}
                                    resizeMode='contain'
                                />
                            </View>

                        </View>

                    </TouchableOpacity>

                    <View style={{
                        flexDirection: "row",
                        marginLeft: scale(5),
                        marginTop: verticalScale(25),
                        justifyContent: "space-between"
                    }}>
                        <Text style={{ fontSize: scale(15), color: "#747474", fontWeight: "bold", }}>
                            Vaccine Schedule
                        </Text>
                        {this.state.expanded ? <TouchableOpacity onPress={this.changeLayout}>
                            <Text style={{ fontSize: scale(12), color: "#747474", alignSelf: "flex-end" }}>
                                Hide
                        </Text>
                        </TouchableOpacity> :
                            <TouchableOpacity onPress={this.changeLayout}>
                                <Text style={{ fontSize: scale(12), color: "#747474", alignSelf: "flex-end" }}>
                                    View All
                        </Text>
                            </TouchableOpacity>}


                    </View>

                    {this.props.VaccineScheule.map((item, index) => (
                        <View>
                            {index <= 2 ?
                                <View style={{
                                    justifyContent: 'center',
                                    backgroundColor: '#ffffff',
                                    flexDirection: "row",
                                    alignItems: 'center',
                                    width: scale(322),
                                    marginTop: verticalScale(10),
                                    height: scale(80),
                                    borderRadius: 10
                                }}>
                                    <View style={{
                                        width: "25%",
                                        justifyContent: "center",
                                        alignItems: "center",
                                        height: scale(80),
                                    }}>
                                        {item.complete_count === item.count ? <Image
                                            style={{ width: scale(46), height: scale(46) }}
                                            source={require('../../assets/ui.png')}
                                            resizeMode='contain'
                                        /> : <View style={{
                                            borderRadius: 400,
                                            width: scale(46),
                                            height: scale(46),
                                            backgroundColor: "#EDEDED"
                                        }}>
                                            </View>}
                                    </View>

                                    <View style={{
                                        width: "45%",
                                        height: scale(80),
                                        flexDirection: "column",
                                        backgroundColor: "#fff",
                                        justifyContent: "center"
                                    }}>
                                        <Text style={{
                                            fontSize: scale(13),
                                            color: "#000"
                                        }}>
                                            {item.duration}
                                        </Text>

                                        <Text style={{
                                            fontSize: scale(13),
                                            color: "#000",
                                            marginTop: verticalScale(8)
                                        }}>
                                            {item.date}
                                        </Text>

                                    </View>

                                    <View style={{
                                        width: "30%",
                                        height: scale(80),
                                        backgroundColor: "#fff",
                                        justifyContent: "center"
                                    }}>
                                        <Text style={{
                                            fontSize: scale(13),
                                            color: "#000",
                                        }}>
                                            {item.complete_count}/{item.count} Vaccines
                            </Text>
                                    </View>

                                </View>
                                :

                                <View style={{ height: this.state.expanded ? null : 0, overflow: 'hidden', }}>
                                    {index > 2 ?
                                        <View style={{
                                            justifyContent: 'center',
                                            backgroundColor: '#ffffff',
                                            flexDirection: "row",
                                            alignItems: 'center',
                                            width: scale(322),
                                            marginTop: verticalScale(10),
                                            height: scale(80),
                                            borderRadius: 10
                                        }}>
                                            <View style={{
                                                width: "25%",
                                                justifyContent: "center",
                                                alignItems: "center",
                                                height: scale(80),
                                            }}>
                                                {item.complete_count === item.count ? <Image
                                                    style={{ width: scale(46), height: scale(46) }}
                                                    source={require('../../assets/ui.png')}
                                                    resizeMode='contain'
                                                /> : <View style={{
                                                    borderRadius: 400,
                                                    width: scale(46),
                                                    height: scale(46),
                                                    backgroundColor: "#EDEDED"
                                                }}>
                                                    </View>}
                                            </View>

                                            <View style={{
                                                width: "45%",
                                                height: scale(80),
                                                flexDirection: "column",
                                                backgroundColor: "#fff",
                                                justifyContent: "center"
                                            }}>
                                                <Text style={{
                                                    fontSize: scale(13),
                                                    color: "#000"
                                                }}>
                                                    {item.duration}
                                                </Text>

                                                <Text style={{
                                                    fontSize: scale(13),
                                                    color: "#000",
                                                    marginTop: verticalScale(8)
                                                }}>
                                                    {item.date}
                                                </Text>

                                            </View>

                                            <View style={{
                                                width: "30%",
                                                height: scale(80),
                                                backgroundColor: "#fff",
                                                justifyContent: "center"
                                            }}>
                                                <Text style={{
                                                    fontSize: scale(13),
                                                    color: "#000",
                                                }}>
                                                    {item.complete_count}/{item.count} Vaccines
                            </Text>
                                            </View>

                                        </View>
                                        : null}
                                </View>
                            }
                        </View>
                    ))}


                    <View style={{ height: scale(20) }} />

                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f4f7f9',
    },
    vaccinestext: {
        fontSize: scale(15),
        color: "#707070",
        marginLeft: scale(10),
        marginTop: verticalScale(7)
    },
    card: {
        height: scale(234),
        width: scale(322),
        backgroundColor: "#ffffff",
        borderRadius: 10,
        marginTop: verticalScale(15)
    },
    cardvaccine: {
        height: scale(151),
        width: scale(200),
        backgroundColor: "transparent",
        borderRadius: 10,
        marginLeft: scale(15),
        marginTop: verticalScale(20),
    },
    input: {
        margin: scale(15),
        height: scale(50),
        width: scale(300),
        padding: scale(5),
        fontSize: scale(16),
        borderBottomWidth: 1,
        borderBottomColor: 'black',
    },
    digitalreport: {
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        alignItems: 'center',
        width: scale(322),
        marginTop: verticalScale(10),
        height: scale(69),
        borderRadius: 10
    },
})

const VaccineDetails = (childVaccines) => {
    const res = childVaccines;
    let data = [];
    for (let key in res.childVaccines) {
        const vaccineObject = {};
        var com = 0;
        res.childVaccines[key].vaccines.forEach((vaccine, index) => {
            vaccineObject[`vaccine${index + 1}`] = vaccine.name;
            if (vaccine.givenDate !== "") {
                com = com + 1;
            }
            vaccineObject[`complete_count`] = com;
        });
        vaccineObject['date'] = res.childVaccines[key].dueDate;
        vaccineObject['name'] = res.child.name;
        vaccineObject['duration'] = key;
        vaccineObject[`count`] = res.childVaccines[key].vaccines.length;

        var dateString1 = res.childVaccines[key].dueDate;
        var dateParts1 = dateString1.split("/");
        var dateObject1 = new Date(+dateParts1[2], dateParts1[1] - 1, +dateParts1[0]);
        var month = new Array();
        month[0] = "Jan";
        month[1] = "Feb";
        month[2] = "Mar";
        month[3] = "Apr";
        month[4] = "May";
        month[5] = "Jun";
        month[6] = "Jul";
        month[7] = "Aug";
        month[8] = "Sept";
        month[9] = "Oct";
        month[10] = "Nov";
        month[11] = "Dec";
        vaccineObject['month'] = month[dateObject1.getMonth()]
        vaccineObject['datenum'] = dateObject1.getDate()
        vaccineObject['year'] = dateObject1.getUTCFullYear()
        data.push(vaccineObject);
    }
    console.log('data', data);
    //construct data object here for pass to the Flatlist
    let finalData = [];
    for (var i = 0; i < data.length; i++) {
        const v = {};
        if (data[i].count != data[i].complete_count) {
            finalData.push(data[i]);
        }
    }
    console.log('finalDatavaccine', finalData);

    return finalData;
}

const VaccineScheule = (childVaccines) => {
    const res = childVaccines;
    let data = [];
    for (let key in res.childVaccines) {
        const vaccineObject = {};
        var com = 0;
        res.childVaccines[key].vaccines.forEach((vaccine, index) => {
            vaccineObject[`vaccine${index + 1}`] = vaccine.name;
            if (vaccine.givenDate !== "") {
                com = com + 1;
            }
            vaccineObject[`complete_count`] = com;
        });
        vaccineObject['date'] = res.childVaccines[key].dueDate;
        vaccineObject['name'] = res.child.name;
        vaccineObject['duration'] = key;
        vaccineObject[`count`] = res.childVaccines[key].vaccines.length;

        var dateString1 = res.childVaccines[key].dueDate;
        var dateParts1 = dateString1.split("/");
        var dateObject1 = new Date(+dateParts1[2], dateParts1[1] - 1, +dateParts1[0]);
        var month = new Array();
        month[0] = "Jan";
        month[1] = "Feb";
        month[2] = "Mar";
        month[3] = "Apr";
        month[4] = "May";
        month[5] = "Jun";
        month[6] = "Jul";
        month[7] = "Aug";
        month[8] = "Sept";
        month[9] = "Oct";
        month[10] = "Nov";
        month[11] = "Dec";
        vaccineObject['month'] = month[dateObject1.getMonth()]
        vaccineObject['datenum'] = dateObject1.getDate()
        vaccineObject['year'] = dateObject1.getUTCFullYear()
        data.push(vaccineObject);
    }
    console.log('data', data);
    return data;
}

function mapStateToProps(state) {
    let selectedChild = state.userinfo.selectedChild
    return {
        user: state.userinfo.user[selectedChild],
        // ageInWeek:calculateAge(state.userinfo.user[0].child.dob),
        // completeVaccines:calculateVaccines(state.userinfo.user[0]),
        // completePerc:calculatePerc(state.userinfo.user[0]),
        nextVaccine: VaccineDetails(state.userinfo.user[selectedChild]),
        VaccineScheule: VaccineScheule(state.userinfo.user[selectedChild]),
        loder: state.loder,

    }
}

export default connect(mapStateToProps, { userinfo, loadingOn, loadingOff, changeSelectedChild, rhymes, Milestone })(Vaccine)