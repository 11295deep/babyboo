import React from 'react';
import {
    Button,
    View,
    Text,
    StyleSheet,
    TextInput,
    ImageBackground,
    Image,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator,
    BackHandler,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import { login,fbloginUser,forgotpwd } from '../../Action'
import AsyncStorage from '@react-native-community/async-storage';
import Header from "../../Components/Header";
import { scale, moderateScale, verticalScale } from '../../Components/Scale';
import LinearGradient from 'react-native-linear-gradient';
import { HeaderBackButton } from 'react-navigation-stack';

const screenDimension = Dimensions.get('window');
const isBigDevice = screenDimension.height > 600;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const shadowStyle = {
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
}

class ForgotPassword extends React.Component {

    constructor(props) {
        super(props);
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.state = {
            email: '',
        }
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        this.props.navigation.navigate('Auth');
        return true;
    }

    _pwd = async () => {
        var {
            email,
        } = this.state;
        const data = {
            "username": email,
        }
        console.log('email', this.state.email);
        const response = await this.props.forgotpwd(data);
        console.log('res', response);
        if (response.status === 200) {
            Alert.alert('Password successfully send to your Email id')
            this.props.navigation.navigate('Auth');
        }
    }

    render() {

        return (
            // <ImageBackground
            //     style={{ flex: 1 }}
            //     source={require('../assets/background.png')}>
            <View style={styles.container}>
                <View style={{ marginTop: verticalScale(15), marginLeft: scale(25), flexDirection: "row",width:"100%" }}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Auth')}>
                        <Image
                            style={{ width: scale(20), height: scale(20), borderWidth: 0, marginTop: 5 }}
                            source={require('../../assets/arrow.png')}
                            resizeMode='contain'
                        />
                    </TouchableOpacity>
                    <Text style={{ fontSize: scale(20), fontWeight: "bold", marginLeft: scale(20), color: "#707070" }}>Forgot Password</Text>
                </View>
                <View style={styles.container}>
                    <Image
                        style={{ width: scale(130), height: scale(130), borderWidth: 0, }}
                        source={require('../../assets/pwdlogo.png')}
                        resizeMode='contain'
                    />
                    <View style={{ marginTop: verticalScale(20), alignSelf: "flex-start", marginLeft: scale(20) }}>
                        <Text style={{ fontSize: scale(15), fontWeight: "bold", marginLeft: scale(12), color: "#707070" }}>Please enter your registered Email Id.</Text>
                    </View>
                    <View style={{ marginTop: verticalScale(11), alignSelf: "flex-start", marginLeft: 20 }}>
                        <Text style={{ fontSize: scale(11), marginLeft: scale(12), color: "#707070" }}>We will send a verification code to your registered Email Id.</Text>
                    </View>

                    <View style={[styles.SectionStyle, shadowStyle]}>
                        <TextInput
                            style={[styles.input]}
                            placeholder="Email"
                            autoCapitalize="none"
                            keyboardType="email-address"
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email} />
                    </View>

                    <TouchableOpacity
                        style={[styles.btnSignin, shadowStyle]}
                        onPress={this._pwd}>
                        <Text style={{ color: '#a076e8', fontWeight: 'bold', fontSize: scale(15) }}>Send</Text>
                    </TouchableOpacity>

                </View>
            </View>

            // </ImageBackground>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent:'center',
    },
    input: {
        margin: scale(15),
        height: scale(50),
        width: scale(300),
        paddingLeft: scale(20),
        fontSize: scale(15),
        borderRadius: 10,
        backgroundColor: '#fff',
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        borderRadius: 10,
        height: scale(50),
        width: scale(300),
        margin: scale(20),
        marginTop: verticalScale(50)
    },
    btnSignin: {
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        marginLeft: scale(20),
        borderRadius: 10,
        marginTop: verticalScale(100),
        width: scale(300),
        marginRight: scale(20),
        padding: scale(10),
        height: scale(50),
    },
})

function mapStateToProps(state) {
    return {
        // otpdata: state.otp.otpdata,
        // userdata: state.register.userdata,
    }
}

export default connect(mapStateToProps, { login,fbloginUser,forgotpwd })(ForgotPassword)